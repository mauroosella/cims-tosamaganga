from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=True


class product_product(osv.osv):
    _name = 'product.product'
    _inherit = 'product.product'
    
    def _get_stock_location_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_location_id')
    
    def _check_stock_move(self, cr, uid, ids, context=None):
        stock_obj = self.pool.get('stock.move')
        res = stock_obj.read(cr, uid, ids, ['product_id'])
        
        p_id = []
        for p in res:
            p_id.append(p['product_id'][0])
            
        return list(set(p_id))

    def _get_qty_at_location(self, cr, uid, ids,field_name, arg, context):
        location_id = self._get_stock_location_id(cr, uid)
        stock_o = self.pool.get('stock.report.prodlots')

        res = {}.fromkeys(ids, 0.0)
        cr.execute('''
            SELECT product_id, sum(qty) from stock_report_prodlots
            where prodlot_id is not NULL and location_id = '''+str(location_id)+''' group by product_id order by 1 desc;''')
        res.update(dict(cr.fetchall()))
        return res
        
    _columns = {
        #'pharmacy' : fields.boolean('Pharmacy'),
        'qty_available_at_location' : fields.function(_get_qty_at_location,type="integer",string="Available"),
        'min_qty' : fields.integer('Minimum Level'),
    }
    
    _defaults = {
        #'pharmacy' : False,
        'min_qty' : 0.0,
    }
    
    _order = 'name_template asc'
    
    def update_list_price(self, cr, uid, ids, price,context=None):
        if context is None:
            context = {}
        self.write(cr, uid, ids, {'list_price' : price}, context=context)
    
    def _get_product_id_by_name(self, cr, uid, product_name):
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('name','=',product_name)])
        
        for product in product_obj.browse(cr, uid, product_ids):
            if product is not None:
                return product.id    
        
        return None
    
    def onchange_list_price(self, cr, uid, ids, product_name, price, context=None):
        product_id = self._get_product_id_by_name(cr, uid, product_name)
        
        if product_id is not None:
            history_price_obj = self.pool.get('isf.product.history.sell')
            history_price_obj.create(cr, uid, {
                'product' : product_id,
                'price' : price,
            }, context=context)
        
        return True
        
    def onchange_standard_price(self, cr, uid, ids, product_name, price, context=None):
        product_id = self._get_product_id_by_name(cr, uid, product_name)
        
        if product_id is not None:
            history_price_obj = self.pool.get('isf.product.history.purchase')
            history_price_obj.create(cr, uid, {
                'product' : product_id,
                'price' : price,
            }, context=context)
        
        return True

    def name_get(self, cr, user, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not len(ids):
            return []
        def _name_get(d):
            name = d.get('name','')
            code = d.get('default_code',False)
            if code:
                name = '%s [%s]' % (name,code)
            if d.get('variants'):
                name = name + ' - %s' % (d['variants'],)
            return (d['id'], name)

        partner_id = context.get('partner_id', False)

        result = []
        for product in self.browse(cr, user, ids, context=context):
            sellers = filter(lambda x: x.name.id == partner_id, product.seller_ids)
            if sellers:
                for s in sellers:
                    mydict = {
                              'id': product.id,
                              'name': s.product_name or product.name,
                              'default_code': s.product_code or product.default_code,
                              'variants': product.variants
                              }
                    result.append(_name_get(mydict))
            else:
                mydict = {
                          'id': product.id,
                          'name': product.name,
                          'default_code': product.default_code,
                          'variants': product.variants
                          }
                result.append(_name_get(mydict))
        return result

    
product_product()

class isf_product_history_sell(osv.Model):
    _name = 'isf.product.history.sell'
    
    _columns = {
        'product' : fields.many2one('product.product','name','Product',required=True),
        'price' : fields.float('Price', size=32, required=True),
    }

class isf_product_history_purchase(osv.Model):
    _name = 'isf.product.history.purchase'
    
    _columns = {
        'product' : fields.many2one('product.product','name','Product',required=True),
        'lot_id' : fields.many2one('stock.production.lot','id','Lot',required=False),
        'price' : fields.float('Price', size=32, required=True),
    }
#
# Lot View
#

class isf_product_wizard(osv.TransientModel):
    _name = 'isf.product.wizard'

    def _get_default_code(self, cr, uid, ids, context=None):
        prod_o = self.pool.get('product.product')
        prod_ids = prod_o.search(cr,uid,[],order="id desc")
        max_id = prod_ids[:1]

        if _debug:
            _logger.debug('==> MAX ID : %s',max_id)

        return int(max_id[0]) + 1

    def _get_type(self, cr, uid, context=None):
        if context is None:
            context = {}

        if context.get('type'):
            return context.get('type')

        return 'product'
    
    def _get_internal_type(self, cr, uid, context=None):
        if context is None:
            context = {}

        if context.get('internal_type'):
            return context.get('internal_type')

        return None
    
    def _get_default_purchase_ok(self, cr, uid, context=None):
        if context is None:
            context = {}

        if context.get('purchase_ok'):
            return context.get('purchase_ok')

        return False
    
    def _get_default_cost_method(self, cr, uid, context=None):
        if context is None:
            context = {}
        
        if context.get('type') == 'service':
            return 'standard'
        
        return 'fifo'
    
    def onchange_supplier_price(self, cr, uid, ids, supplier_price,uom_po_id,context=None):
        if context is None:
            context = {}
        
        result = {'value': {} }

        if uom_po_id:
            uom_obj = self.pool.get('product.uom').browse(cr, uid, uom_po_id)
            standard_price = supplier_price * uom_obj.factor
        else:
            standard_price = supplier_price
        
        calculated_price = 0
        percentage = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','percentage_cost_price')
        if percentage > 0:
            calculated_price = round(standard_price * percentage / 100, 2)
        
        result['value'].update({
            'standard_price' : standard_price,
            'standard_price_view' : standard_price,
            'price' : calculated_price,
        })
        return result

    _columns = {
        'name' : fields.char('Product Name',size=128),
        'price': fields.float('Price'),
        'supplier_price' : fields.float('Supplier Price'),
        'standard_price': fields.float('Unit Cost'),
        'standard_price_view': fields.float('Unit Cost'),
        'default_code' : fields.char('Code'),
        'categ_id' : fields.many2one('product.category','Category'),
        'type' :  fields.selection([('service','Service'),('product','Stockable Product')], 'Product Type'),
        'internal_type' : fields.selection([('drug', 'Drug'),('consumable', 'Consumable')], 'Internal Type'),
        'min_qty' : fields.integer('Min Quantity'),
        'sale_ok' : fields.boolean('Can Be Sold'),
        'purchase_ok' : fields.boolean('Can Be Purchased'),
        'uom_po_id': fields.many2one('product.uom', 'Purchase Unit of Measure', help="Default Unit of Measure used for purchase orders. It must be in the same category than the default unit of measure."),
        'cost_method': fields.selection([('standard','Standard Price'), ('average','Average Price'), ('fifo', 'FIFO'), ('lifo', 'LIFO')],'Costing Method',required=True),
        'property_account_income': fields.property(
            'account.account',
            type='many2one',
            relation='account.account',
            string="Income Account",
            view_load=True,
            help="This account will be used for invoices instead of the default one to value sales for the current product."),
        'property_account_expense': fields.property(
            'account.account',
            type='many2one',
            relation='account.account',
            string="Expense Account",
            view_load=True,
            help="This account will be used for invoices instead of the default one to value expenses for the current product."),
        'tag_ids' : fields.many2many('product.tag', 'isf_product_wizard_product_tag_rel', 'product_id', 'tag_id', 'Active Ingredients'),
    }

    _defaults = {
        'type' : _get_type,
        'internal_type' : _get_internal_type,
        'default_code' : _get_default_code,
        'sale_ok' : True,
        'purchase_ok' : _get_default_purchase_ok,
        'cost_method' : _get_default_cost_method,
    }
    
    
    def add_product(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        prod_pool = self.pool.get('product.product')
        account_pool = self.pool.get('account.account')
        
        
        # UOM for services
        if not data.uom_po_id:
            uom_pool = self.pool.get('product.uom')
            data.uom_po_id = uom_pool.browse(cr, uid, 1)
        
        # cost_method if type changes
        if data.type == 'service':
            data.cost_method = 'standard'
        else:
            data.cost_method = 'fifo'
    
        tag_ids = [d['id'] for d in data.tag_ids]
        sale_ok = True
        purchase_ok = True
        # income and expense accounts
        if data.internal_type and data.internal_type == 'consumable':
            sale_ok = False;
            account_income_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','consumable_property_account_income_id')
            account_expense_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','consumable_property_account_expense_id')
            data.property_account_income = account_pool.browse(cr, uid, account_income_id).id
            data.property_account_expense = account_pool.browse(cr, uid, account_expense_id).id
        if data.internal_type and data.internal_type == 'drug':
            account_income_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','product_property_account_income_id')
            account_expense_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','product_property_account_expense_id')
            data.property_account_income = account_pool.browse(cr, uid, account_income_id).id
            data.property_account_expense = account_pool.browse(cr, uid, account_expense_id).id
        
        if _debug:
            _logger.debug('==> standard_price : %s',data.standard_price_view)
            _logger.debug('==> tag_ids : %s', tag_ids)
        
        vals = {
            'name' : data.name,
            'list_price' : data.price,
            'type' : data.type,
            'procure_method' : 'make_to_stock',
            'supply_method' : 'buy',
            'default_code' : data.default_code,
            'categ_id' : data.categ_id.id,
            'min_qty' : data.min_qty,
            'sale_ok' : sale_ok,
            'purchase_ok' : purchase_ok,
            'uom_po_id' : data.uom_po_id.id,
            'standard_price' : data.standard_price_view,
            'cost_method' : data.cost_method,
            'property_account_income' : data.property_account_income,
            'property_account_expense' : data.property_account_expense,
            'track_incoming' : True,
            'track_outgoing' : True,
            'tag_ids' : [(6, 0, tag_ids)], #https://stackoverflow.com/questions/9377402/insert-into-many2many-odoo-former-openerp
        }
        
        prod_id = prod_pool.create(cr, uid, vals, context=context)
        
        if _debug:
            _logger.debug('==> new prod_id : %s', prod_id)
        
        return True