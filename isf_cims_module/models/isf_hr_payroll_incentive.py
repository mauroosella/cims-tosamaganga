from openerp.osv import fields, osv
from tools.translate import _
import logging
import openerp

_logger = logging.getLogger(__name__)
_debug=False

class isf_hr_incentive(osv.Model):
    _name = 'isf.hr.incentive'
    
    _rec_name = 'employee_id'
    
    _columns = {
        'state' : fields.selection([('draft','Draft'),('confirm','Confirmed')],'State',readonly=True),
        'employee_id' : fields.many2one('hr.employee','Employee',required=True),
        'start_period_id' : fields.many2one('account.period','Start Period',required=True),
        'end_period_id' : fields.many2one('account.period','End Period',required=True),
        'salary_rule_id' : fields.many2one('hr.salary.rule','Rule',required=True),
        'amount' : fields.float('Amount',required=True),
        'currency_id' : fields.many2one('res.currency','Currency',required=True),
    }
    
    _defaults = {
        'state' : 'draft',
    }

    def confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state' : 'confirm'}, context=context)
        
    def cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state' : 'draft'}, context=context)
        
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        inc = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in inc:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a incentive entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True