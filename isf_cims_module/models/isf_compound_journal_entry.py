from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=False

#
# Multiple Income
#
class isf_compound_line(osv.osv_memory):
    _name = 'isf.compound.line.income'
    
    
    def _get_accounts(self, cr, uid,  context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                if _debug:
                    _logger.debug('Account id : %d', account_id)
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    if _debug:
                        _logger.debug('Account : %d , %s, %s', account.id, account.code, account.name)
                    result.append((account.id, account.code+" "+account.name))
                
        return result
    
    _columns = {
        'name' : fields.char('Description',size=64,required=False),
        'account' : fields.selection(_get_accounts,'Account', required=True),
        'amount' : fields.float('Amount',size=32, required=True),
        'analytic_account' : fields.many2one('account.analytic.account','Analytic Account', required=False),
        'line_id': fields.many2one('isf.compound.journal.entry.income', 'Line', select=True),
    }
    
isf_compound_line()

class isf_compound_journal_entry_income(osv.osv_memory):
    _name = 'isf.compound.journal.entry.income'
    
    def _get_help(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','help_sale')
        return help
    
    def _get_sale_journal_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','journal_sale_id')
    
    def _get_cash_bank_account(self, cr, uid,  context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_cash_bank_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                if _debug:
                    _logger.debug('Account id : %d', account_id)
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    if _debug:
                        _logger.debug('Account : %d , %s, %s', account.id, account.code, account.name)    
                    result.append((account.id, account.code+" "+account.name))
                
        return result
    
    _columns = {
        'name' : fields.char('Description',size=64,required=True),
        'ref' : fields.char('Reference',size=32, required=False),
        'cash_bank_account_id' : fields.selection(_get_cash_bank_account,'Cash/Bank Account', required=True),
        'amount' : fields.float('Amount',size=16,required=True),
        'line_ids' : fields.one2many('isf.compound.line.income','line_id','Income',required=True),
        'date' : fields.date('Date',required=True),
        'balance' : fields.float('Balance',size=32),
        'currency' : fields.many2one('res.currency','Currency'),
        'currency_view' : fields.many2one('res.currency','Currency',readonly=True),
        'help_sale' : fields.text('Help', size=512),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
        'balance' : 0.0,
        'help_sale' : _get_help,
    }
    
    def _check_balance(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        balance = data.amount
        
        for line in data.line_ids:
            if _debug:
                _logger.debug('Balance : %f', balance)
            balance = balance - line.amount
        
        return (balance == 0.0)
        
    def _check_amount(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        return (data.amount >= 0.0)
    
    _constraints = [
         (_check_balance, 'Balance must be zero (0.0) ',['balance']),
         (_check_amount, 'Balance must be positive',['amount']),
    ]
    
    def save_and_new(self, cr, uid, ids, context=None):
        ir_model_pool = self.pool.get('ir.model')
        
        for record in self.browse(cr, uid, ids, context=context):
            if _debug:
                _logger.debug('Model id : %s', record.id)
                
        self._save(cr, uid, ids, context=None)
                           
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': self._name,
            'target': 'new',
            'context': {
                'default_model': 'isf.compound.journal.entry.income',
                'ref' : False,
            },
    }
    
    def onchange_account(self, cr, uid, ids, account_id, context=None):
        if context is None:
            context = {}
    
        isf_lib = self.pool.get('isf.lib.utils')
    
        result = {'value':{} }
        account_pool = self.pool.get('account.account')
        account_ids = account_pool.search(cr, uid, [('id','=',account_id)],limit=1)
        account_obj = account_pool.browse(cr, uid, account_ids, context=context)
        
        for account in account_obj:
            currency_id = account.currency_id.id
            
            if not currency_id:
                currency_id = isf_lib.get_company_currency_id(cr, uid, context=context)
                
            result['value'].update({
                'currency' : currency_id,
                'currency_view' : currency_id,                    
            })
        return result
        
        
    def onchange_amount(self, cr, uid, ids, amount, context=None):
        result = {'value':{} }
        
        self._compute_balance(cr, uid, ids, amount,context=context)
        
        result['value'].update({
            'balance' : balance,
        })
        
        return result
    
    def isf_cj_save(self, cr, uid, ids, context=None):
        self._save(cr, uid, ids, context=context)
        return {'type': 'ir.actions.act_window_close'}
        
    
    def _save(self, cr, uid, ids, context=None):
        isf_lib = self.pool.get('isf.lib.utils')
        
        if _debug:
            _logger.debug('Context : %s', context)
            
        data = self.browse(cr, uid, ids, context=context)[0]
        
        journal_id = self._get_sale_journal_id(cr, uid)
        
        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        move = move_pool.account_move_prepare(cr, uid, journal_id, data.date, ref=data.ref, context=context)
        move_id = move_pool.create(cr, uid, move, context=context)
        
        company_currency_id = isf_lib.get_company_currency_id(cr, uid, context=context)
        currency_id = False
        amount_currency = False
        amount_currency_credit = False
        amount = data.amount
		
        if _debug:
            _logger.debug('Currency Company(%s) data(%s)',company_currency_id, data.currency.id)
        
        compute_amount_currency = False
        if data.currency.id != company_currency_id:
            currency_pool = self.pool.get('res.currency')
            amount =  currency_pool.compute(cr, uid, data.currency.id, company_currency_id, data.amount, context=context)
            amount_currency = data.amount
            currency_id = data.currency.id
            compute_amount_currency = True
            
        # Debit move line on cash/bank account
        move_line = {
            'analytic_account_id': False,
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency_id,
            'credit': 0.0,
            'debit': amount,
            'date_maturity' : False,
            'amount_currency': amount_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.cash_bank_account_id),
            'state' : 'valid'
        }
        
        if _debug:
            _logger.debug('move_line : %s',move_line)
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        # Iterate on account line to credit the corresponding amount
        for line in data.line_ids:
            amount_currency = False
            amount = line.amount
            if compute_amount_currency:
                amount =  currency_pool.compute(cr, uid, data.currency.id, company_currency_id, line.amount, context=context)
                amount_currency = -1 * line.amount
                
            move_line = {
                'analytic_account_id': line.analytic_account.id,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : False,
                'name': line.name or '/',
                'currency_id': currency_id,
                'credit': amount,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(line.account),
                'state' : 'valid'
            }
            
            if _debug:
                _logger.debug('move_line : %s',move_line)
		
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        return True
        
#
# Multiple Expense
#
class isf_compound_line_expense(osv.osv_memory):
    _name = 'isf.compound.line.expense'
    
    
    def _get_accounts(self, cr, uid,  context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                if _debug:
                    _logger.debug('Account id : %d', account_id)
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    if _debug:
                        _logger.debug('Account : %d , %s, %s', account.id, account.code, account.name)
                    result.append((account.id, account.code+" "+account.name))
                
        return result
    
    _columns = {
        'name' : fields.char('Description',size=64,required=False),
        'account' : fields.selection(_get_accounts,'Account', required=True),
#         'account' : fields.many2one('account.account','Account', required=True, domain=[('user_type.name','=','Expense')]),
        'amount' : fields.float('Amount',size=32, required=True),
        'analytic_account' : fields.many2one('account.analytic.account','Analytic Account', required=True),
        'line_id': fields.many2one('isf.compound.journal.entry.expense', 'Line', select=True),
    }
    
    
isf_compound_line_expense()

class isf_compound_journal_entry_expense(osv.osv_memory):
    _name = 'isf.compound.journal.entry.expense'
    
    def _get_help(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','help_purchase')
        return help
    
    def _get_sale_journal_id(self, cr, uid, context=None):
        if context is None:
            context = {}
        
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','journal_purchase_id')
    
    def _get_cash_bank_account(self, cr, uid,  context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_cash_bank_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                if _debug:
                    _logger.debug('Account id : %d', account_id)
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    if _debug:
                        _logger.debug('Account : %d , %s, %s', account.id, account.code, account.name)
                    result.append((account.id, account.code+" "+account.name))
                
        return result
    
    _columns = {
        'name' : fields.char('Description',size=64,required=True),
        'ref' : fields.char('Reference',size=32, required=False),
        'cash_bank_account_id' : fields.selection(_get_cash_bank_account,'Cash/Bank Account', required=True),
        'amount' : fields.float('Amount',size=16,required=True),
        'line_ids' : fields.one2many('isf.compound.line.expense','line_id','Expense',required=True),
        'date' : fields.date('Date',required=True),
        'balance' : fields.float('Balance',size=32),
        'currency' : fields.many2one('res.currency','Currency'),
        'currency_view' : fields.many2one('res.currency','Currency',readonly=True),
        'help_purchase' : fields.text('Help', size=512),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
        'balance' : 0.0,
        'help_purchase' : _get_help,
    }
    
    def _check_balance(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        balance = data.amount
        
        for line in data.line_ids:
            if _debug:
                _logger.debug('Balance : %f', balance)
            balance = balance - line.amount
        
        return (balance == 0.0)
        
    def _check_amount(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        return (data.amount >= 0.0)
    
    _constraints = [
         (_check_balance, 'Balance must be zero (0.0) ',['balance']),
         (_check_amount, 'Balance must be positive',['amount']),
    ]
    
    def save_and_new(self, cr, uid, ids, context=None):
        ir_model_pool = self.pool.get('ir.model')
        
        for record in self.browse(cr, uid, ids, context=context):
            if _debug:
                _logger.debug('Model id : %s', record.id)
                
        self._save(cr, uid, ids, context=None)
                           
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': self._name,
            'target': 'new',
            'context': {
                'default_model': 'isf.compound.journal.entry.expense',
                'ref' : False,
            },
    }
    
    def onchange_account(self, cr, uid, ids, account_id, context=None):
        if context is None:
            context = {}
    
        isf_lib = self.pool.get('isf.lib.utils')
    
        result = {'value':{} }
        account_pool = self.pool.get('account.account')
        account_ids = account_pool.search(cr, uid, [('id','=',account_id)],limit=1)
        account_obj = account_pool.browse(cr, uid, account_ids, context=context)
        
        for account in account_obj:
            currency_id = account.currency_id.id
            
            if not currency_id:
                currency_id = isf_lib.get_company_currency_id(cr, uid, context=context)
                
            result['value'].update({
                'currency' : currency_id,
                'currency_view' : currency_id,                    
            })
        return result
        
        
    def onchange_amount(self, cr, uid, ids, amount, context=None):
        result = {'value':{} }
        
        self._compute_balance(cr, uid, ids, amount,context=context)
        
        result['value'].update({
            'balance' : balance,
        })
        
        return result
    
    def isf_cj_save(self, cr, uid, ids, context=None):
        self._save(cr, uid, ids, context=context)
        return {'type': 'ir.actions.act_window_close'}
        
    
    def _save(self, cr, uid, ids, context=None):
        isf_lib = self.pool.get('isf.lib.utils')
        
        if _debug:
            _logger.debug('Context : %s', context)
            
        data = self.browse(cr, uid, ids, context=context)[0]
        
        journal_id = self._get_sale_journal_id(cr, uid)
        
        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        move = move_pool.account_move_prepare(cr, uid, journal_id, data.date, ref=data.ref, context=context)
        move_id = move_pool.create(cr, uid, move, context=context)
        
        company_currency_id = isf_lib.get_company_currency_id(cr, uid, context=context)
        currency_id = False
        amount_currency = False
        amount_currency_credit = False
        amount = data.amount
		
        if _debug:
            _logger.debug('Currency Company(%s) data(%s)',company_currency_id, data.currency.id)
        
        compute_amount_currency = False
        if data.currency.id != company_currency_id:
            currency_pool = self.pool.get('res.currency')
            amount =  currency_pool.compute(cr, uid, data.currency.id, company_currency_id, data.amount, context=context)
            amount_currency = -1 * data.amount
            currency_id = data.currency.id
            compute_amount_currency = True
            
        # Credit move line on cash/bank account
        move_line = {
            'analytic_account_id': False,
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency_id,
            'credit': amount,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': amount_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.cash_bank_account_id),
            'state' : 'valid'
        }
        
        if _debug:
            _logger.debug('move_line : %s',move_line)
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        # Iterate on account line to debit the corresponding amount
        for line in data.line_ids:
            amount_currency = False
            amount = line.amount
            if compute_amount_currency:
                amount =  currency_pool.compute(cr, uid, data.currency.id, company_currency_id, line.amount, context=context)
                amount_currency = line.amount
                
            move_line = {
                'analytic_account_id': line.analytic_account.id,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : False,
                'name': line.name or '/',
                'currency_id': currency_id,
                'credit': 0.0,
                'debit': amount,
                'date_maturity' : False,
                'amount_currency': amount_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(line.account),
                'state' : 'valid'
            }
            
            if _debug:
                _logger.debug('move_line : %s',move_line)
		
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        return True