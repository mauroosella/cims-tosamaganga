from openerp.osv import fields, orm, osv
import logging
import time
from openerp import tools

_logger = logging.getLogger(__name__)

class isf_module_update(orm.Model):
    _name='isf.module.update'
    
    _columns = {
            'module_name' : fields.char('Module Name', size=128),
            'updated' : fields.boolean('Updated'),
        }
    
    _defaults = {
            'updated' : False,
        }
    
    _sql_constraints = [(
        'module_name', 'unique(module_name)', 'module already exists')]
    
    def is_updated(self, cr, uid, module_name, debug=False, context=None):
        context = context or {}
        if debug:
            _logger.debug('==> looking for ''%s'' module...', module_name)
        update_id = self.search(cr, uid, [('module_name','=',module_name)], limit=1)
        if not update_id:
            if debug:
                _logger.debug('==> not found! Creating record...')
            update_id = self.create(cr, uid, {'module_name' : module_name,})
        else:
            update_id = update_id[0]
            if debug:
                _logger.debug('==> found with id %s!', update_id)
            
        update = self.browse(cr, uid, update_id)
        return update.updated
    
    def module_updated(self, cr, uid, module_name, debug=False, context=None):
        context = context or {}
        update_id = self.search(cr, uid, [('module_name','=',module_name)], limit=1)
        self.write(cr, uid, update_id, {'updated' : True,})
        if debug:
            _logger.debug('==> Updated...')