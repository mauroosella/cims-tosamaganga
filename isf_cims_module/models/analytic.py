from openerp.osv import fields, osv
from tools.translate import _
import logging

_debug=False
_logger = logging.getLogger(__name__)

class account_analytic_account(osv.osv):
    _inherit = 'account.analytic.account'
    
    def name_get(self, cr, uid, ids, context=None):
        res = []
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for id in ids:
            elmt = self.browse(cr, uid, id, context=context)
            name = _("%s - %s") % (elmt.code, self._get_one_full_name(elmt))
            res.append((id,name))
        return res
    
#     def _get_full_name(self, cr, uid, ids, name=None, args=None, context=None):
#         if context == None:
#             context = {}
#         res = {}
#         for elmt in self.browse(cr, uid, ids, context=context):
#             res[elmt.id] = self._get_one_full_name(elmt)
#         return res
#     
#     def _get_one_full_name(self, elmt, level=6):
#         if level<=0:
#             return '...'
#         if elmt.parent_id and not elmt.type == 'template':
#             parent_path = self._get_one_full_name(elmt.parent_id, level-1) + " / "
#         else:
#             parent_path = ''
#         return parent_path + elmt.name
        