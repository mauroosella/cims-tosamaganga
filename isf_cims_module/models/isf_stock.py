from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time
import openerp.addons.decimal_precision as dp
import openerp
from cookielib import logger
from openerp.addons import one2many_sorted
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=True


class stock_location(osv.osv):
    _inherit = 'stock.location'
    
    _columns = {
        'available_in_selection' : fields.boolean('Available in Selection', required=False),
        'analytic_account_id': fields.many2one('account.analytic.account','Analytic Account', required=False),
    }
    
    _defaults = {
        'available_in_selection' : False,
        'analytic_account_id' : False,
    }
    
stock_location()


class stock_fill_inventory(osv.osv_memory):
    _inherit = "stock.fill.inventory"
    
    def fill_inventory(self, cr, uid, ids, context=None):
        """ To Import stock inventory according to products available in the selected locations.
        @param self: The object pointer.
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param ids: the ID or list of IDs if we want more than one
        @param context: A standard dictionary
        @return:
        """
        if context is None:
            context = {}

        inventory_line_obj = self.pool.get('stock.inventory.line')
        location_obj = self.pool.get('stock.location')
        move_obj = self.pool.get('stock.move')
        uom_obj = self.pool.get('product.uom')
        if ids and len(ids):
            ids = ids[0]
        else:
             return {'type': 'ir.actions.act_window_close'}
        fill_inventory = self.browse(cr, uid, ids, context=context)
        res = {}
        res_location = {}

        if fill_inventory.recursive:
            location_ids = location_obj.search(cr, uid, [('location_id',
                             'child_of', [fill_inventory.location_id.id])], order="id",
                             context=context)
        else:
            location_ids = [fill_inventory.location_id.id]

        if _debug:
            _logger.debug('Location : %s',location_ids)
            _logger.debug('Context : %s',context)

        res = {}
        flag = False

        for location in location_ids:
            datas = {}
            res[location] = {}
            move_ids = move_obj.search(cr, uid, ['|',('location_dest_id','=',location),('location_id','=',location),('state','=','done'),('prodlot_id','!=',False)], context=context)
            local_context = dict(context)
            local_context['raise-exception'] = False
            for move in move_obj.browse(cr, uid, move_ids, context=context):
                lot_id = move.prodlot_id.id
                prod_id = move.product_id.id
                if move.location_dest_id.id != move.location_id.id:
                    if move.location_dest_id.id == location:
                        qty = uom_obj._compute_qty_obj(cr, uid, move.product_uom,move.product_qty, move.product_id.uom_id, context=local_context)
                    else:
                        qty = -uom_obj._compute_qty_obj(cr, uid, move.product_uom,move.product_qty, move.product_id.uom_id, context=local_context)

                    if datas.get((prod_id, lot_id)):
                        qty += datas[(prod_id, lot_id)]['product_qty']

                    period_id = False
                    cost_price = move.product_id.list_price
                    if move.prodlot_id:
                        cost_price = move.prodlot_id.cost_price
                    
                    datas[(prod_id, lot_id)] = {'product_id': prod_id, 'location_id': location, 'product_qty': qty, 'product_uom': move.product_id.uom_id.id, 'prod_lot_id': lot_id, 'expiry_date': move.prodlot_id.ref,'unit_price':cost_price}

            if datas:
                flag = True
                res[location] = datas

        if not flag:
            raise osv.except_osv(_('Warning!'), _('No product in this location. Please select a location in the product form.'))


        for stock_move in res.values():
            for stock_move_details in stock_move.values():
                stock_move_details.update({'inventory_id': context['active_ids'][0]})
                domain = []
                for field, value in stock_move_details.items():
                    if field == 'product_qty' and fill_inventory.set_stock_zero:
                         domain.append((field, 'in', [value,'0']))
                         continue
                    domain.append((field, '=', value))

                line_ids = inventory_line_obj.search(cr, uid, domain, context=context)

                if not line_ids:
                    if stock_move_details['product_qty'] != 0:
                        if fill_inventory.set_stock_zero:
                            stock_move_details.update({'product_qty': 0})
                        inventory_line_obj.create(cr, uid, stock_move_details, context=context)

        return {'type': 'ir.actions.act_window_close'}

class stock_inventory(osv.Model):
    _inherit = 'stock.inventory'
    _order = 'date desc'
    
    _columns = {
        'inventory_line_id': one2many_sorted.one2many_sorted(
            'stock.inventory.line', 
            'inventory_id', 
            'Inventories', 
            readonly=True, 
            states={'draft': [('readonly', False)]},
            order='product_name.upper()',
            ),
        'move_id_in' : fields.many2one('account.move','Move Id'),
        'move_id_out' : fields.many2one('account.move','Move Out'),
        'currency_id' : fields.many2one('res.currency','Currency'),
        'location_id' : fields.many2one('stock.location','Location',domain=[('available_in_selection','=',True),('usage','=','internal')]),
    }
    
    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
    
    def action_confirm(self, cr, uid, ids, context=None):
        """ Confirm the inventory and writes its finished date
        @return: True
        """
        if context is None:
            context = {}
        # to perform the correct inventory corrections we need analyze stock location by
        # location, never recursively, so we use a special context
        product_context = dict(context, compute_child=False)
        stock_enabled = self._get_enable_stock(cr, uid)
        
        company_curr_id = self._get_company_currency_id(cr, uid, context=context)

        currency_pool = self.pool.get('res.currency')
        location_obj = self.pool.get('stock.location')
        lot_obj = self.pool.get('stock.production.lot')
        line_obj = self.pool.get('stock.inventory.line')
        money_in = 0.0
        money_out = 0.0
        name = False
        date=False
        location_id = False
        lots = []
        duplicated = []
        for inv in self.browse(cr, uid, ids, context=context):
            name = inv.name
            date = inv.date
            move_ids = []
            for line in inv.inventory_line_id:
                if line.prod_lot_id not in lots:
                    lots.append(line.prod_lot_id)
                else:
                    name = line.product_id.name_template
                    if name not in duplicated:
                        duplicated.append(name)
                        
                pid = line.product_id.id
                product_context.update(uom=line.product_uom.id, to_date=inv.date, date=inv.date, prodlot_id=line.prod_lot_id.id)
                amount = location_obj._product_get(cr, uid, line.location_id.id, [pid], product_context)[pid]
                change = line.product_qty - amount
                    
                if _debug:
                    _logger.debug('from_date : %s', product_context['date'])
                    _logger.debug('to_date : %s', product_context['to_date'])
                    _logger.debug('line.product_qty : %s', line.product_qty)
                    _logger.debug('amount : %s', amount)
                    _logger.debug('change : %s', change)
                
                # Convert cost_price 
                cost_price = line.unit_price
                if inv.currency_id.id != company_curr_id:
                    cost_price = currency_pool.compute(cr, uid, inv.currency_id.id, company_curr_id, line.unit_price, context=context)
                
                lot_id = False
                if line.prod_lot_id:
                    if _debug:
                        _logger.debug('line.prod_lot_id : %s', line.prod_lot_id)
                        _logger.debug('line.prod_lot_id.id : %s', line.prod_lot_id.id)
                    lot_id = line.prod_lot_id.id
                    if lot_id:
                        lot_obj.write(cr, uid, [lot_id], {'cost_price' : cost_price, 'active' : True}, context=None)
                else:
                    if _debug:
                        _logger.debug('ids : %s', ids)
                        _logger.debug('pid : %s', pid)
                        _logger.debug('expiry_date : %s', line.expiry_date)
                        _logger.debug('creation_date : %s', inv.date)
                    lot_id = self.create_serial(cr, uid, ids, pid, line.expiry_date, inv.date, cost_price)
                    line.prod_lot_id = lot_obj.browse(cr, uid, [lot_id])[0]
                    line_obj.write(cr, uid, [line.id], {'prod_lot_id' : lot_id}, context=None)
                    if _debug:
                        _logger.debug('line.prod_lot_id : %s', line.prod_lot_id)
                        _logger.debug('line.prod_lot_id.id : %s', line.prod_lot_id.id)
                    #raise osv.except_osv(_('Warning!'), _('Brake.'))
                
                if change:
                    location_id = line.product_id.property_stock_inventory.id
                    value = {
                        'name': _('INV:') + (line.inventory_id.name or ''),
                        'product_id': line.product_id.id,
                        'product_uom': line.product_uom.id,
                        'prodlot_id': lot_id,
                        'date': inv.date,
                        'date_expected' : inv.date,
                    }

                    if change > 0:
                        value.update( {
                            'product_qty': change,
                            'location_id': location_id,
                            'location_dest_id': line.location_id.id,
                        })
                        money_in += (change * line.unit_price)
                    else:
                        value.update( {
                            'product_qty': -change,
                            'location_id': line.location_id.id,
                            'location_dest_id': location_id,
                        })
                        money_out += (change * line.unit_price)
                        
                    move_ids.append(self._inventory_line_hook(cr, uid, line, value))
            
            if _debug:
                _logger.debug('==> lots : %s', lots)
                _logger.debug('==> duplicated : %s', duplicated)
                
            if len(duplicated) > 0:
                raise osv.except_osv(_('Warning!'), _('Repeated products:\n\n %s \n\nPlease combine quantities.') % ('\n'.join(duplicated)))
        
            self.write(cr, uid, [inv.id], {'state': 'confirm', 'move_ids': [(6, 0, move_ids)]})
            self.pool.get('stock.move').action_confirm(cr, uid, move_ids, context=context)
            
            if stock_enabled:
                if money_in > 0:
                    self._manage_money_on_inventory(cr, uid, ids, location_id, name, money_in,date,'in')
                if money_out > 0:
                    self._manage_money_on_inventory(cr, uid, ids, location_id, name, money_out,date,'out')
        
        return True
        
    def action_done(self, cr, uid, ids, context=None):
        """ Finish the inventory
        @return: True
        """
        if context is None:
            context = {}
        else:
            context = context.copy()
        move_obj = self.pool.get('stock.move')
        for inv in self.browse(cr, uid, ids, context=context):
            context['force_date'] = inv.date
            move_obj.action_done(cr, uid, [x.id for x in inv.move_ids], context=context)
            self.write(cr, uid, [inv.id], {'state':'done', 'date_done': time.strftime('%Y-%m-%d %H:%M:%S')}, context=context)
               
        #Remove empty Serial Number created by mistake
        lot_obj = self.pool.get('stock.production.lot')
        lot_ids = lot_obj.search(cr, uid, [])
        all_ids = lot_obj.browse(cr, uid, lot_ids)
        index = 1
        for lot in all_ids:
            if lot.stock_available == 0:
                if _debug:
                    _logger.debug('empty id [%s]: %s ==> "active" = False', index, lot.id)
                #lot_obj.write(cr, uid, lot.id, {'active': False})
                index += 1
        
        return True


    def _generate_cost_of_items_moves(self, cr, uid, move_line_pool,name, cost, move_cost_id, cost_of_item_sold_account_id, 
        stock_account_id, analytic_account_id, stock_analytic_account_id, context=None):
        move_line_cost1 = {
                'analytic_account_id': analytic_account_id, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : name,
                'name': 'Cost Of Items',
                'currency_id': False,
                'credit': 0.0,
                'debit': cost,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_cost_id,
                'account_id': cost_of_item_sold_account_id,
                'state' : 'valid',
            }
        
        move_line_cost2 = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : name,
                'name': 'Cost Of Items',
                'currency_id': False,
                'credit': cost,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_cost_id,
                'account_id': stock_account_id,
                'state' : 'valid',
            }
    
        res1 = move_line_pool.create(cr, uid, move_line_cost1,context=context,check=False)
        res2 = move_line_pool.create(cr, uid, move_line_cost2,context=context,check=False)
        
        return res1 & res2
        
    def get_default_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','account_id')
    
    def get_default_inventory_account_id(self, cr, uid):
        return  self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','inventory_account_id')
        
    def get_default_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','journal_id')
    
    def _get_enable_stock(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','enable_stock')
    
    def _manage_money_on_inventory(self,cr, uid, ids, location_id, name, money, date,move_type):
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        location_obj = self.pool.get('stock.location')
        location = location_obj.browse(cr, uid, [location_id])[0]
        journal_id = self.get_default_journal_id(cr, uid)
        
        if move_type == 'in':   
            src_account_id = self.get_default_inventory_account_id(cr, uid)
            dest_account_id = self.get_default_account_id(cr, uid)
        else:
            dest_account_id = self.get_default_inventory_account_id(cr, uid)
            src_account_id = self.get_default_account_id(cr, uid)
        
        move_cost = move_pool.account_move_prepare(cr, uid, journal_id, date, ref=name)
        move_id = move_pool.create(cr, uid, move_cost)
        self._generate_cost_of_items_moves(cr, uid, move_line_pool, 'Inventory Adjustment', money, move_id, dest_account_id, src_account_id, location.analytic_account_id.id, location.analytic_account_id.id, context=None)
                    
        if move_type == 'in':
            self.write(cr, uid, ids , {'move_id_in' : move_id})
        if move_type == 'out':
            self.write(cr, uid, ids , {'move_id_out' : move_id})

        return True
        
    def action_cancel_draft(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        stock_enabled = self._get_enable_stock(cr, uid)
        
        if stock_enabled:
            move_obj = self.pool.get('account.move')
            
            move_ids = []
            if data.move_id_in:
                move_ids.append(data.move_id_in.id)
            
            if data.move_id_out:
                move_ids.append(data.move_id_out.id)
            
            move_obj.button_cancel(cr, uid, move_ids)
            move_obj.unlink( cr, uid, move_ids, context=None, check=False)
        
        
        return super(stock_inventory,self).action_cancel_draft(cr, uid, ids, context=context)
    
    
    def action_cancel_inventory(self, cr, uid, ids, context=None):
        
        # Hide Serial Numbers linked to the inventory
        # WARNING: What happens if the Serial Number is already used before the inventory?
        # ANSWER: not possible, every inventory creates new Serial Numbers as fresh
#         for inv in self.browse(cr, uid, ids, context=context):
#             self.pool.get('stock.production.lot').write(cr, uid, [x.prodlot_id.id for x in inv.move_ids], {'active':false})
        
        return super(stock_inventory,self).action_cancel_inventory(cr, uid, ids, context=context)
            

#     def create_prod_lot_id(self, cr, uid, ids, product_id, expiry_date, price, creation_date):
#         lot_id = False
# 
#         if _debug:
#             _logger.debug('Expiry date : ',expiry_date)
# 
#         product_obj = self.pool.get('product.product')
#         product_ids = product_obj.search(cr, uid, [('id','=',product_id)])
#         product = product_obj.browse(cr, uid, product_ids)[0]
#         
#         isf_lib = self.pool.get('isf.lib.utils')
#         lot_obj = self.pool.get('stock.production.lot')
#         
#         lot_ids = lot_obj.search(cr, uid, [('product_id','=',product_id),('ref','=',expiry_date)])
# 
#         
#         if len(lot_ids) == 0:
#             lot_id = isf_lib.create_lot_id_with_expiry_date(cr, uid, product, expiry_date,creation_date)
#         else:
#             lot = lot_obj.browse(cr, uid, lot_ids)[0]
#             lot_id = lot.id
# 
#         if lot_id:
#             isf_lib.set_cost_price(cr, uid, product_id, lot_id, price)
# 
#         return lot_id
# 
    def create_serial(self, cr, uid, ids, product_id, expiry_date, creation_date, cost_price):
        lot_id = False
        
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('id','=',product_id)])
        product = product_obj.browse(cr, uid, product_ids)[0]
        
        isf_lib = self.pool.get('isf.lib.utils')
        lot_obj = self.pool.get('stock.production.lot')
        lot_ids = []
        
        lot_ids = lot_obj.search(cr, uid, [('product_id','=',product_id),('ref','=',expiry_date)])

        
        if len(lot_ids) == 0:
            lot_id = isf_lib.create_lot_id_with_expiry_date(cr, uid, product, expiry_date, creation_date)
            lot_obj.write(cr, uid, [lot_id], {'cost_price' : cost_price, 'active' : True}, context=None)
        else:
            lot = lot_obj.browse(cr, uid, lot_ids)[0]
            lot_id = lot.id

        return lot_id
   
stock_inventory()     
        
class stock_inventory_line(osv.Model):
    _inherit = 'stock.inventory.line'

    def _get_total_price(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line_id in ids:
            line = self.browse(cr, uid, [line_id])[0]
            quantity = line.product_qty / line.product_uom.factor
            res[line.id] = line.unit_price * quantity
            
        return res
    
    _columns = {
        'product_name' : fields.related('product_id','name_template',readonly=True,type="char",relation="product.product",string="Product Name",store=True),
        'expiry_date' : fields.date('Expiry Date'),
        'date' : fields.date('Creation Date'),
        'unit_price' : fields.float('Unit Price'),
        'price' : fields.function(_get_total_price,type="float",string='Price'),
    }
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(stock_inventory_line, self).default_get(cr, uid, fields, context=context)
        
        if _debug:
            _logger.debug('==> in default_get')
            
        if context.get('inv_location_id'):
            res['location_id'] = context.get('inv_location_id')
            res['date'] = context.get('creation_date')
        return res
        
    def on_change_product_id(self, cr, uid, ids, location_id,product_id,product_uom, date, context=None):
        result = super(stock_inventory_line, self).on_change_product_id(cr, uid, ids, location_id,product_id,product_uom, date)
        
        if _debug:
            _logger.debug('==> in on_change_product_id')
            _logger.debug('==> in result : %s', result)
            
        if product_id:
            stock_o = self.pool.get('stock.production.lot')
            product = self.pool.get('product.product').browse(cr, uid, [product_id])[0]
#             stock_ids = stock_o.search(cr, uid, [('product_id','=',product_id),('cost_price','>',0)],order='life_date',limit=1)
#             if len(stock_o.browse(cr, uid, stock_ids)) > 0:
#                 stock = stock_o.browse(cr, uid, stock_ids)[0]
#                 if stock is not None:
#                     result['value'].update({
#                         'unit_price' : stock.cost_price,
#                         'product_uom' : stock.product_id.uom_po_id.id,
#                     })
#             else:
#                 result['value'].update({
#                         'unit_price' : product.standard_price,
#                         'product_uom' : product.uom_po_id.id,
#                     })
            
            
            result['value'].update({
                'product_uom' : product.uom_po_id.id,
                    })
            
            #TODO: find the reference UoM 
            reference_product_id = 1
            result['domain'] = {'product_uom' : ['|',('id','=',product.uom_po_id.id),('id','=',reference_product_id)]}
        return result
    
    def on_change_product_lot_id(self, cr, uid, ids, prod_lot_id, context=None):
#         result = super(stock_inventory_line, self).on_change_product_lot_id(cr, uid, ids, prod_lot_id)
        result = {'value' : {} }        
        if _debug:
            _logger.debug('==> in on_change_product_lot_id')
            
        stock_o = self.pool.get('stock.production.lot')
        stock = stock_o.browse(cr, uid, prod_lot_id)
        result['value'].update({
                    'unit_price' : stock.cost_price,
                })
        if _debug:
            _logger.debug('==> result : %s', result)
        return result
    
#     def create_serial(self, cr, uid, ids, product_id, expiry_date, creation_date):
#         lot_id = False
#         
#         product_obj = self.pool.get('product.product')
#         product_ids = product_obj.search(cr, uid, [('id','=',product_id)])
#         product = product_obj.browse(cr, uid, product_ids)[0]
#         
#         isf_lib = self.pool.get('isf.lib.utils')
#         lot_obj = self.pool.get('stock.production.lot')
#         lot_ids = []
#         
#         lot_ids = lot_obj.search(cr, uid, [('product_id','=',product_id),('ref','=',expiry_date)])
# 
#         
#         if len(lot_ids) == 0:
#             lot_id = isf_lib.create_lot_id_with_expiry_date(cr, uid, product, expiry_date, creation_date)
#         else:
#             lot = lot_obj.browse(cr, uid, lot_ids)[0]
#             lot_id = lot.id
#             #raise osv.except_osv(_('Warning!'), _('Expiry Date for the same product already present.'))
#             self.log(cr, uid, id, _('Expiry Date for the same product already present.'))
# 
#         return lot_id
# 
    def onchange_expiry_date(self, cr, uid, ids, product_id, expiry_date, creation_date, context=None):
        result = {'value':{}}
        
        if _debug:
            _logger.debug('==> in onchange_expiry_date')
            
#         lot_id = self.create_serial(cr, uid, ids, product_id, expiry_date, creation_date)
#          
#         result['value'].update({
#             'prod_lot_id' : lot_id,
#         })
         
        return result
        #return {'warning':{'title':'Warning', 'message':'Same product with same expiring date already present'}}


class stock_production_lot(osv.osv):
    _inherit = 'stock.production.lot'
     
    _columns = {
        'qty' : fields.integer('Qty',store=True),
        'price' : fields.float('Price'),
        'cost_price' : fields.float('Cost Price'),
        'active' : fields.boolean('Active'),
    }
 
    _order = 'product_id,ref asc'
     
     
    _defaults = {
        'qty' : 0,
        'price' : 0,
        'cost_price' : 0,
        'active' : True,
    }
          
    def onchange_expiry_date(self, cr, uid, ids, expiry_date, context=None):
        result = {'value' : {}}
        date_str = str(expiry_date)
        date = date_str[:10]
         
        result['value'].update({
            'ref' : date,
        })
         
        return result
 
stock_production_lot()
    
# class stock_picking_in(osv.osv):
#     _inherit = 'stock.picking.in'
# 
#     def action_process(self, cr, uid, ids, context=None):
#         if context is None:
#             context = {}
#         
#         data = self.browse(cr, uid, ids)[0]
#         
#         """Open the partial picking wizard"""
#         context.update({
#             'active_model': self._name,
#             'active_ids': ids,
#             'active_id': len(ids) and ids[0] or False,
#             'order_id' : data.purchase_id.name,
#         })
#         return {
#             'view_type': 'form',
#             'view_mode': 'form',
#             'res_model': 'stock.partial.picking',
#             'type': 'ir.actions.act_window',
#             'target': 'new',
#             'context': context,
#             'nodestroy': True,
#         }
# 
# stock_picking_in()


# class stock_picking(osv.osv):
#     _inherit = 'stock.picking'
#     
#     def create(self, cr, user, vals, context=None):
#         isf_lib = self.pool.get('isf.lib.utils')
#         
#         if vals['type'] == 'in':
#             purchase_obj = self.pool.get('purchase.order')
#             currency_obj = self.pool.get('res.currency')
#             purchase_ids = purchase_obj.search(cr, user, [('id','=',vals['purchase_id'])])
#             purchase = purchase_obj.browse(cr, user, purchase_ids)[0]
#             company_curr_id = isf_lib.get_company_currency_id(cr, user)
#         
#             for line in purchase.order_line: 
#                 lot_id = isf_lib.create_lot_id(cr, user, line.product_id, line.date_order, purchase.document_number, purchase.name)
#                 if _debug:
#                     _logger.debug('Product[%s] Date[%s] LotId[%s]',line.product_id.name_template, line.date_order, lot_id)
#                 
#                 # Save the product price on the purchase price history
#                 price = line.price_unit
#                 if company_curr_id != purchase.order_currency_id.id:
#                     curr_price = currency_obj.compute(cr, user, purchase.order_currency_id.id, company_curr_id, price, context=context)
#                 else:
#                     curr_price = price
#                 isf_lib.set_cost_price(cr, user, line.product_id.id, lot_id, curr_price)
#         
#             stock_id = super(stock_picking,self).create(cr, user, vals, context=None)
#             return stock_id
#         else:
#             return super(stock_picking,self).create(cr, user, vals, context=context)
#         
#     
#         
# stock_picking()

class stock_return_picking(osv.osv_memory):
    _inherit = 'stock.return.picking'
    
    def view_init(self, cr, uid, fields_list, context=None):
        """
         Creates view dynamically and adding fields at runtime.
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param context: A standard dictionary
         @return: New arch of view with new columns.
        """
        if context is None:
            context = {}
        res = super(stock_return_picking, self).view_init(cr, uid, fields_list, context=context)
        record_id = context and context.get('active_id', False)
        if record_id:
            pick_obj = self.pool.get('stock.picking')
            pick = pick_obj.browse(cr, uid, record_id, context=context)
            if pick.state not in ['done','confirmed','assigned']:
                raise osv.except_osv(_('Warning!'), _("You may only return pickings that are Confirmed, Available or Done!"))
            valid_lines = 0
            return_history = self.get_return_history(cr, uid, record_id, context)
            for m  in pick.move_lines:
                if m.state == 'done' and m.product_qty > return_history.get(m.id, 0):
                    valid_lines += 1
            if not valid_lines:
                raise osv.except_osv(_('Warning!'), _("No products to return (only lines in Done state and not fully returned yet can be returned)!"))
        return res
    
    def get_return_history(self, cr, uid, pick_id, context=None):
        """ 
         Get  return_history.
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param pick_id: Picking id
         @param context: A standard dictionary
         @return: A dictionary which of values.
        """
        pick_obj = self.pool.get('stock.picking')
        pick = pick_obj.browse(cr, uid, pick_id, context=context)
        return_history = {}
        for m  in pick.move_lines:
            if m.state == 'done':
                return_history[m.id] = 0
                for rec in m.move_history_ids2:
                    # only take into account 'product return' moves, ignoring any other
                    # kind of upstream moves, such as internal procurements, etc.
                    # a valid return move will be the exact opposite of ours:
                    #     (src location, dest location) <=> (dest location, src location))
                    if rec.location_dest_id.id == m.location_id.id \
                        and rec.location_id.id == m.location_dest_id.id:
                        return_history[m.id] += rec.product_qty #(rec.product_qty * rec.product_uom.factor)
        return return_history


class stock_partial_picking_line(osv.osv_memory):
    _inherit = 'stock.partial.picking.line'
    
#     
#     _columns = {
#         'expiry_date' : fields.related('prodlot_id','life_date',type="date",relation="stock.production.lot",
#             string='Expiry Date', required=False, select=True, store=True),
#     }
#     
#     def onchange_expiry_date(self, cr, uid, ids, product_id,expiry_date, context=None):
#         if context is None:
#             context = {}
#           
#         if _debug:
#             _logger.debug('====> expiry_date : %s', expiry_date)
#               
#         result = {'value' : {}}
#         isf_lib = self.pool.get('isf.lib.utils')
#         product_obj = self.pool.get('product.product')
#         product_ids = product_obj.search(cr, uid, [('id','=',product_id)])
#         product = product_obj.browse(cr, uid, product_ids)[0] or False
#   
#         lot_id = isf_lib.create_lot_id_with_expiry_date(cr, uid, product, expiry_date)
#   
#         result['value'].update({
#             'prodlot_id' : lot_id,
#             })
#   
#         return result
#     
    def _check_prodlot_id(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
         
        if not data.prodlot_id:
            return False
        
        return True
    
    def _check_prodlot_life_date(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
         
        if not data.prodlot_id.life_date:
            return False
        
        return True
     
    _constraints = [
        (_check_prodlot_id, 'You must insert a serial number for each products', ['prodlot_id']),
        (_check_prodlot_life_date, 'You must define an expiring date for each serial number', ['prodlot_id']),
    ]
 
stock_partial_picking_line()

class stock_partial_picking(osv.osv_memory):
    _inherit = 'stock.partial.picking'
    
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        #override of fields_view_get in order to change the label of the process button and the separator accordingly to the shipping type
        if context is None:
            context={}
        _logger.debug(context)
        res = super(stock_partial_picking, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
        type = context.get('active_model', False)
        if type:
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//button[@name='do_partial']"):
                if type == 'stock.picking.in':
                    node.set('string', _('_Receive'))
                elif type == 'stock.picking.out':
                    node.set('string', _('_Deliver'))
            for node in doc.xpath("//separator[@name='product_separator']"):
                if type == 'stock.picking.in':
                    node.set('string', _('Receive Products'))
                elif type == 'stock.picking.out':
                    node.set('string', _('Deliver Products'))
            res['arch'] = etree.tostring(doc)
        return res
#     
#     def default_get(self, cr, uid, fields, context=None):
#         isf_lib = self.pool.get('isf.lib.utils')
#         res = super(stock_partial_picking, self).default_get(cr, uid, fields, context=context)
#         if _debug:
#             _logger.debug('====> Context : %s', context)
#             _logger.debug('====> Res : %s', res)
#             
#         order_id = context.get('order_id')
#         if 'move_ids' in res:
#             for move in res['move_ids']:
#                 lot = isf_lib.get_lot_by_order(cr, uid, move['product_id'], order_id)
#                 move.update({
#                     'prodlot_id' : lot.id or False,
#                     'expiry_date' : lot.life_date or False,
#                 })
#                 if _debug:
#                     _logger.debug('Move : %s', move)
#                 
#             
#         return res
#         
    def do_partial(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        isf_lib = self.pool.get('isf.lib.utils')
        prodlot_obj = self.pool.get('stock.production.lot')
        company_curr_id = isf_lib.get_company_currency_id(cr, uid)
        currency_obj = self.pool.get('res.currency')
        
        for line in data.move_ids:
            price = line.cost
            if line.currency:
                curr_price = currency_obj.compute(cr, uid, line.currency.id, company_curr_id, price, context=context)
            else:
                curr_price = price
            isf_lib.set_cost_price(cr, uid, line.product_id.id, line.prodlot_id.id, curr_price)
#             _logger.debug('expiry_date : %s', line)
            #isf_lib.set_expiry_date(cr, uid, line.prodlot_id.id, line.prodlot_id.life_date)
        return super(stock_partial_picking,self).do_partial(cr, uid, ids, context=context)
         
stock_partial_picking()

class isf_stock_product_line_batch(osv.TransientModel):
    _name = 'isf.stock.product.line.batch'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Batch'),
        'product_id' : fields.many2one('product.product','Product'),
        'product_name' : fields.related('product_id','name_template',readonly=True,type="char",relation="product.product",string="Product Name",store=False),
        'qty' : fields.float('Qty'),
        'stock_line_id': fields.integer('Stock Id'),
    }

class isf_stock_product_line_select(osv.TransientModel):
    _name = 'isf.stock.product.line.select'
    
    _columns = {
        'product_line_ids' : fields.one2many('isf.stock.product.line.batch','stock_line_id','Products',required=False),
        'location_id' : fields.many2one('stock.location','Source Location',required=False),
        'statement_id' : fields.integer('Statement Id'),
    }
    
    def _pre_load_batch(self, cr, uid,context=None):
        if _debug:
            _logger.debug('CTX : %s', context)
        lot_o = self.pool.get('stock.production.lot')
        lot_ids = lot_o.search(cr, uid, [('stock_available','>=',1.0)], context=context) #workaround for qty close to zero
        if _debug:
            _logger.debug('IDS : %s', lot_ids)
        
        batch_o = self.pool.get('isf.stock.product.line.batch')
        line_id = context.get('statement_id')
        for lot in lot_o.browse(cr, uid, lot_ids, context=context):
            if lot.stock_available >= 1.0: #workaround for qty close to zero
                res = batch_o.search(cr, uid, [('stock_id','=',lot.id),('stock_line_id','=',line_id)])
                if _debug:
                    _logger.debug('RES : %s',res)
                if len(res) == 0:
                    batch_o.create(cr, uid, {
                        'stock_id' : lot.id,
                        'product_id' : lot.product_id.id,
                        'qty': lot.stock_available,
                        'stock_line_id' : line_id,
                    })                
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(isf_stock_product_line_select, self).default_get(cr, uid, fields, context=context)
            
        statement_id = context.get('statement_id')
        data_o = self.pool.get('isf.stock.moves')
        statement_id = context.get('active_id')
        data_ids = data_o.search(cr, uid, [('id','=',statement_id)])
        data = data_o.browse(cr, uid, data_ids)[0]
        res['location_id'] = data.location_source_id.id
        res['statement_id'] = statement_id
        
        ctx = context.copy()
        ctx.update({
            'location_id' : data.location_source_id.id,
            'statement_id' : data.id,
        })
    
        stock_o = self.pool.get('isf.stock.product.line.batch')
        stock_ids = stock_o.search(cr, uid, [('stock_line_id','=',data.id)])
        
        for stock in stock_o.browse(cr, uid, stock_ids):
            stock_o.unlink(cr, uid, stock.id, context=context)
        
        self._pre_load_batch(cr, uid, context=ctx)
        
        return res
    
    def import_product(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        
        if context is None:
            context = {}
        context.update({
            'location_id' : data.location_id.id
        })
    
        line_obj = self.pool.get('isf.stock.moves.line')
        stock_o = self.pool.get('stock.production.lot')
        stock_ids = []
        for line in data['product_line_ids']:
            stock_ids.append(line.stock_id.id)
            
        active_id = context.get('active_id')
        state = context.get('state')
        
        
        now = datetime.datetime.now()
        
        for stock in stock_o.browse(cr, uid, stock_ids, context=context):
            expired = False
            exp = datetime.datetime.strptime(stock.life_date, "%Y-%m-%d %H:%M:%S") #datetime.datetime.strptime(stock.ref, "%Y-%m-%d")
            if exp < now:
                expired = True
                
            if _debug:
                _logger.debug('STOCK : Context : %s', context)
                _logger.debug('STOCK : Available : %s', stock.stock_available)
                
            line_id = line_obj.create(cr, uid, {
                'stock_id' : stock.id,
                'expiry_date' : stock.ref,
                'product_id' : stock.product_id.id,
                'stock_available' : stock.stock_available,
                'quantity' : 0,
                'expired' : expired,
                'statement_id' : active_id,
                'state' : state,
                'uom_id' : stock.product_id.uom_id.id,
            }, context=context)
                
        return True


class isf_stock_moves_line(osv.Model):
    _name = 'isf.stock.moves.line'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Lot Id'),
        'expiry_date' : fields.char('Expiry Date'),
        'product_id' : fields.many2one('product.product','Product'),
        'product_uom' : fields.related('product_id','uom_po_id',readonly=True, type='many2one',relation='product.uom', string='PUoM'),
        'stock_available' : fields.integer('Available'),
        'required' : fields.integer('Required'),
        'quantity' : fields.integer('Quantity'),
        'expired' : fields.boolean('Expired'),
        'statement_id' : fields.many2one('isf.stock.moves',string="Statement Id",select=True),
        'move_id' : fields.many2one('stock.move','id'),
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
        'uom_id' : fields.many2one('product.uom','UoM'),
    }
    
class isf_stock_moves(osv.Model):
    _name = 'isf.stock.moves'
    _order = 'date desc'
    
    _columns = {
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
        'name' : fields.char('Description', size=128, required=True),
        'ref' : fields.char('Document Number', size=32, required=True),
        'date' : fields.date('Date', required=True),
        'location_source_id' : fields.many2one('stock.location','Source Location',required=True,domain=[('available_in_selection','=',True)]),
        'location_source_name' : fields.related('location_source_id','name',readonly=True, type='char',relation='stock.location', string='Source Location'),
        'location_dest_id' : fields.many2one('stock.location','Destination Location',required=True,domain=[('available_in_selection','=',True)]),
        'location_dest_name' : fields.related('location_dest_id','name',readonly=True, type='char',relation='stock.location', string='Dest Location'),
        'line_ids' : fields.one2many('isf.stock.moves.line','statement_id','Products'),
        'sequence_id' : fields.char('ID',size=32),
    }
    
    _defaults = {
        'state' : 'draft',
    }
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','INT')])
        if len(seq_ids) == 0:
            self.create_sequence(cr, uid, vals)
            seq_ids = obj_sequence.search(cr, uid, [('name','=','INT')])
        
        period_ids = obj_period.find(cr, uid, vals['date'], context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
        
        vals['sequence_id'] = new_seq
        return super(isf_stock_moves, self).create(cr, uid, vals, context)
    
    def create_sequence(self, cr, uid, vals, context=None):
        """ Create new no_gap entry sequence for every new Joural
        """
        # in account.journal code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = '1'
        
        seq = {
            'name': 'INT',
            'implementation':'no_gap',
            'prefix': "INT/%(year)s/",
            'padding': 4,
            'number_increment': 1
        }
        return self.pool.get('ir.sequence').create(cr, uid, seq)
    
    def get_default_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','journal_id')
        
    def get_default_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','account_id')
    
    
    def _save(self, cr, uid, ids, context=None):
        isf_lib = self.pool.get('isf.lib.utils')
        data = self.browse(cr, uid, ids)[0]
        
        journal_id = self.get_default_journal_id(cr, uid)
        account_id = self.get_default_account_id(cr, uid)
        
        if not data.sequence_id:
            obj_period = self.pool.get('account.period')
            obj_sequence = self.pool.get('ir.sequence')
            seq_ids = obj_sequence.search(cr, uid, [('name','=','INT')])
            period_ids = obj_period.find(cr, uid, data.date, context=context)
            period = obj_period.browse(cr, uid, period_ids)[0]
            ctx = context.copy()
            ctx.update({
                'fiscalyear_id': period.fiscalyear_id.id,
            })
            new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
            self.write(cr, uid, ids, {'sequence_id' : new_seq}, context=ctx)
            origin = new_seq
        else:
            origin = data.sequence_id
        
        line_o = self.pool.get('isf.stock.moves.line')
        
        for line in data.line_ids:
            cost_price = line.stock_id.cost_price * line.quantity
            if cost_price == 0.0:
                cost_price = line.product_id.list_price * line.quantity
                
                
            move_id = isf_lib._generate_stock_move(cr, uid, data.name, data.ref, data.location_source_id.id,data.location_dest_id.id, line.product_id, line.quantity,  data.date, 'internal',line.stock_id.id, origin,uom_id=line.uom_id, context=context)
            line_o.write(cr, uid, line.id, {'move_id' : move_id, 'state' : 'done'}, context=context)
        
        self.write(cr, uid, ids, {'state' : 'done' }, context=context)
        
        return True
    
    def _update_stock_available(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        updated = False
        
        # Adding location to context in order to have stock_available per location
        ctx = context.copy()    
        ctx.update({
            'location_id' : data.location_source_id.id
        })
        
        stock_move_o = self.pool.get('isf.stock.moves.line')
        for line in data.line_ids:
            if _debug:
                _logger.debug('context: %s', ctx)
                _logger.debug('line.id: %s', line.id)
                _logger.debug('lot_id: %s', line.stock_id.id)
                _logger.debug('product: %s', line.product_id.name_template)
                
            lot_o = self.pool.get('stock.production.lot')
            lot_id = lot_o.browse(cr, uid, line.stock_id.id, ctx)
            
            old_available = line.stock_available
            real_available = int(lot_id.stock_available)
            
            if old_available != real_available:
                if _debug:    
                    _logger.debug('old stock_available: %s', old_available)
                    _logger.debug('real stock_available at location: %s', real_available)
                    _logger.debug('updating stock_available!')
                stock_move_o.write(cr, uid, line.id, {'stock_available' : real_available})
                updated = True
                
        return updated
    
    def check(self, cr, uid, ids, context=None):
        self._update_stock_available(cr, uid, ids, context)
        
        return {'type': 'ir.actions.act_window_close'}
    
    def confirm(self, cr, uid, ids, context=None):
        if self._update_stock_available(cr, uid, ids, context):
            raise osv.except_osv(_('Warning!'), _('Some availability are changed'))
        else:
            self._confirm(cr, uid, ids, context=context)
        return {'type': 'ir.actions.act_window_close'}
    
    def _confirm(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        
        if not data.line_ids:
            raise osv.except_osv(_('Warning!'), _('You must insert at least one product.'))
        
        lots = []
        duplicated = []
        for line in data.line_ids:
            if line.stock_id not in lots:
                lots.append(line.stock_id)
            else:
                name = line.product_id.name_template
                if name not in duplicated:
                    duplicated.append(name)
            
            quantity = line.quantity
            if quantity == 0:
                raise osv.except_osv(_('Warning!'), _('Zero (0) quantities not allowed.'))
            
            issued = quantity / line.uom_id.factor
            if issued > line.stock_available:
                raise osv.except_osv(_('Warning!'), _('Issuing quantity %s for product %s exceed the available quantity %s') % (issued, line.product_id.name, line.stock_available))
            
        if len(duplicated) > 0:
            raise osv.except_osv(_('Warning!'), _('Repeated products:\n\n %s \n\nPlease combine quantities.') % ('\n'.join(duplicated)))
        
        self._save(cr, uid, ids, context=context)
        return {'type': 'ir.actions.act_window_close'}
        
    def set_to_draft(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        stock_move_o = self.pool.get('stock.move')
        line_o = self.pool.get('isf.stock.moves.line')
        
        #
        # Remove stock moves
        #
        for line in data.line_ids:
            stock_move_o.write(cr, uid, line.move_id.id, {'state' : 'cancel'}, context=context)
            line_o.write(cr, uid, line.id, {'state' : 'draft'}, context=context)
            
        #
        # Remove journal entry (cost of items move)
        #
        #move_ids = []
        #if data.move_cost_id:
        #    move_ids.append(data.move_cost_id.id)
        
        #move_o.write(cr, uid, move_ids, {'state':'draft'}, context=context)
        #move_o.unlink( cr, uid, move_ids, context=None, check=False)
            
        self.write(cr, uid, ids, {'state' : 'draft'}, context=context)
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        stock_moves = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in stock_moves:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a stock move entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True

isf_stock_moves()

####################################################################################################################################
############################################# STOCK DONATION #######################################################################
####################################################################################################################################


class isf_stock_donation_line(osv.Model):
    _name = 'isf.stock.donation.line'
    
    _columns = {
        'product_id' : fields.many2one('product.product','Product',required=True),
        'quantity' : fields.integer('Quantity', required=True),
        'uom_id' : fields.many2one('product.uom','UoM'),
        'price' : fields.float('Price', required=True),
        'total_price' : fields.float('Total Price', required=False),
        'expiry_date' : fields.date('Expiry Date', required=False),
        'line_id': fields.many2one('isf.stock.donation', 'Line', select=True),
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
        'move_id' : fields.many2one('stock.move','id'),
    }
    
    def onchange_price(self, cr, uid, ids, qty, price, context=None):
        result = {'value' : {}}
        
        total_price = qty * price
        result['value'].update({
            'total_price' : total_price,
        })
        
        return result

    def onchange_product(self, cr, uid, ids, product_id, context=None):
        result = {'value' : {}}
        prod_obj = self.pool.get('product.product')
        prod_ids = prod_obj.search(cr, uid, [('id','=',product_id)])
        if prod_ids:
            prod = prod_obj.browse(cr, uid, prod_ids)[0]
            result['value'].update({
                'uom_id' : prod.uom_id.id,
                })

        return result;

class isf_stock_donation(osv.Model):
    _name = 'isf.stock.donation'
    
    _columns = { 
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
        'location_id' : fields.many2one('stock.location','Destination',required=True,domain=[('available_in_selection','=',True)]),
        'name' : fields.char('Description', size=128, required=True),
        'date' : fields.date('Date', required=True),
        'currency_id' : fields.many2one('res.currency','Currency', required=True),
        'line_ids' : fields.one2many('isf.stock.donation.line','line_id','Products',required=True),
        'move_cost_id' : fields.many2one('account.move','id'),
        'sequence_id' : fields.char('ID',size=32),
    }
    
    _defaults = {
        'state' : 'draft',
    }
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','DON')])
        if len(seq_ids) == 0:
            self.create_sequence(cr, uid, vals)
            seq_ids = obj_sequence.search(cr, uid, [('name','=','DON')])
        
        period_ids = obj_period.find(cr, uid, vals['date'], context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
        
        vals['sequence_id'] = new_seq
        return super(isf_stock_donation, self).create(cr, uid, vals, context)
    
    def create_sequence(self, cr, uid, vals, context=None):
        """ Create new no_gap entry sequence for every new Joural
        """
        # in account.journal code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = '1'
        
        seq = {
            'name': 'DON',
            'implementation':'no_gap',
            'prefix': "DON/%(year)s/",
            'padding': 4,
            'number_increment': 1
        }
        return self.pool.get('ir.sequence').create(cr, uid, seq)
    
    def get_default_journal_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','journal_id')
        
    def get_default_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','account_id')
        
    def get_default_donor_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','donor_account_id')
    
    def _get_enable_stock(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','enable_stock')
    
    #def get_default_main_location_id(self, cr, uid):
    #    return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','main_location_id')
        
    def get_default_supplier_location_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','supplier_location_id')
    
    def _save(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        
        data = self.browse(cr, uid, ids)[0]
        stock_enabled = self._get_enable_stock(cr, uid)
        
        isf_lib = self.pool.get('isf.lib.utils')
        curr_obj = self.pool.get('res.currency')
        location_obj = self.pool.get('stock.location')
        line_o = self.pool.get('isf.stock.donation.line')
        
        comp_curr_id = isf_lib.get_company_currency_id(cr, uid)
        supp_location_id = self.get_default_supplier_location_id(cr, uid)
        donor_account_id = self.get_default_donor_account_id(cr,uid)
        account_id = self.get_default_account_id(cr,uid)
        stock_journal_id = self.get_default_journal_id(cr, uid)
        
        wizard = self.browse(cr, uid, ids)[0]
        location_ids = location_obj.search(cr, uid, [('id','=',data.location_id.id)])
        stock_location = location_obj.browse(cr, uid, location_ids)[0]
        
        currency_id = False
        total_total_price = 0.0
        for line in wizard.line_ids:
            total_curr_price = False
            if line.quantity > 0:
                total_price = line.total_price
                
                if comp_curr_id != wizard.currency_id.id:
                    total_price = curr_obj.compute(cr, uid, wizard.currency_id.id, comp_curr_id, line.total_price, context=context)
                    total_curr_price = line.total_price
                    currency_id = wizard.currency_id.id
            
                cost_price = total_price / line.quantity
                total_total_price += total_price
                
                expiry_date = False
                if line.expiry_date:
                    expiry_date = line.expiry_date
                
                if _debug:
                    _logger.debug('product_id : %s', line.product_id)
            
                dest_account_id = account_id
                
                if expiry_date:
                    lot_id = isf_lib.create_lot_id_with_expiry_date(cr, uid, line.product_id, expiry_date)
                else:
                    lot_id = isf_lib.create_lot_id_with_expiry_date(cr, uid, line.product_id, False)
                move_id = isf_lib._generate_stock_move(cr, uid, wizard.name, wizard.name, supp_location_id, data.location_id.id, line.product_id, line.quantity,  wizard.date, 'in', lot_id, data.sequence_id, uom_id=line.uom_id, context=context)
                isf_lib.set_cost_price(cr, uid, line.product_id.id, lot_id, cost_price)
                line_o.write(cr, uid, line.id, {'move_id' : move_id, 'state' : 'done'}, context=context)
        
        if stock_enabled:
            move_pool = self.pool.get('account.move')
            move_line_pool = self.pool.get('account.move.line')
            move_cost = move_pool.account_move_prepare(cr, uid, stock_journal_id, wizard.date, ref=wizard.name, context=context)    
            move_cost_id = move_pool.create(cr, uid, move_cost, context=context)
            isf_lib._generate_cost_of_items_moves(cr, uid, move_line_pool, wizard.name, 'Donation', total_total_price, move_cost_id, dest_account_id, donor_account_id, 
                    stock_location.analytic_account_id.id, False, currency_id, total_curr_price, context=None)
            self.write(cr, uid, ids, {'state' : 'done' , 'move_cost_id' : move_cost_id}, context=context)
        
        self.write(cr, uid, ids, {'state' : 'done'}, context=context)
        return True
            
            
    def confirm(self, cr, uid, ids, context=None):
        self._save(cr, uid, ids, context=context)
        return {'type': 'ir.actions.act_window_close'}
        
    def set_to_draft(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        stock_move_o = self.pool.get('stock.move')
        line_o = self.pool.get('isf.stock.donation.line')
        stock_enabled = self._get_enable_stock(cr, uid)
        
        #
        # Remove stock moves
        #
        for line in data.line_ids:
            stock_move_o.write(cr, uid, line.move_id.id, {'state' : 'cancel'}, context=context)
            line_o.write(cr, uid, line.id, {'state' : 'draft'}, context=context)
            
        #
        # Remove journal entry (cost of items move)
        #
        move_ids = []
        if data.move_cost_id:
            move_ids.append(data.move_cost_id.id)
        
        if stock_enabled:
            move_o = self.pool.get('account.move')
            move_o.write(cr, uid, move_ids, {'state':'draft'}, context=context)
            move_o.unlink( cr, uid, move_ids, context=None, check=False)
            
        self.write(cr, uid, ids, {'state' : 'draft'}, context=context)
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        stock_moves = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in stock_moves:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a stock move entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
        
    def print_report(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)[0]
        
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_stock', 'isf_stock_move_report_wizard')   
        ctx = context.copy() 
        result = {
            'name': 'Stock Module',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.stock.move.report',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }   
        return result
        
##################################################################################################################################################
############################################################ STOCK OUTPUT ########################################################################
##################################################################################################################################################

class isf_stock_output_batch(osv.TransientModel):
    _name = 'isf.stock.output.batch'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Batch'),
        'product_id' : fields.related('stock_id','product_id',type="many2one",relation="product.product",string="Product",store=False),
        'qty' : fields.float('Qty'),
        'line_id' : fields.integer('Line'),
        'stock_line_id': fields.integer('Stock Id'),
    }
    
    

class isf_stock_output_line_select(osv.TransientModel):
    _name = 'isf.stock.output.line.select'
    
    _columns = {
        'today' : fields.date('today'),
        'output_type' : fields.selection([('expired','Expired'),('broken','Broken')],'Op.Type'),
        'product_ids' : fields.many2many('stock.production.lot',string="Products",required=False),
        'batch_broken_ids' : fields.one2many('isf.stock.output.batch','line_id','Batches',required=False),
        'location_id' : fields.many2one('stock.location','Source Location',required=False),
        'statement_id' : fields.integer('Statement Id'),
    }
    
    _defaults = {
        'today' : fields.date.context_today,
    }
    
    def _pre_load_batch(self, cr, uid,context=None):
        lot_o = self.pool.get('stock.production.lot')
        lot_ids = lot_o.search(cr, uid, [('stock_available','>',0.0)], context=context)
        batch_o = self.pool.get('isf.stock.output.batch')
        line_id = context.get('statement_id')
        for lot in lot_o.browse(cr, uid, lot_ids, context=context):
            if lot.stock_available > 0:
                res = batch_o.search(cr, uid, [('stock_id','=',lot.id),('stock_line_id','=',line_id)])
                if len(res) == 0:
                    batch_o.create(cr, uid, {
                        'stock_id' : lot.id,
                        'product_id' : lot.product_id.id,
                        'qty': lot.stock_available,
                        'stock_line_id' : line_id,
                    })                
        
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(isf_stock_output_line_select, self).default_get(cr, uid, fields, context=context)
        statement_id = context.get('active_id')
        data_o = self.pool.get('isf.stock.output')
        data_ids = data_o.search(cr, uid, [('id','=',statement_id)])
        data = data_o.browse(cr, uid, data_ids)[0]
        res['location_id'] = data.location_id.id
        res['output_type'] = data.output_type
        res['statement_id'] = statement_id
        
        ctx = context.copy()
        ctx.update({
            'location_id' : data.location_id.id,
            'statement_id' : data.id,
        })
    
    
        stock_o = self.pool.get('isf.stock.output.batch')
        stock_ids = stock_o.search(cr, uid, [('line_id','=',data.id)])
        
        for stock in stock_o.browse(cr, uid, stock_ids):
            stock_o.unlink(cr, uid, stock.id, context=context)
        
        self._pre_load_batch(cr, uid, context=ctx)
    
        return res
    
    def import_product(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        
        if context is None:
            context = {}
        ctx = {}
        ctx = context.copy()
        ctx.update({
            'location_id' : data.location_id.id
        })
    
        line_obj = self.pool.get('isf.stock.output.line')
        stock_o = self.pool.get('stock.production.lot')
        stock_ids = []
        
        if data['output_type'] == 'expired':
            for stock in data['product_ids']:
                stock_ids.append(stock.id)
        else:
            for stock in data['batch_broken_ids']:
                stock_ids.append(stock.stock_id.id)
        
        if _debug:
            _logger.debug('STOCK IDS : %s', stock_ids)    
         
        active_id = context.get('active_id')
        state = context.get('state')
        
        
        now = datetime.datetime.now()
        
        for stock in stock_o.browse(cr, uid, stock_ids, context=ctx):
            expired = False
            exp = datetime.datetime.strptime(stock.ref, "%Y-%m-%d")
            if exp < now:
                expired = True
            
            if _debug:
                _logger.debug('ADD : %s', stock)
            
            if stock.stock_available > 0.0:
                qty = 1
                if expired:
                    qty = stock.stock_available
                line_id = line_obj.create(cr, uid, {
                    'stock_id' : stock.id,
                    'expiry_date' : stock.ref,
                    'product_id' : stock.product_id.id,
                    'stock_available' : stock.stock_available,
                    'quantity' : qty,
                    'uom_id' : stock.product_id.uom_id.id,
                    'expired' : expired,
                    'statement_id' : active_id,
                    'state' : state,
                }, context=context)
                
        return True



class isf_stock_output_line(osv.Model):
    _name = 'isf.stock.output.line'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Lot Id'),
        'expiry_date' : fields.char('Expiry Date'),
        'product_id' : fields.many2one('product.product','Product'),
        'stock_available' : fields.integer('Available'),
        'quantity' : fields.integer('Quantity'),
        'uom_id' : fields.many2one('product.uom','UoM'),
        'expired' : fields.boolean('Expired'),
        'statement_id' : fields.many2one('isf.stock.output',string="Statement Id",select=True),
        'move_id' : fields.many2one('stock.move','id'),
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
    }
    
    _defaults = {
        'state' : 'draft',
    }

class isf_stock_output(osv.Model):
    _name = 'isf.stock.output'
    
    def _get_output_type(self, cr, uid,  context=None):
        return [('expired','Expired'),('broken','Broken\Loss')]
        
    
    _columns = {
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
        'name' : fields.char('Description', size=128, required=True),
        'date' : fields.date('Date',required=True),
        'output_type' : fields.selection(_get_output_type,'Expense Type', required=True),
        'line_ids' : fields.one2many('isf.stock.output.line','statement_id','Products'),
        'move_cost_id' : fields.many2one('account.move','id'),
        'location_id' : fields.many2one('stock.location','Location',required=True,domain=[('available_in_selection','=',True)]),
        'sequence_id' : fields.char('ID',char=64),
    }
    
    _defaults = {
        'state' : 'draft',
    }    
     
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','Stock Dispose')])
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids,context)
        vals['sequence_id'] = new_seq
        res_id = super(isf_stock_output, self).create(cr, uid, vals, context=context)
        return res_id
    
        
    def get_expired_location_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','expired_location_id')
        
    def get_broken_location_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','loss_location_id')
        
    def get_expired_account_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','expired_account_id')
        
    def get_broken_account_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','loss_account_id')
    
    def get_asset_account_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','account_id')
    
    def get_journal_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','journal_id')
    
    def _get_enable_stock(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','enable_stock')
    
    def confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cr, uid, ids)[0]
        stock_enabled = self._get_enable_stock(cr, uid)
     
        expense_account_id = False
        journal_id = False
        aa_id = False
        
        isf_lib = self.pool.get('isf.lib.utils')
                
        if wizard.output_type == 'expired':
            expense_account_id = self.get_expired_account_id(cr, uid)
            location_dest_id = self.get_expired_location_id(cr, uid)
        elif wizard.output_type == 'broken':
            expense_account_id = self.get_broken_account_id(cr, uid)
            location_dest_id = self.get_broken_location_id(cr, uid)
            
            
        account_asset_id = self.get_asset_account_id(cr, uid)
        journal_id = self.get_journal_id(cr, uid)
        
        line_o = self.pool.get('isf.stock.output.line')
        location_source_id = wizard.location_id.id
        aa_id = wizard.location_id.analytic_account_id.id
        
        for line in wizard.line_ids:
            cost_price = line.stock_id.cost_price * line.quantity
            if cost_price == 0.0:
                cost_price = line.product_id.list_price * line.quantity
            
            move_id = isf_lib._generate_stock_move(cr, uid, wizard.name, wizard.name, location_source_id, location_dest_id, line.product_id, line.quantity, wizard.date, 'internal', line.stock_id.id, wizard.sequence_id, uom_id=line.uom_id, context=context)
            line_o.write(cr, uid, line.id, {'move_id' : move_id, 'state' : 'done'}, context=context)
            
            if stock_enabled:
                move_pool = self.pool.get('account.move')
                move_line_pool = self.pool.get('account.move.line')        
                move_cost = move_pool.account_move_prepare(cr, uid, journal_id, wizard.date, ref=wizard.name, context=context)    
                move_cost_id = move_pool.create(cr, uid, move_cost, context=context)
                isf_lib._generate_cost_of_items_moves(cr, uid, move_line_pool, wizard.name, line.product_id.name, cost_price, move_cost_id, expense_account_id,account_asset_id, aa_id, aa_id, context=None)
            
        self.write(cr, uid, wizard.id, {'move_cost_id' : move_cost_id}, context=context)
        self.write(cr, uid, ids, {'state' : 'done' }, context=context)
        
        return True
        
    def set_to_draft(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        stock_move_o = self.pool.get('stock.move')
        line_o = self.pool.get('isf.stock.output.line')
        stock_enabled = self._get_enable_stock(cr, uid)
        
        #
        # Remove stock moves
        #
        for line in data.line_ids:
            stock_move_o.write(cr, uid, line.move_id.id, {'state' : 'cancel'}, context=context)
            line_o.write(cr, uid, line.id, {'state' : 'draft'}, context=context)
            
        #
        # Remove journal entry (cost of items move)
        #
        move_ids = []
        if data.move_cost_id:
            move_ids.append(data.move_cost_id.id)
        
        if stock_enabled:
            move_o = self.pool.get('account.move')
            move_o.unlink( cr, uid, move_ids, context=None, check=False)
            
        self.write(cr, uid, ids, {'state' : 'draft'}, context=context)
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        stock_moves = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in stock_moves:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a stock output entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
        

      
