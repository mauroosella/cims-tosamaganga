from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
from datetime import timedelta
from datetime import date
import openerp

_logger = logging.getLogger(__name__)
_debug=False

class isf_bank_account_checking(osv.osv):
    _name = 'isf.bank.account.checking'
    
    def _get_company_currency(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id.id
    
    def _prev_period(self, cr, uid, ids, period_id, account_id, context=None):
        if _debug:
            _logger.debug('==> Period id : %s', period_id)
    
        period_obj = self.pool.get('account.period')
        actual_period_ids = period_obj.search(cr, uid, [('id','=', period_id)])
        actual_period = period_obj.browse(cr, uid, actual_period_ids)[0] or None
        
        if _debug:
            _logger.debug('Start date (actual period) : %s', actual_period.date_start)
            
        Date = datetime.datetime.strptime(actual_period.date_start, "%Y-%m-%d")
        end_date = Date - timedelta(days=1)
    
        prev_period = False
        bank_obj = self.pool.get('isf.bank.account.checking')
        bank_ids = bank_obj.search(cr, uid, [('period_id.date_stop','=', end_date),('account_id','=',account_id)])
        for bank in bank_obj.browse(cr, uid, bank_ids):
            prev_period = bank.period_id
        
        return prev_period

    def _load_outstandig_cheque_for_period(self, cr, uid, ids, account_id, date_start, date_end, context=None):
        if context is None:
            context = {}
        
        lines_obj = self.pool.get('isf.bank.account.checking.lines')
        lines_ids = lines_obj.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('checked','=',False),('account_id','=',account_id)])
        
        cheque = 0.0
        for line in lines_obj.browse(cr, uid, lines_ids, context=context):
            cheque = cheque + abs(line.amount)
            
        if _debug:
            _logger.debug('Outstandig cheque for last period : %f', cheque)
            
        return cheque
        
    def _load_starting_balance_from_prev_period(self, cr, uid, ids, account_id, period_id, context=None):
        if context is None:
            context = {}
            
        prev_period = self._prev_period(cr, uid, ids, period_id, account_id, context=context)
        if prev_period is None or prev_period is False:
            return 0.0
        
        bank_obj = self.pool.get('isf.bank.account.checking')
        bank_ids = bank_obj.search(cr, uid, [('period_id','>=',prev_period.id),('account_id','=',account_id)])    
        bank = bank_obj.browse(cr, uid, bank_ids, context=context)[0] or None
        
        if bank is None:
            return 0.0
            
        return bank.ending_balance
    
    def _load_starting_balance(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        
        datas = self.browse(cr, uid, ids)
        res = {}
        starting_balance = 0.0
        for data in datas:
            starting_balance = self._load_opening_balance(cr, uid, ids, data.period_id.id, data.account_id.id, context)
            res[data.id] = starting_balance
        
        return res
        
    def _load_opening_balance(self, cr, uid, ids, period_id, account_id, context=None):
        if context is None:
            context = {}
            
        period_obj = self.pool.get('account.period')
        period = period_obj.browse(cr, uid, period_id)
        opening_period_id = period_obj.search(cr, uid, [('fiscalyear_id','=',period.fiscalyear_id.id),('special','=',True)])
        
        period_ids = period_obj.search(cr, uid, [('id','>=',opening_period_id),('id','<',period_id)])
                
        if _debug:
            _logger.debug('Period : %s', period_id)
            _logger.debug('Opening period : %s', opening_period_id)
            _logger.debug('Periods for balance: %s', period_ids)
            
        company_currency = self._get_company_currency(cr, uid, context=context)
            
        opening_balance = 0.0
        move_line_obj = self.pool.get('account.move.line')
        move_line_ids = move_line_obj.search(cr, uid, [('account_id','=',account_id),('period_id','in',period_ids)])
        for line in move_line_obj.browse(cr, uid, move_line_ids):
            if (not line.account_id.currency_id) or (line.currency_id.id == company_currency.id):
                if line.debit > 0.0:
                    opening_balance = opening_balance + line.debit
                if line.credit > 0.0:
                    opening_balance = opening_balance - line.credit
            else:
                opening_balance = opening_balance + line.amount_currency
            
        return opening_balance
        
    
    def _get_first_valid_period(self, cr, uid, ids, account_id, context=None):
        period_obj = self.pool.get('account.period')
        bank_account_obj = self.pool.get('isf.bank.account.checking')
        bank_account_ids = bank_account_obj.search(cr, uid, [('account_id','=',account_id),('state','=','reconciled')])
        
        periods = []
        for bank_account in bank_account_obj.browse(cr, uid, bank_account_ids):
            if _debug:
                _logger.debug('==> Account[%s] Period[%s]', bank_account.account_id.name, bank_account.period_id.name)
            periods.append(bank_account.period_id)
            
        last_period = None
        for period in periods:
            if last_period is None:
                last_period = period
            elif period.date_stop > last_period.date_stop:
                last_period = period
        
        if last_period is not None:        
            date = datetime.datetime.strptime(last_period.date_stop, "%Y-%m-%d")
            date_start = date + timedelta(days=1)
            
            next_period_ids = period_obj.search(cr, uid, [('special','=',False),('date_start','=',date_start)])
            next_period = period_obj.browse(cr, uid, next_period_ids)[0] or None
            
            if _debug:
                _logger.debug('Last Period name[%s]', last_period.name)
                _logger.debug('Next Period name[%s]', next_period.name)
            
            return next_period
        else:
            period_id = period_obj.find(cr, uid)[0]
            period = period_obj.browse(cr, uid, period_id)
            
            if _debug:
                _logger.debug('Last Period name[%s]', period.name)
            
            return period    
            
        return None
        
    
    def _remove_all_moves(self, cr, uid, ids, context=None):
        lines_obj = self.pool.get('isf.bank.account.checking.lines')
        lines_ids = lines_obj.search(cr, uid, [('statement_id','in', ids)])
        
        if _debug:
            _logger.debug('Lines ids[%s]',lines_ids);
        
        lines_obj.unlink(cr, uid, lines_ids)
        
        result = {'value':{} }
        result['value'] = {
            'line_ids' : None,
        }
        
        return result
    
    def onchange_period(self, cr, uid, ids, period_id=None, account_id=None, context=None):
        if context is None:
            context = {}
            
        result = {'value':{} }
        
        if account_id == False or period_id == False:
            return result
        
        period_obj = self.pool.get('account.period')
        period = period_obj.browse(cr, uid, period_id)
        
        cheque_amount = 0.0
        starting_balance = self._load_opening_balance(cr, uid, ids, period.id, account_id, context=context)
        
        result['value'].update({
            'starting_balance' : starting_balance,
            'outstandig_cheque_prev_period' : cheque_amount,
            'period_id' : period.id,
        })
        if _debug:
            _logger.debug('Result : %s', result)
            
        self.write(cr, uid, ids, {'period_id' : period.id}, context=context)
        
        self._remove_all_moves(cr, uid, ids)
        
        return result
    
    
    def onchange_account(self, cr, uid, ids, account_id=None, context=None):
        if context is None:
            context = {}

        result = {'value':{} }
        
        period = self._get_first_valid_period(cr, uid, ids, account_id, context=context)
        
        if account_id == False:
            return result
            
        last_period = self._prev_period(cr, uid, ids, period.id, account_id, context=context)
        
        if last_period is None or last_period == False:
            cheque_amount = 0.0
            starting_balance = self._load_opening_balance(cr, uid, ids, period.id, account_id, context=context)
        else:
            cheque_amount = self._load_outstandig_cheque_for_period(cr, uid, ids, account_id, last_period.date_start, last_period.date_stop,context=context)
            starting_balance = self._load_starting_balance_from_prev_period(cr, uid, ids, account_id, period.id, context=context)
        
        
        result['value'].update({
            'starting_balance' : starting_balance,
            'outstandig_cheque_prev_period' : cheque_amount,
            'period_id' : period.id,
        })
        
        self.write(cr, uid, ids, {'period_id' : period.id}, context=context)
        
        _logger.debug('Result : %s', result)
        
        self._remove_all_moves(cr, uid, ids)
        
        return result
        
    
    _columns = {
        'name' : fields.char('Name', size=64, required=False),
        'account_id' : fields.many2one('account.account', 'Account', required=True),
        'date' : fields.date('Date', required=True),
        'period_id' : fields.many2one('account.period', 'Period', required=True),
        'state': fields.selection([('draft','Open'), ('close','Confirmed'),('reconciled','Reconciled')], 'Status', readonly=True),
        'line_ids' : fields.one2many('isf.bank.account.checking.lines','statement_id','Entries', required=False,readonly=True, states={'draft':[('readonly',False)]}),
        #'starting_balance' : fields.float('Starting Balance', required=False),
        'starting_balance' : fields.function(_load_starting_balance, type='float', string='Starting Balance', store=True),
        'ending_balance' : fields.float('Ending Balance (supposed)', required=False),
        'difference' : fields.float('Uncleared Payments', required=False),
        'total_amount' : fields.float('Ending Balance (calculated)', required=False),
        'outstandig_cheque_prev_period' : fields.float('Oustanding Cheque Last Period (Paid)', required=False),
        'cleared_payments' : fields.float('Cleared Payments', required=False),
        'cleared_receipts' : fields.float('Cleared Receipts', required=False),
        'total_ending' : fields.float('Total', required=False),
        'reconciled_difference' : fields.float('Reconciled', required=False),
        'reconcile_id' : fields.many2one('account.move.reconcile','Reconcile', required=False),
    }
    
    _defaults = {
        'state' : 'draft',
        'name' : '/',
        'date' : date.today().strftime('%Y-%m-%d'),
    }
    
    
#     def create(self, cr, uid, vals, context=None):    
#         period = self._get_first_valid_period(cr, uid, 0 , vals['account_id'], context=context)
#         starting_balance = self._load_starting_balance_from_prev_period(cr, uid, 0, vals['account_id'], period.id, context=context)
#         vals['period_id'] = period.id
#         vals['starting_balance'] = starting_balance
#         return super(isf_bank_account_checking, self).create(cr, uid, vals, context=context)
    
#     def create_bank_account_checking(self, cr, uid, ids, context=None):
#         self.write(cr, uid, ids, {'state':'draft'}, context=context)
    
    def confirm_bank_account_checking(self, cr, uid, ids, context=None):
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0])])
        line_obj.write(cr, uid, line_ids, {'state' : 'close'}, context=context)
        self.write(cr, uid, ids, {'state':'close'}, context=context)
        
    def unconfirm_bank_account_checking(self, cr, uid, ids, context=None): 
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0])])
        line_obj.write(cr, uid, line_ids, {'state' : 'draft'}, context=context)
        self.write(cr, uid, ids, {'state':'draft'}, context=context)
        
        return True
        
    def reconcile(self, cr, uid, ids, context=None):
        bank =  self.read(cr, uid, ids, context=context)[0]
        
        if abs(bank['reconciled_difference']) <= 0.00001:
            self._reconcile(cr, uid, ids, context=context)
            self.write(cr, uid, ids, {'state':'reconciled'}, context=context)
            if _debug:
                _logger.info('====> RECONCILED')
        else:
            if _debug:
                _logger.info('====> CANNOT RECONCILE <==== Difference : %f', bank['reconciled_difference'])
                raise openerp.exceptions.Warning(_('CANNOT RECONCILE\nDifference: %s')%_(bank['reconciled_difference']))
            
        return True
        
    def unreconcile(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        bank =  self.read(cr, uid, ids, context=context)[0]
        
        move_rec_obj = self.pool.get('account.move.reconcile')
        move_line_obj = self.pool.get('account.move.line')
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0]),('checked','=',True)])
        
        move_ids = []
        for line in line_obj.browse(cr, uid, line_ids):
            move_ids.append(line.move_line_id.id)
        
        unlink_ids = []
        for line in move_line_obj.browse(cr, uid, move_ids, context=context):
            if line.reconcile_id:
                if line.reconcile_id.id == bank['reconcile_id'][0]:
                    if _debug:
                        _logger.debug("====> LINE ID : %s",line.id)
                    move_line_obj.write(cr, uid, line.id, {'reconcile_id' : False}, context=context)
      
        
        line_obj.write(cr, uid, line_ids, {'state' : 'close'}, context=context)    
        self.write(cr, uid, ids, {'reconcile_id':False}, context=context)
        self.write(cr, uid, ids, {'state':'close'}, context=context)
        return True
        
    def _reconcile(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        bank =  self.read(cr, uid, ids, context=context)[0]
        
        move_rec_obj = self.pool.get('account.move.reconcile')
        move_line_obj = self.pool.get('account.move.line')
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0])])
        
        r_id = move_rec_obj.create(cr, uid, {
            'type': 'manual',
        })
        
        self.write(cr, uid, ids, {'reconcile_id' : r_id}, context=context)
        
        move_ids = []
        for line in line_obj.browse(cr, uid, line_ids):
            if line.checked:
                move_ids.append(line.move_line_id.id)
                line_obj.write(cr, uid, line.id, {'state' : 'reconciled'}, context=context)    
                
        move_line_obj.write(cr, uid, move_ids, {'reconcile_id' : r_id}, context=context)
                    
        return True
        
    def compute_checking(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        bank =  self.read(cr, uid, ids, context=context)[0]
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0])])
        #bank_ids = bank_obj.search(cr, uid, [('id','=', statement_id)])
        
        total_amount = bank['starting_balance']
        total_outstanding = 0.0
        difference = 0.0
        if _debug:
            _logger.debug('Starting balance = %f', total_amount)
        
        cleared_receipts = 0.0
        cleared_payments = 0.0
        total_check = 0.0
        for line in line_obj.browse(cr, uid, line_ids, context=context):
            if line.checked:
                if _debug:
                    _logger.debug('==> comparing move_line and bank periods : %s and %s', line.move_line_id.period_id.id, bank['period_id'][0])
                if line.move_line_id.period_id.id == bank['period_id'][0]:
                    total_amount += line.amount
                if line.amount < 0.0:
                    cleared_payments += abs(line.amount)
                if line.amount > 0.0:
                    cleared_receipts += line.amount
            else:
                total_outstanding += abs(line.amount)
                
            if _debug:
                _logger.debug('Amount : %f', line.amount)
        
        reconciled_difference = abs(bank['ending_balance'] - total_amount)
                
        if _debug:
            _logger.debug('TOTAL AMOUNT : %f', total_amount)
            _logger.debug('Total outstanding : %f', total_outstanding)
            _logger.debug('Cleared payments : %f', cleared_payments)
            _logger.debug('Ending Balance : %f',bank['ending_balance'])
            _logger.debug('Reconciled difference : %f', reconciled_difference)
        
        
        total_ending = bank['starting_balance'] + cleared_receipts
        self.write(cr, uid, ids, {'difference' : total_outstanding}, context=context)
        self.write(cr, uid, ids, {'total_amount' : total_amount}, context=context)
        self.write(cr, uid, ids, {'cleared_payments' : cleared_payments}, context=context)
        self.write(cr, uid, ids, {'cleared_receipts' : cleared_receipts}, context=context)
        self.write(cr, uid, ids, {'reconciled_difference' : reconciled_difference}, context=context)
        self.write(cr, uid, ids, {'total_ending' : total_ending}, context=context)
        
        return True
        
    def check_all(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0])])
        
        for line in line_obj.browse(cr, uid, line_ids, context=context):
           line_obj.write(cr, uid, line.id, {'checked' : True }, context=context)
           
    def uncheck_all(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        line_obj = self.pool.get('isf.bank.account.checking.lines')
        line_ids = line_obj.search(cr, uid, [('statement_id.id','=',ids[0])])
        
        for line in line_obj.browse(cr, uid, line_ids, context=context):
           line_obj.write(cr, uid, line.id, {'checked' : False }, context=context)
                
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        bankcheck = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in bankcheck:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a bank checking statement which is not draft. You should refund it instead.'))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
     

isf_bank_account_checking()

class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    _name = 'account.move.line'
    
    _columns = {
        'checked' : fields.boolean('Checked', required=False),
    }
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = {} if default is None else default.copy()
        default.update({
            'checked': False
        })
        return super(account_move_line, self).copy(cr, uid, id, default=default, context=context)

    
account_move_line()

class isf_bank_account_checking_lines(osv.osv):
    _name = 'isf.bank.account.checking.lines'
    
    _columns = {
        'date' : fields.date('Date'),
        'name' : fields.char('Name', size=32),
        'reference' : fields.char('Reference', size=32),
        'partner_id' : fields.many2one('res.partner', 'Partner'),
        'account_id' : fields.many2one('account.account', 'Account'),
        'amount' : fields.float('Amount'),
        'checked' : fields.boolean('Checked',readonly=True, states={'draft':[('readonly',False)]}),
        'statement_id' : fields.many2one('isf.bank.account.checking','Statement',select=True, required=True, ondelete='cascade'),
        'move_line_id' : fields.many2one('account.move.line','id','Move Line Id', required=True),
        'state' : fields.selection([('draft','Open'), ('close','Confirmed'),('reconciled','Reconciled')], 'Status', readonly=True),
    }
    
    _defaults = {
        'state' : 'draft',
    }
    
        
    def onchange_checked(self, cr, uid, ids, checked, statement_id,context=None):
        bank_obj = self.pool.get('isf.bank.account.checking')
        bank_ids = bank_obj.search(cr, uid, [('id','=',statement_id)])
        
        if _debug:
            _logger.debug('onchange_checked, statement_id : %d , checked : %s', statement_id, checked)
        
        self.write(cr, uid, ids, {'checked' : checked}, context=None)
        
        bank_obj.compute_checking(cr, uid, bank_ids, context=context)
        return True
    
isf_bank_account_checking_lines()

class isf_bank_account_checking_from_line(osv.osv_memory):
    _name = 'isf.bank.account.checking.from.lines'
    
    _columns = {
        'line_ids' : fields.many2many('account.move.line','isf_bac_fl_rel',string="Lines"),
    }
    
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(isf_bank_account_checking_from_line, self).default_get(cr, uid, fields,     context=context)
        
        statement_id = context['statement_id']
        bank_account_lines_obj = self.pool.get('isf.bank.account.checking.lines')
        bank_account_lines_ids = bank_account_lines_obj.search(cr, uid, [('statement_id','=',statement_id)])
        move_line = []
        
        for line in bank_account_lines_obj.browse(cr, uid, bank_account_lines_ids):
            move_line.append(line.move_line_id.id)
            
        if _debug:
            _logger.debug('Context : %s', context)
            _logger.debug('move_line : %s', move_line)
        res['line_ids'] = move_line
        return res
    
    def _get_the_last_period(self, cr, uid, period_id, account_id):
        prev_period = False
        bank_obj = self.pool.get('isf.bank.account.checking')
        bank_ids = bank_obj.search(cr, uid, [('period_id.date_stop','<', period_id.date_start),('account_id','=',account_id)])
        for bank in bank_obj.browse(cr, uid, bank_ids):
            prev_period = bank.period_id
        
        return prev_period
    
    def _load_oustanding_balance(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        statement_id = context.get('statement_id')
        account_id = context.get('account_id')
        period_id = context.get('period_id')
        
        if not statement_id:
            return {'type': 'ir.actions.act_window_close'}
            
        if _debug:
            _logger.debug('Statement_id : %d',statement_id)
        
        bank_check_line_obj = self.pool.get('isf.bank.account.checking.lines')
        lines_obj = self.pool.get('account.move.line')
        bank_check_obj = self.pool.get('isf.bank.account.checking')
        bank_check_ids = bank_check_obj.search(cr, uid, [('account_id','=',account_id),('period_id','=',period_id)])
        bank_check = bank_check_obj.browse(cr, uid, bank_check_ids, context=context)[0]
        
        prev_period = self._get_the_last_period(cr, uid, bank_check.period_id,account_id)
        if prev_period:
            prev_bank_check_ids = bank_check_obj.search(cr, uid, [('period_id.id','=', prev_period.id),('account_id','=',account_id)],limit=1)
            for bank in bank_check_obj.browse(cr, uid, prev_bank_check_ids):
                for line in bank.line_ids:
                    if not line.checked:
                
                        bank_check_line_obj.create(cr, uid, {
                            'date' : line.date,
                            'name' : line.name,
                            'reference' : line.reference,
                            'partner_id' : line.partner_id.id,
                            'account_id' : line.account_id.id,
                            'amount' : line.amount,
                            'checked' : False,
                            'statement_id' : statement_id,
                            'move_line_id' : line.move_line_id.id,
                        }, context=context)
        return True
    
    def _get_company_currency(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id.id
        
    def populate_statement(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        statement_id = context.get('statement_id')
        account_id = context.get('account_id')
        period_id = context.get('period_id')
        
        if not statement_id:
            return {'type': 'ir.actions.act_window_close'}
            
        if _debug:
            _logger.debug('Statement_id : %d',statement_id)
            
        lines_obj = self.pool.get('account.move.line')
        bank_check_obj = self.pool.get('isf.bank.account.checking')
        bank_check_line_obj = self.pool.get('isf.bank.account.checking.lines')
        bank_check_line_ids = bank_check_line_obj.search(cr, uid, [('statement_id','=',statement_id)])
        bank_check_ids = bank_check_obj.search(cr, uid, [('account_id','=',account_id),('period_id','=',period_id)])
        bank_check = bank_check_obj.browse(cr, uid, bank_check_ids, context=context)[0]
            
        data =  self.read(cr, uid, ids, context=context)[0]
        line_ids = data['line_ids']
        
        if not line_ids:
            return {'type': 'ir.actions.act_window_close'}
            
        #
        # Load in current_move_line[] all the move_line_id previously inserted in this statement
        # this control prevent the records duplication
        #
        current_move_line=[]
        for move_line in bank_check_line_obj.browse(cr, uid, bank_check_line_ids):
            current_move_line.append(move_line.move_line_id.id)
        
        self._load_oustanding_balance(cr, uid, ids, context=context)
        company_currency = self._get_company_currency(cr, uid, context=context)
        
        for line in lines_obj.browse(cr, uid, line_ids, context=context):    
            amount = 0.0
            
            if (not bank_check.account_id.currency_id) or (bank_check.account_id.currency_id.id == company_currency.id):
                if line.debit > 0:
                    amount = line.debit
                if line.credit > 0:
                    amount = -line.credit
            else:
                amount = line.amount_currency
                
         
            found_oustanding = bank_check_line_obj.search(cr, uid, [('move_line_id','=', line.id)])
            
            if not found_oustanding:
                if line.id not in current_move_line:
                    bank_check_line_obj.create(cr, uid, {
                        'date' : line.date,
                        'name' : line.name,
                        'reference' : line.ref,
                        'partner_id' : line.partner_id.id,
                        'account_id' : line.account_id.id,
                        'amount' : amount,
                        'checked' : False,
                        'statement_id' : statement_id,
                        'move_line_id' : line.id,
                    }, context=context)
            else:
                _logger.debug('Found outstanding : not create line')
                
        return {'type': 'ir.actions.act_window_close'}


isf_bank_account_checking_from_line()
