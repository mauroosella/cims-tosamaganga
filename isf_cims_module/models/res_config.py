import time
import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from os.path import join as opj

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging

_logger = logging.getLogger(__name__)
_debug=False
#################################################################################################################################
########################################################### GENERAL CONFIG ######################################################
#################################################################################################################################
class isf_cims_general_config_settings(osv.osv_memory):
    _name = 'isf.cims.general.config.settings'
    _inherit = 'res.config.settings'

    _columns = {
        'percentage_cost_price' : fields.float('Percentage to calculate cost_price'),
        'consumable_property_account_income_id' : fields.many2one('account.account', 'Default Income Account', required=False, domain=[('type','!=','view')], help="Default Income account for new Consumable Products"),
        'consumable_property_account_expense_id' : fields.many2one('account.account', 'Default Expense Account', required=True, domain=[('type','!=','view')], help="Default Expense account for new Consumable Products"),
        'product_property_account_income_id' : fields.many2one('account.account', 'Default Income Account', required=True, domain=[('type','!=','view')], help="Default Income account for new Products"),
        'product_property_account_expense_id' : fields.many2one('account.account', 'Default Expense Account', required=True, domain=[('type','!=','view')], help="Default Expense account for new Products"),
    }
    
    
    # Set Method
    
    
    def set_percentage_cost_price(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.cims.config', 'percentage_cost_price',config.percentage_cost_price)
        
    def set_consumable_property_account_income_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.cims.config', 'consumable_property_account_income_id',config.consumable_property_account_income_id.id)
        
    def set_consumable_property_account_expense_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.cims.config', 'consumable_property_account_expense_id',config.consumable_property_account_expense_id.id)
        
    def set_product_property_account_income_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.cims.config', 'product_property_account_income_id',config.product_property_account_income_id.id)
        
    def set_product_property_account_expense_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.cims.config', 'product_property_account_expense_id',config.product_property_account_expense_id.id)
    
    
    # Get Method
    
    
    def get_default_percentage_cost_price(self, cr, uid, ids, context=None):
        percentage_cost_price = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','percentage_cost_price')
        return {'percentage_cost_price' : percentage_cost_price }
    
    def get_default_consumable_property_account_income_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','consumable_property_account_income_id')
        return {'consumable_property_account_income_id' : account }
    
    def get_default_consumable_property_account_expense_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','consumable_property_account_expense_id')
        return {'consumable_property_account_expense_id' : account }
    
    def get_default_product_property_account_income_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','product_property_account_income_id')
        return {'product_property_account_income_id' : account }
    
    def get_default_product_property_account_expense_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','product_property_account_expense_id')
        return {'product_property_account_expense_id' : account }
    
#################################################################################################################################
########################################################### MONEY TRASFER #######################################################
#################################################################################################################################

class isf_money_transfer_config_settings(osv.osv_memory):
    _name = 'isf.money.transfer.config.settings'
    _inherit = 'res.config.settings'
    _help = 'Register transfer of money from Cash to Bank, from Bank to Cash or from Bank to Bank, when already occurred with the bank receipt'
	
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal',required=True),
        'help' : fields.text('Help', size=512, required=False),
    }
	
    def _get_help(self, cr, uid, context=None):
        help = 'Registers transfers of money from Cash to Bank, from Bank to Cash or from Bank to Bank, when already occurred with the bank receipt.'
        return help
	
    _defaults = {
        'help' : _get_help,
    }
	
    # Set method
	
    def set_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.money.transfer', 'journal_id',config.journal_id.id)
		
    def set_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.money.transfer', 'help',config.help)
        
		
    # get method
		
    def get_default_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.money.transfer','journal_id')
        return {'journal_id' : journal }
		
    def get_default_help(self, cr, uid, ids, context=None):
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.money.transfer','help')
        return {'help' : help or self._help}


#################################################################################################################################
########################################################### HR PAYROLL ##########################################################
#################################################################################################################################

class isf_hr_payroll_config_settings(osv.osv_memory):
    _name = 'isf.hr.payroll.config.settings'
    _inherit = 'res.config.settings'
    
    _columns = {
        'journal_id' : fields.many2one('account.journal','Payroll Payment Journal',required=True),
        'moh_journal_id' : fields.many2one('account.journal','Payroll Preoperative Journal',required=True),
        'advances_account_id' : fields.many2one('account.account','Advances Account',required=True),
        'advances_journal_id' : fields.many2one('account.journal','Payroll Advances Journal',required=True),
        'cash_bank_account_ids' : fields.many2many('account.account',string="Cash\Bank Account",required=True),
        'account_moh_founds_id' : fields.many2one('account.account','Minister of Health account (Founds)', required=True),
        'account_moh_income_id' : fields.many2one('account.account','Minister of Health account (Income)', required=True),
        'account_moh_id' : fields.many2one('account.account','Minister of Health account', required=True),
        'account_gross_id' : fields.many2one('account.account','Gross Account', required=True),
        'analytic_account_id' : fields.many2one('account.analytic.account','Default Analytic Account',required=True),
    }
    
    # Set method
	
    def set_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'journal_id',config.journal_id.id)
    
    def set_moh_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'moh_journal_id',config.moh_journal_id.id)
    
    def set_advances_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'advances_journal_id',config.advances_journal_id.id)
    
    def set_advances_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'advances_account_id',config.advances_account_id.id)
    
    def set_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.cash_bank_account_ids:
            account_ids.append(account.id)
			
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'cash_bank_account_ids',account_ids)
    
        
    def set_account_moh_income_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_moh_income_id',config.account_moh_income_id.id)
    
    def set_account_moh_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_moh_id',config.account_moh_id.id)
    
    def set_account_gross_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_gross_id',config.account_gross_id.id)
		
    
    def set_account_moh_founds_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_moh_founds_id',config.account_moh_founds_id.id)
        
    def set_analytic_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'analytic_account_id',config.analytic_account_id.id)
        
    # Get Method
    
    def get_default_moh_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','moh_journal_id')
        return {'moh_journal_id' : journal }
    
    def get_default_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','journal_id')
        return {'journal_id' : journal }
    
    def get_default_advances_journal_id(self, cr, uid, ids, context=None):
        advances_journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','advances_journal_id')
        return {'advances_journal_id' : advances_journal_id }
        
    def get_default_advances_account_id(self, cr, uid, ids, context=None):
        advances_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','advances_account_id')
        return {'advances_account_id' : advances_account_id }
        
    def get_default_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.hr.payroll', 'cash_bank_account_ids')
		
        return {'cash_bank_account_ids' : account_ids}
        
    def get_default_account_moh_income_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_moh_income_id')
        return {'account_moh_income_id' : account }
        
    def get_default_account_moh_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_moh_id')
        return {'account_moh_id' : account }
        
    def get_default_account_gross_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_gross_id')
        return {'account_gross_id' : account }
    
    def get_default_account_moh_founds_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','account_moh_founds_id')
        return {'account_moh_founds_id' : account }
        
    def get_default_analytic_account_id(self, cr, uid, ids, context=None):
        account = self.pool.get('ir.values').get_default(cr, uid, 'isf.hr.payroll','analytic_account_id')
        return {'analytic_account_id' : account }

#################################################################################################################################
########################################################### PHARMACY POS ########################################################
#################################################################################################################################        

class isf_pharmacy_pos_config_settings(osv.osv_memory):
    _name = 'isf.pharmacy.pos.config.settings'
    _inherit = 'res.config.settings'
    _help = 'Registers pharmacy prescription.'
	
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal Cash',required=True),
        'credit_journal_id' : fields.many2one('account.journal','Journal Credit',required=True),
        'payment_account_ids' : fields.many2many('account.account',string='Payment Accounts', required=True),
        'free_of_charge_account_id' : fields.many2one('account.account',string='Free of Charge Account', required=True),
        'internal_account_id' : fields.many2one('account.account',string='Internal Use Account', required=True),
        'analytic_account_id' : fields.many2one('account.analytic.account','Analytic Account', required=True),
        'stock_analytic_account_id' : fields.many2one('account.analytic.account','Stock Analytic Account', required=True),
        'product_category_id' : fields.many2one('product.category', "Product category",required=True),
        'stock_account_id' : fields.many2one('account.account', 'Main Stock Account', required=True),
        'income_account_id': fields.many2one('account.account', 'Income Account', required=True),
        'help' : fields.text('Help', size=512, required=False),
        'post_on_confirm' : fields.boolean('Post On Confirm', required=False),
        'enable_stock' : fields.boolean('Enable Stock', required=False),
        'enable_lot_management' : fields.boolean('Enable Lot Management', required=False),
        'stock_location_id' : fields.many2one('stock.location','Stock Location', required=False),
        'main_stock_location_id' : fields.many2one('stock.location','Main Stock Location', required=False),
        'output_location_id' : fields.many2one('stock.location','Output Location', required=False),
        'preferred_method' : fields.many2one('account.account',string='Preferred Payment Method', required=True),
        'cost_of_item_sold_account_id' : fields.many2one('account.account',string='Cost Of Item Sold', required=True),
        'cost_of_item_expired_account_id' : fields.many2one('account.account',string='Cost Of Item Expired', required=True),
        'cost_of_item_broken_account_id' : fields.many2one('account.account',string='Cost Of Item Broken\Loss', required=True),
        'stock_journal_id' : fields.many2one('account.journal','Stock Journal',required=True),
        'discount_account_id' : fields.many2one('account.account','Discount Account', required=True),
        'discount_product_id' : fields.many2one('product.product','Discount Product', required=True,domain=[('type','=','service')]),
        'supplier_location_id' : fields.many2one('stock.location','Supplier Location'),
        'expired_location_id' : fields.many2one('stock.location','Expired Location'),
        'broken_location_id' : fields.many2one('stock.location','Broken Location'),
        'central_account_ids' : fields.many2many('account.account','isf_pharm_central_account_rel','phamr_id','account_id','Central Account'),
        'unexpected_income_account_id' : fields.many2one('account.account','Unexpected Income'),
        'unexpected_expense_account_id' : fields.many2one('account.account','Unexpected Expense'),
        'preferred_central_account_id' : fields.many2one('account.account','Prefererd Central Account'),
    }
	
    def _get_help(self, cr, uid, context=None):
        return self._help
	
    _defaults = {
        'help' : _get_help,
        'post_on_confirm' : True,
        'enable_stock' : False,
        'enable_lot_management' : False,
    }
	
	# Set method
    
    
    def set_discount_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'discount_account_id',config.discount_account_id.id)
        
    def set_central_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
	   	
        account_ids = []
        for account in config.central_account_ids:
            account_ids.append(account.id)
     
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'central_account_ids',account_ids)
        
    def set_unexpected_income_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'unexpected_income_account_id',config.unexpected_income_account_id.id)
    
    
    def set_unexpected_expense_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'unexpected_expense_account_id',config.unexpected_expense_account_id.id)
        
    def set_preferred_central_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'preferred_central_account_id',config.preferred_central_account_id.id)
        
    def set_discount_product_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'discount_product_id',config.discount_product_id.id)
	
    def set_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'journal_id',config.journal_id.id)

    def set_credit_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'credit_journal_id',config.credit_journal_id.id)
            
    def set_stock_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_journal_id',config.stock_journal_id.id)
        
    def set_stock_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_location_id',config.stock_location_id.id)
        
    def set_main_stock_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'main_stock_location_id',config.main_stock_location_id.id)
            
        
    def set_output_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'output_location_id',config.output_location_id.id)
        
    def set_expired_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'expired_location_id',config.expired_location_id.id)
    
    def set_broken_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'broken_location_id',config.broken_location_id.id)
        
    def set_income_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'income_account_id',config.income_account_id.id)
        
    def set_analytic_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'analytic_account_id',config.analytic_account_id.id)
        
    def set_stock_analytic_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_analytic_account_id',config.stock_analytic_account_id.id)
        
    def set_free_of_charge_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'free_of_charge_account_id',config.free_of_charge_account_id.id)
        
    def set_cost_of_item_sold_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'cost_of_item_sold_account_id',config.cost_of_item_sold_account_id.id)
    
    def set_cost_of_item_expired_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'cost_of_item_expired_account_id',config.cost_of_item_expired_account_id.id)
    
    def set_cost_of_item_broken_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'cost_of_item_broken_account_id',config.cost_of_item_broken_account_id.id)
        
    def set_stock_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_account_id',config.stock_account_id.id)
        
    	
    def set_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'help',config.help)
        
    def set_post_on_confirm(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'post_on_confirm',config.post_on_confirm)
        
    def set_enable_stock(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'enable_stock',config.enable_stock)
        
    def set_enable_lot_management(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'enable_lot_management',config.enable_lot_management)
        
    def set_payment_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.payment_account_ids:
            account_ids.append(account.id)
            if _debug:
                _logger.debug('==> Payment Account : %s', account_ids)
			
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'payment_account_ids',account_ids)
        
    def set_product_category_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'product_category_id',config.product_category_id.id)
        
    def set_preferred_method(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'preferred_method',config.preferred_method.id)
        
    def set_supplier_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'supplier_location_id',config.supplier_location_id.id)
        
    # get method
    
    def get_default_internal_account_id(self, cr, uid, ids, context=None):
        internal_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','internal_account_id')
        return {'internal_account_id' : internal_account_id }
            
    def get_default_unexpected_income_account_id(self, cr, uid, ids, context=None):
        unexpected_income_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','unexpected_income_account_id')
        return {'unexpected_income_account_id' : unexpected_income_account_id }
    
    def get_default_unexpected_expense_account_id(self, cr, uid, ids, context=None):
        unexpected_expense_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','unexpected_expense_account_id')
        return {'unexpected_expense_account_id' : unexpected_expense_account_id }
        
    def get_default_preferred_central_account_id(self, cr, uid, ids, context=None):
        preferred_central_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','preferred_central_account_id')
        return {'preferred_central_account_id' : preferred_central_account_id }
    
    def get_default_discount_account_id(self, cr, uid, ids, context=None):
        discount_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','discount_account_id')
        return {'discount_account_id' : discount_account_id }
    
    def get_default_discount_product_id(self, cr, uid, ids, context=None):
        discount_product_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','discount_product_id')
        return {'discount_product_id' : discount_product_id }
    
    def get_default_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','journal_id')
        return {'journal_id' : journal }

    def get_default_credit_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','credit_journal_id')
        return {'credit_journal_id' : journal }
        
    def get_default_stock_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_journal_id')
        return {'stock_journal_id' : journal }
        
    def get_default_stock_location_id(self, cr, uid, ids, context=None):
        stock = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_location_id')
        return {'stock_location_id' : stock }
        
    def get_default_main_stock_location_id(self, cr, uid, ids, context=None):
        stock = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','main_stock_location_id')
        return {'main_stock_location_id' : stock }
        
    def get_default_output_location_id(self, cr, uid, ids, context=None):
        output = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','output_location_id')
        return {'output_location_id' : output }    
    
    def get_default_expired_location_id(self, cr, uid, ids, context=None):
        output = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','expired_location_id')
        return {'expired_location_id' : output }    
    
    def get_default_broken_location_id(self, cr, uid, ids, context=None):
        output = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','broken_location_id')
        return {'broken_location_id' : output }    
        
    def get_default_analytic_account_id(self, cr, uid, ids, context=None):
        analytic_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','analytic_account_id')
        return {'analytic_account_id' : analytic_id }
        
    def get_default_stock_analytic_account_id(self, cr, uid, ids, context=None):
        analytic_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_analytic_account_id')
        return {'stock_analytic_account_id' : analytic_id }
        
    def get_default_free_of_charge_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','free_of_charge_account_id')
        return {'free_of_charge_account_id' : account_id }
        
    def get_default_income_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','income_account_id')
        return {'income_account_id' : account_id }
        
    def get_default_cost_of_item_sold_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','cost_of_item_sold_account_id')
        return {'cost_of_item_sold_account_id' : account_id }
    
    def get_default_cost_of_item_expired_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','cost_of_item_expired_account_id')
        return {'cost_of_item_expired_account_id' : account_id }
    
    def get_default_cost_of_item_broken_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','cost_of_item_broken_account_id')
        return {'cost_of_item_broken_account_id' : account_id }
	
    def get_default_stock_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_account_id')
        return {'stock_account_id' : account_id }
        
    	
    def get_default_help(self, cr, uid, ids, context=None):
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','help')
        return {'help' : help or self._help}
        
    def get_default_post_on_confirm(self, cr, uid, ids, context=None):
        poc = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','post_on_confirm')
        return {'post_on_confirm' : poc }
    
    def get_default_enable_stock(self, cr, uid, ids, context=None):
        stock = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','enable_stock')
        return {'enable_stock' : stock }
        
    def get_default_enable_lot_management(self, cr, uid, ids, context=None):
        lot_mng = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','enable_lot_management')
        return {'enable_lot_management' : lot_mng }
    
    def get_default_supplier_location_id(self, cr, uid, ids, context=None):
        supplier_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','supplier_location_id')
        return {'supplier_location_id' : supplier_location_id }
    
    	
    def get_default_payment_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.pharmacy.pos', 'payment_account_ids')
		
        return {'payment_account_ids' : account_ids}
        
    def get_default_central_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.pharmacy.pos', 'central_account_ids')
		
        return {'central_account_ids' : account_ids}
        
    def get_default_product_category_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        product_category_id = ir_values.get_default(cr, uid, 'isf.pharmacy.pos', 'product_category_id')
		
        return {'product_category_id' : product_category_id}
        
    def onchange_enable_stock(self, cr, uid, ids, enable_stock,context=None):
        result = {'value' : {}}
        
        result['value'].update({
            'enable_stock' : enable_stock,
        })
        
        return result
        
    def onchange_enable_lot_management(self, cr, uid, ids, enable_lot,context=None):
        result = {'value' : {}}
        
        result['value'].update({
            'enable_lot_management' : enable_lot,
        })
        
        return result
        
    def get_default_preferred_method(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','preferred_method')
        return {'preferred_method' : account_id } 
        
#################################################################################################################################
####################################################### COMPOUND JOURNAL ENTRY ##################################################
#################################################################################################################################


class isf_compound_journal_entry_layout_config_settings(osv.osv_memory):
    _name = 'isf.compound.journal.entry.layout.config.settings'
    _inherit = 'res.config.settings'
    
    _columns = {
        #sale
        'journal_sale_id' : fields.many2one('account.journal','Dafault Sale Journal', required=True, domain=[('type','=','sale')]),
        'sale_cash_bank_account_ids' : fields.many2many('account.account','isf_compound_income_account_cb_rel',string="Cash/Bank Accounts", required=True, store=False),
        'sale_account_ids' : fields.many2many('account.account','isf_compound_income_account_rel',string="Income Accounts", required=True, domain=[('type','!=','view')], store=False),
        'help_sale' : fields.text('Help Sale', size=512, required=False),
        # purchase
        'journal_purchase_id' : fields.many2one('account.journal','Default Purchase Journal', required=True,domain=[('type','=','purchase')]),
        'purchase_cash_bank_account_ids' : fields.many2many('account.account','isf_compound_purchase_account_cb_rel',string="Cash/Bank Accounts", required=True, store=False),
        'purchase_account_ids' : fields.many2many('account.account','isf_compound_purchase_account_rel',string="Purchase Accounts", required=True, domain=[('type','!=','view')], store=False),
        'help_purchase' : fields.text('Help Purchase', size=512, required=False),
    }
    
    def _get_default_help_sale(self, cr, uid, context=None):
        help = 'Banks, Cash and Paybale accounts must be debited, Income accounts must be credited'
        return help
        
    def _get_default_help_purchase(self, cr, uid, context=None):
        help = 'Expense accounts must be debited, Banks, Cash and Receivable accounts must be credited'
        return help
    
    _defaults = {
        'help_sale' : _get_default_help_sale,
        'help_purchase' : _get_default_help_purchase,
    }
    
    # Set methods
    
    def set_help_sale(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'help_sale', config.help_sale)
        
    def set_help_purchase(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'help_purchase', config.help_purchase)
    
    def set_journal_sale_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'journal_sale_id',config.journal_sale_id.id)
    
    def set_journal_purchase_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'journal_purchase_id',config.journal_purchase_id.id)
    
    def set_sale_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.sale_cash_bank_account_ids:
            account_ids.append(account.id)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_cash_bank_account_ids',account_ids)
        
    def set_sale_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.sale_account_ids:
            account_ids.append(account.id)
		
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_account_ids',account_ids)
    
    def set_purchase_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.purchase_cash_bank_account_ids:
            account_ids.append(account.id)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_cash_bank_account_ids',account_ids)
        
    def set_purchase_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        
        account_ids = []
        for account in config.purchase_account_ids:
            account_ids.append(account.id)
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_account_ids',account_ids)
    
    # Get Methods
    
    def get_default_journal_sale_id(self, cr, uid, ids, context=None):
        journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','journal_sale_id')
        return {'journal_sale_id' : journal_id }
        
    def get_default_journal_purchase_id(self, cr, uid, ids, context=None):
        journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','journal_purchase_id')
        return {'journal_purchase_id' : journal_id }
        
    def get_default_help_sale(self, cr, uid, ids, context=None):
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','help_sale')
        if help is None:
            help = self._get_default_help_sale(cr, uid, context=context)
        return {'help_sale' : help}
        
    def get_default_help_sale(self, cr, uid, ids, context=None):
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.compound.journal.entry.layout','help_purchase')
        if help is None:
            help = self._get_default_help_purchase(cr, uid, context=context)
        return {'help_purchase' : help}
        
    def get_default_sale_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_cash_bank_account_ids')
		
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
		
        return {'sale_cash_bank_account_ids' : valid_ids}
        
    def get_default_sale_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_account_ids')
		
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
		
        return {'sale_account_ids' : valid_ids}
    
    def get_default_purchase_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_cash_bank_account_ids')
		
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
		
        return {'purchase_cash_bank_account_ids' : valid_ids}
    
    def get_default_purchase_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_account_ids')
        
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
        
        return {'purchase_account_ids' : valid_ids}

#################################################################################################################################
################################################################# ISF POS #######################################################
#################################################################################################################################       

class isf_pos_config_settings(osv.osv_memory):
    _name = 'isf.pos.config.settings'
    _inherit = 'res.config.settings'
    _help = 'Registers receipts and documents of direct incomes, without invoicing by the hospital. It can be used to record receipts like direct sale of drugs, medical services or donations and rentals. For multiple Analytic Account use the Multiple Incomes feature'
	
    _columns = {
        'cash_bank_account_ids' : fields.many2many('account.account',string='Account',required=True),
        'preferred_method_id' : fields.many2one('account.account',string='Preferred Account',required=True),
        'dest_location_id' : fields.many2one('stock.location','Output Location', required=True),
        'expired_location_id' : fields.many2one('stock.location','Location (Expired/Scrapped)',required=True),
        'internal_location_id' : fields.many2one('stock.location','Internal Location',required=True),
        'central_account_ids' : fields.many2many('account.account','isf_pos_central_account_rel','pos_id','account_id',string='Central Accounts'),
        'unexpected_income_account_id' : fields.many2one('account.account','Unexpected Income'),
        'unexpected_expense_account_id' : fields.many2one('account.account','Unexpected Expense'),
        'product_discount_id' : fields.many2one('product.product','Discount Product',required=True),
        'account_discount_id' : fields.many2one('account.account','Discount Account',required=True),
        'enable_stock' : fields.boolean('Enable Stock', required=False),
    }
    
    _defaults = {
        'enable_stock' : True,
    }
    
    def set_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.cash_bank_account_ids:
            account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'cash_bank_account_ids',account_ids)
        
    def set_central_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.central_account_ids:
            account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'central_account_ids',account_ids)
        
    def set_preferred_method_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'preferred_method_id',config.preferred_method_id.id)
        
    def set_dest_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'dest_location_id',config.dest_location_id.id)
    
    def set_expired_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'expired_location_id',config.expired_location_id.id)
    
    def set_internal_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'internal_location_id',config.internal_location_id.id)
        
    def set_unexpected_income_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'unexpected_income_account_id',config.unexpected_income_account_id.id)
        
    def set_unexpected_expense_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'unexpected_expense_account_id',config.unexpected_expense_account_id.id)

    def set_product_discount_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'product_discount_id',config.product_discount_id.id)

    def set_account_discount_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'account_discount_id',config.account_discount_id.id)
    
    def set_enable_stock(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'enable_stock',config.enable_stock)
        
    #
    # GET Method
    #
    
    def get_default_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.pos.config', 'cash_bank_account_ids')
        
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
		
        return {'cash_bank_account_ids' : valid_ids}
        
    def get_default_central_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.pos.config', 'central_account_ids')
        
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
		
        return {'central_account_ids' : valid_ids}
        
    def get_default_preferred_method_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','preferred_method_id')
        return {'preferred_method_id' : account_id }
        
    def get_default_dest_location_id(self, cr, uid, ids, context=None):
        dest_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','dest_location_id')
        return {'dest_location_id' : dest_location_id }
        
    def get_default_expired_location_id(self, cr, uid, ids, context=None):
        expired_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','expired_location_id')
        return {'expired_location_id' : expired_location_id }
        
    def get_default_internal_location_id(self, cr, uid, ids, context=None):
        internal_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','internal_location_id')
        return {'internal_location_id' : internal_location_id }
        
    def get_default_unexpected_income_account_id(self, cr, uid, ids, context=None):
        unexpected_income_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','unexpected_income_account_id')
        return {'unexpected_income_account_id' : unexpected_income_account_id }
    
    def get_default_unexpected_expense_account_id(self, cr, uid, ids, context=None):
        unexpected_expense_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','unexpected_expense_account_id')
        return {'unexpected_expense_account_id' : unexpected_expense_account_id }

    def get_default_product_discount_id(self, cr, uid, ids, context=None):
        product_discount_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','product_discount_id')
        return {'product_discount_id' : product_discount_id }

    def get_default_account_discount_id(self, cr, uid, ids, context=None):
        account_discount_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','account_discount_id')
        return {'account_discount_id' : account_discount_id }
    
    def get_default_enable_stock(self, cr, uid, ids, context=None):
        stock = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','enable_stock')
        return {'enable_stock' : stock }

###################################################################################################################################
################################################################# ISF STOCK #######################################################
###################################################################################################################################

class isf_stock_move_config_settings(osv.osv_memory):
    _name = 'isf.stock.move.config.settings'
    _inherit = 'res.config.settings'
	
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal',required=True),
        'account_id' : fields.many2one('account.account','Account',required=True),
        'donor_account_id' : fields.many2one('account.account','Donors Account',required=True),
        'supplier_location_id' : fields.many2one('stock.location','Supplier Location',required=True),
        'inventory_account_id' : fields.many2one('account.account','Inventory',required=True),
        'expired_account_id' : fields.many2one('account.account','Expired Account',required=True),
        'loss_account_id' : fields.many2one('account.account','Broken\Loss Account',required=True),
        'expired_location_id' : fields.many2one('stock.location','Expired Location',required=True),
        'loss_location_id' : fields.many2one('stock.location','Broken\Loss Location',required=True),
        'enable_stock' : fields.boolean('Enable Stock', required=False),
    }
    
    # Set method
	
    def set_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'journal_id',config.journal_id.id)
        
    def set_inventory_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'inventory_account_id',config.inventory_account_id.id)
    
    def set_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'account_id',config.account_id.id)
        
    def set_donor_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'donor_account_id',config.donor_account_id.id)
    
    def set_expired_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'expired_account_id',config.expired_account_id.id)
    
    def set_loss_account_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'loss_account_id',config.loss_account_id.id)
    
    def set_supplier_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'supplier_location_id',config.supplier_location_id.id)
    
    def set_expired_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'expired_location_id',config.expired_location_id.id)
     
    def set_loss_location_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'loss_location_id',config.loss_location_id.id)
        
    def set_enable_stock(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'enable_stock',config.enable_stock)
    
    # Get method
    
    def get_default_journal_id(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','journal_id')
        return {'journal_id' : journal }
        
    def get_default_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','account_id')
        return {'account_id' : account_id }
        
    def get_default_expired_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','expired_account_id')
        return {'expired_account_id' : account_id }
        
    def get_default_loss_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','loss_account_id')
        return {'loss_account_id' : account_id }
    
    def get_default_inventory_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','inventory_account_id')
        return {'inventory_account_id' : account_id }
        
    def get_default_donor_account_id(self, cr, uid, ids, context=None):
        account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','donor_account_id')
        return {'donor_account_id' : account_id }
        
    def get_default_supplier_location_id(self, cr, uid, ids, context=None):
        supplier_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','supplier_location_id')
        return {'supplier_location_id' : supplier_location_id }
        
    def get_default_expired_location_id(self, cr, uid, ids, context=None):
        expired_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','expired_location_id')
        return {'expired_location_id' : expired_location_id }
        
    def get_default_loss_location_id(self, cr, uid, ids, context=None):
        loss_location_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','loss_location_id')
        return {'loss_location_id' : loss_location_id }
    
    def get_default_enable_stock(self, cr, uid, ids, context=None):
        stock = self.pool.get('ir.values').get_default(cr, uid, 'isf.stock.move','enable_stock')
        return {'enable_stock' : stock }

###################################################################################################################################
####################################################### ISF PURCHASE WITHOUT INVOICE ##############################################
###################################################################################################################################
    
class isf_purchase_without_invoice_config_settings(osv.osv_memory):
    _name = 'isf.purchase.without.invoice.config.settings'
    _inherit = 'res.config.settings'
    _help = 'Registers receipts and documents of direct expenses, without invoice from supplier. It can be used to record direct expenses like fuel purchases or bank charges. For multiple Analytic Account use the Multiple Expenses feature'
    
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal',required=True),
        'cash_bank_account_ids' : fields.many2many('account.account','cash_bank_account_ids_isf_purchase_without_invoice_rel',string='Cash/Bank',required=True,store=False,),
        'expense_account_ids' : fields.many2many('account.account','expense_account_ids_isf_purchase_without_invoice_rel',string='Expense',required=True,store=False),
        'analytic_account_ids' : fields.many2many('account.analytic.account','account_analytic_ids_isf_purchase_without_invoice_rel',string='Analytic',required=False,domain=[('type','!=','view')],store=False),
        'help' : fields.text('Help',size=512,required=True),
    }
    
    # Set Methods
    
    def set_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'journal_id',config.journal_id.id)
		
    def set_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'help',config.help)
        
    def set_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.cash_bank_account_ids:
            account_ids.append(account.id)
			
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'cash_bank_account_ids',account_ids)
        
    def set_expense_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.expense_account_ids:
            account_ids.append(account.id)
			
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'expense_account_ids',account_ids)
        
    def set_analytic_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        for account in config.analytic_account_ids:
            account_ids.append(account.id)
            if _debug:
                _logger.debug('Analytic : %s', config.analytic_account_ids)
                _logger.debug('==> Analytic Account : %s', account_ids)
                	
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'analytic_account_ids',account_ids)
        
    # Get Methods
    
    def get_default_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'cash_bank_account_ids')
		
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
    		
        return {'cash_bank_account_ids' : valid_ids}
		
    def get_default_expense_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'expense_account_ids')
		
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
            
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in valid_account_ids:
                    valid_ids.append(account)
           
        return {'expense_account_ids' : valid_ids}
		
    def get_default_analytic_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = []
        account_ids = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'analytic_account_ids')
		
        analytic_account_obj = self.pool.get('account.analytic.account')
        analytic_account_ids = analytic_account_obj.search(cr, uid, [])
            
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if account in analytic_account_ids:
                    valid_ids.append(account)
        
        return {'analytic_account_ids' : valid_ids}
		
    def get_default_journal(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.purchase.without.invoice','journal_id')
        return {'journal_id' : journal }
		
    def get_default_help(self, cr, uid, ids, context=None):
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.purchase.without.invoice','help')
        return {'help' : help or self._help}     
    
    def _get_default_help(self, cr, uid, context=None):
        return self._help 
        
###################################################################################################################################
######################################################### ISF SALE WITHOUT INVOICE ################################################
###################################################################################################################################
class isf_sale_without_invoice_config_settings(osv.osv_memory):
    _name = 'isf.sale.without.invoice.config.settings'
    _inherit = 'res.config.settings'
    _help = 'Registers receipts and documents of direct incomes, without invoicing by the hospital. It can be used to record receipts like direct sale of drugs, medical services or donations and rentals. For multiple Analytic Account use the Multiple Incomes feature'
	
    _columns = {
        'cash_bank_account_ids' : fields.many2many('account.account',string='Account',required=True),
        'income_account_ids' : fields.many2many('account.account','income_account_ids_sale_without_invoice_rel',string='Income',required=True),
        'journal_id' : fields.many2one('account.journal','Journal',required=True),
        'analytic_account_ids' : fields.many2many('account.analytic.account','analytic_account_ids_sale_without_invoice_rel',string='Analytic',domain=[('type','!=','view')],required=False),
        'help' : fields.text('Help', size=512, required=True),
    }
	
    def _get_help(self, cr, uid, context=None):
        return self._help
		
    def _get_name(self, cr, uid, context=None):
        return self._name
    
    _defaults = {
        'help' : _get_help,
    }
	
	# Set method
	
    def set_journal_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'journal_id',config.journal_id.id)
		
    def set_help(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'help',config.help)
		
    def set_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        if config.cash_bank_account_ids is not None:
            for account in config.cash_bank_account_ids:
                account_ids.append(account.id)
			
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'cash_bank_account_ids',account_ids)
		
    def set_income_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        if config.income_account_ids is not None:
            for account in config.income_account_ids:
                account_ids.append(account.id)
			
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'income_account_ids',account_ids)
		
    def set_analytic_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
		
        account_ids = []
        if config.analytic_account_ids is not None:
            for account in config.analytic_account_ids:
                account_ids.append(account.id)
            
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'analytic_account_ids',account_ids)
		
	# get method
	
    def get_default_cash_bank_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.sale.without.invoice', 'cash_bank_account_ids')
        
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        if _debug:
            _logger.debug('==> Cash\Bank get')

        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if valid_account_ids is not None:
                    if account in valid_account_ids:
                        valid_ids.append(account)
        		
        return {'cash_bank_account_ids' : valid_ids}
		
    def get_default_income_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = ir_values.get_default(cr, uid, 'isf.sale.without.invoice', 'income_account_ids')
		
		
        valid_account_obj = self.pool.get('account.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if valid_account_ids is not None:
                    if account in valid_account_ids:
                        valid_ids.append(account)
        		
        return {'income_account_ids' : valid_ids}
		
    def get_default_analytic_account_ids(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        account_ids = []
        account_ids = ir_values.get_default(cr, uid, 'isf.sale.without.invoice', 'analytic_account_ids')
		

        valid_account_obj = self.pool.get('account.analytic.account')
        valid_account_ids = valid_account_obj.search(cr, uid, [])
        
        valid_ids = []
        if account_ids is not None:
            for account in account_ids:
                if valid_account_ids is not None:
                    if account in valid_account_ids:
                        valid_ids.append(account)
        		
        
        return {'analytic_account_ids' : valid_ids}
		
    def get_default_journal(self, cr, uid, ids, context=None):
        journal = self.pool.get('ir.values').get_default(cr, uid, 'isf.sale.without.invoice','journal_id')
        return {'journal_id' : journal }
		
    def get_default_help(self, cr, uid, ids, context=None):
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.sale.without.invoice','help')
        return {'help' : help or self._help}
		
		    
