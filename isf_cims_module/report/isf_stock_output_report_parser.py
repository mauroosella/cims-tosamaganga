# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_output_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.output.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_stock_output_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.output')
        
        active_id = ctx.get('active_id')
        moves_ids = obj_report.search(self.cr, self.uid, [('id','=',active_id)])
        moves = obj_report.browse(self.cr, self.uid, moves_ids)[0]
        
        total_price = 0.0
        for line in moves.line_ids:
            res = {
                'lot_id' : line.stock_id.name,
                'expiry_date' : line.stock_id.ref,
                'product' : "["+line.product_id.default_code+"] " + line.product_id.name,
                'qty' : line.quantity,
            }
            self.result_acc.append(res)
        
        return self.result_acc
        
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.output')
        
        active_id = ctx.get('active_id')
        moves_ids = obj_report.search(self.cr, self.uid, [('id','=',active_id)])
        moves = obj_report.browse(self.cr, self.uid, moves_ids)[0]
        
        out_type = 'Expired'
        if moves.output_type == 'broken':
            out_type = 'Broken\Lost'
        
        res = {
            'date' : moves.date,
            'location' : moves.location_id.name,
            'desc' : moves.name,
            'sequence_id' : moves.sequence_id,
            'type' : out_type,
        }
        self.headers_acc.append(res)
        
        if _debug:
            _logger.debug('Table header : %s',res)
        
        
        return self.headers_acc
            
    

report_sxw.report_sxw('report.isf.stock.output.webkit', 'isf.stock.output', 'addons/isf_cims_module/report/pp_output.mako', parser=isf_stock_output_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
