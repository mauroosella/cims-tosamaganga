from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=False

class isf_account_report_general_ledger(osv.osv_memory):
    _inherit = 'account.report.general.ledger'
    _name = 'account.report.general.ledger'
    
    _defaults = {
        'journal_ids' : [],
    }
    
    def onchange_journal(self, cr, uid, ids, journal_ids, context=None):
        if _debug:
            _logger.debug('Journal IDS : %s', journal_ids)
            
        return True
    
isf_account_report_general_ledger()   
    
        