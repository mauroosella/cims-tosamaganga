# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import platform
from datetime import datetime
from datetime import timedelta

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_monthly_income_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.monthly.income.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_monthly_income_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'sub_lines': self.sub_lines,
            'datelines' : self.datelines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.result_date = []
        self.header_acc = []
        self.result_accounts = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_iae_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    def datelines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.monthly.income.wizard')
        isf_lib = self.pool.get('isf.lib.utils')
        report_date_period = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['period_id'])
        report_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['print_date'])
        period_id = report_date_period[0]['period_id']
        period = isf_lib._get_period_date(self.cr, self.uid, period_id[0])
        print_date = report_date[0]['print_date']
        
        if _debug:
            _logger.debug('==> Period : %s',period)
        
        res = {
            'begin_date' : period['date_start'],
            'end_date' : period['date_stop'],
            'print_date' : print_date,
        }
        
        self.result_date.append(res)
        
        return self.result_date
    
    def headers(self):

        obj_analytic = self.pool.get('account.analytic.account')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.monthly.income.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['analytic_account_ids'])
        analytic_ids_unsorted = report_data[0]['analytic_account_ids']
        analytic_ids = obj_analytic.search(self.cr, self.uid, [('id','in',analytic_ids_unsorted)], order='code')
        
        res = {
                'analytic_account' : 'Date',
            }
        self.header_acc.append(res)
        
        for analytic in obj_analytic.browse(self.cr, self.uid, analytic_ids):
            res = {
                'analytic_account' : analytic.name + ' ' + analytic.code,
            }
            
            if _debug:
                _logger.debug('==> Header : %s', analytic.name)
        
            self.header_acc.append(res)
        
        res = {
                'analytic_account' : 'Total',
        }
        self.header_acc.append(res)
        
        return self.header_acc
    
    def sub_lines(self, ids=None, done=None):
        obj_account = self.pool.get('account.account')
        obj_move_line = self.pool.get('account.move.line')
        obj_report = self.pool.get('isf.monthly.income.wizard')
        
        isf_lib = self.pool.get('isf.lib.utils')
        ctx = self.context.copy()
        report_period = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['period_id'])
        period_id = report_period[0]['period_id']
        period = isf_lib._get_period_date(self.cr, self.uid, period_id[0])
        begin_date = period['date_start']
        end_date = period['date_stop']
        date = datetime.strptime(begin_date,"%Y-%m-%d").date()
        end_date_date = datetime.strptime(end_date,"%Y-%m-%d").date()
        
        target_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['entry_type'])
        target = target_obj[0]['entry_type']
        
        account_ids = obj_account.search(self.cr, self.uid, [('id','in',self.result_accounts)],order='code')
        accounts = obj_account.browse(self.cr, self.uid, account_ids)
        
        total_debit = 0.0
        total_credit = 0.0
        result_lines = []
        total_posted = True
        for account in accounts:
            account_debit = 0.0
            account_credit = 0.0
            posted = True
            if target == 'posted':
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('date','>=',date),('date','<=',end_date_date),('move_id.state','=','posted')])
            else:
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('date','>=',date),('date','<=',end_date_date)])
            for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
#                 if move.analytic_account_id.id:
                    account_debit = account_debit + move.debit
                    account_credit = account_credit + move.credit
                    total_debit = total_debit + move.debit
                    total_credit = total_credit + move.credit
                    posted = posted and move.move_id.state == 'posted'
                    total_posted = total_posted and posted
            
            res = {'name' : account.code + " - " + account.name,
                   'debit' : locale.format("%20.0f",account_debit, grouping=True),
                   'credit' : locale.format("%20.0f",account_credit, grouping=True),
                   'posted' : posted}
            result_lines.append(res)
            
        res = {'name' : 'Total',
               'debit' : locale.format("%20.0f",total_debit, grouping=True),
               'credit' : locale.format("%20.0f",total_credit, grouping=True),
               'posted' : total_posted}
        result_lines.append(res)
        
        return result_lines
        

    def lines(self, ids=None, done=None):
        obj_analytic = self.pool.get('account.analytic.account')
        obj_move_line = self.pool.get('account.move.line')
#         obj_account = self.pool.get('account.account')
        isf_lib = self.pool.get('isf.lib.utils')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.monthly.income.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['analytic_account_ids'])
        report_period = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['period_id'])
#         report_income_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['income_account_ids'])
#         report_cash_bank_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['cash_bank_account_ids'])
        target_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['entry_type'])
        
        analytic_ids_unsorted = report_data[0]['analytic_account_ids']
        analytic_ids = obj_analytic.search(self.cr, self.uid, [('id','in',analytic_ids_unsorted)], order='code')
        
        
        period_id = report_period[0]['period_id']
        period = isf_lib._get_period_date(self.cr, self.uid, period_id[0])
        begin_date = period['date_start']
        end_date = period['date_stop']
#         cash_bank_account_ids = report_cash_bank_account[0]['cash_bank_account_ids']
#         income_account_ids = report_income_account[0]['income_account_ids']
        target = target_obj[0]['entry_type']
        
        
        if not ids:
            ids = self.ids
        if not ids:
            return []
            
        total_per_column = {}
        posted_per_column = {}
        
        date_ids = []
        date = datetime.strptime(begin_date,"%Y-%m-%d").date()
        end_date_date = datetime.strptime(end_date,"%Y-%m-%d").date()
        
        while date <= end_date_date:
            date_ids.append(date)
            date = date + timedelta(days=1)
        
        if _debug:
            _logger.debug('==> date IDS : %s',date_ids)
            _logger.debug('==> analytic_ids_unsorted : %s',analytic_ids_unsorted)
            _logger.debug('==> analytic_ids (sorted): %s',analytic_ids)
        
        date_ids.sort()
        
        #
        # Cash/Bank account included in Income
        #
        include_move_ids = []
#         for date in date_ids:
#             for account in obj_account.browse(self.cr, self.uid, cash_bank_account_ids):
#                 if target == 'posted':
#                     include_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('date','=',date),('move_id.state','=','posted')]) 
#                 else:
#                     include_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('date','=',date)]) 
#                 for line in obj_move_line.browse(self.cr, self.uid, include_line_ids):
#                     if line.move_id.journal_id.type == 'sale':
#                         include_move_ids.append(line.move_id.id)
        #
        # INCOME ACCOUNT
        #   
        first_account = '1'
        overall_posted = True
        for date in date_ids:
#             for account in obj_account.browse(self.cr, self.uid, income_account_ids):
                print_account = '1'
                total_account_amount = 0.0
                line = []
                addLine = False
                aIndex = 0
                total_for_line = 0.0
                for analytic in obj_analytic.browse(self.cr, self.uid, analytic_ids):
                    res = {
                        'first_account' : first_account,
                        'is_income' : '1',
                        'is_total' : '0',
                        'print_account' : print_account,
                        'date' : date.day,
                        'analytic_account' : analytic.name,
                        'posted' : True,
                    }
                    print_account = '0'
                    first_account = '0'
                    account_amount = 0.0
                    posted = True
                    if target == 'posted':
                        move_line_ids = obj_move_line.search(self.cr, self.uid, [('analytic_account_id','=', analytic.id),('date','=',date),('move_id.state','=','posted')])
                    else:
                        move_line_ids = obj_move_line.search(self.cr, self.uid, [('analytic_account_id','=', analytic.id),('date','=',date)])
                    for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
    #                         if move.move_id.id in include_move_ids:
                            account_amount = account_amount - move.debit + move.credit
                            posted = posted and move.move_id.state == 'posted'
                            overall_posted = overall_posted and posted
                            
                            if move.account_id.id not in self.result_accounts:
                                self.result_accounts.append(move.account_id.id)
                            
                    if account_amount != 0.0:
                        addLine = True
                
                    account_amount_locale = locale.format("%20.0f",account_amount, grouping=True)
                    res.update({
                        'amount' : account_amount_locale,
                        'posted' : posted,
                    })
                    total_for_line += account_amount
                
                    total_account_amount = total_account_amount + account_amount
                
                    if aIndex not in total_per_column:
                        total_per_column[aIndex] = 0.0
                        posted_per_column[aIndex] = True    
                    total_per_column[aIndex] = total_per_column[aIndex] + account_amount
                    posted_per_column[aIndex] = posted
                    aIndex = aIndex + 1
                
                    line.append(res)
            
                if addLine:
                    posted = True
                    
                    for res in line:    
                        self.result_acc.append(res)
                        posted = posted and res['posted']
                        
                        if _debug:
                            _logger.debug('==> (addLine) res : %s',res)
                    
                    total_line_locale = locale.format("%10.0f",total_for_line, grouping=True)
                    res = {
                        'first_account' : '0',
                        'is_income' : '1',
                        'is_total' : '1',
                        'print_account' : '0',
                        'date' : False,
                        'amount' : total_line_locale,
                        'posted' : posted,
                    }
                    self.result_acc.append(res)
                    
                    if _debug:
                        _logger.debug('==> (addLine) res : %s',res)
            
                    # Total for account
                    #total_account_amount_locale = locale.format("%20.0f",total_account_amount, grouping=True)
                    #res = {
                    #    'first_account' : '0',
                    #    'is_income' : '0',
                    #    'is_total' : '1',
                    #    'print_account' : '0',
                    #    'amount' : total_account_amount_locale,
                    #}
            
                    #self.result_acc.append(res)
                else:
                    if line[0]['first_account'] == '1':
                        first_account = '1'
                        
        #
        # TOTALS PER COLUMN
        #
        print_account = '1'
        for k in total_per_column:
            total_account_amount_locale = locale.format("%20.0f",total_per_column[k], grouping=True)
            
            if _debug:
                _logger.debug('==> K : %s , V : %f', k, total_per_column[k])
            
            res = {
                'first_account' : '0',
                'is_income' : '0',
                'is_total' : '1',
                'print_account' : print_account,
                'date' : 'Totals',
                'amount' : total_account_amount_locale,
                'posted' : posted_per_column[k],
            }
            print_account = '0'
            
            self.result_acc.append(res)
            
        #
        # TOTALS OF TOTALS
        #
        total_of_totals = 0.0
        for k in total_per_column:
            total_of_totals += total_per_column[k]
        
        total_of_totals_locale = locale.format("%20.0f",total_of_totals, grouping=True)
        res = {
            'first_account' : '0',
            'is_income' : '0',
            'is_total' : '1',
            'print_account' : '0',
            'account' : 'Totals',
            'amount' : total_of_totals_locale,
            'posted' : overall_posted,
        }
        self.result_acc.append(res)
        
#         if _debug:
#             _logger.debug('==> result_acc : %s', self.result_acc)
#             _logger.debug('==> result_accounts : %s', self.result_accounts)
           
        return self.result_acc

report_sxw.report_sxw('report.isf.monthly.income.webkit', 'isf.monthly.income.wizard', 'addons/isf_cims_module/report/mi.mako', parser=isf_monthly_income_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
