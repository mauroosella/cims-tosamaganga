# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug = False

class isf_bs_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.bs.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_bs_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
#             'headerlines' : self.headerlines,
#             'chequelines' : self.chequelines,
        })
#         self.context = context
#         self.result_acc = []
#         self.result_header = []
#         self.result_cheque = []
        _logger.debug('localcontext : %s',self.localcontext)

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_bs_object(self, ids):
        bs_obj = self.pool.get('isf.bank.account.checking')
        
        _logger.debug('_get_bs_object : %s',ids)
        
        bs = bs_obj.browse(self.cr, self.uid, ids)
        
        return bs
    
    def headerlines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.bs.bank.checking')
        statement = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['bank_statement_id'])
        
        bs_id = statement[0]['bank_statement_id'][0]
        bs = self._get_bs_object(bs_id)
        
        ending_balance_locale = total_locale =   locale.format("%20.2f",bs.total_amount, grouping=True)  
        res = {
            'name' : bs.name,
            'account' : bs.account_id.code+" "+bs.account_id.name,
            'period' : bs.period_id.name,
            'oe_ending_balance' : ending_balance_locale
        }
        
        
        self.result_header.append(res)
        
        return self.result_header

    def chequelines(self):
        _logger.debug('Looking for cheque lines')
    
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.bs.bank.checking')
        obj_lines = self.pool.get('isf.bank.account.checking.lines')
        obj_bank_checking = self.pool.get('isf.bank.account.checking')
        statement = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['bank_statement_id'])
        bs_id = statement[0]['bank_statement_id'][0]
        bs = self._get_bs_object(bs_id)
        obj_lines_ids = obj_lines.search(self.cr, self.uid, [('statement_id','=',bs_id)])
            
        _logger.debug('BS ID : %d', bs_id)
        _logger.debug('Line ids : %s', obj_lines_ids)
        
        number = 1
        for line in obj_lines.browse(self.cr, self.uid, obj_lines_ids):
            _logger.debug('Checking lines')
            if not line.checked:
                _logger.debug('==> Outstanding cheque')
                amount_locale = locale.format("%20.2f",line.amount, grouping=True)
                res = {
                    'number' : line.reference,
                    'amount' : amount_locale,
                } 
                
                number = number + 1       
            
                self.result_cheque.append(res)
            
        return self.result_cheque
            
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.bs.bank.checking')
        obj_lines = self.pool.get('isf.bank.account.checking.lines')
        obj_bank_checking = self.pool.get('isf.bank.account.checking')
        statement = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['bank_statement_id'])
        bs_id = statement[0]['bank_statement_id'][0]
        bs = self._get_bs_object(bs_id)
        obj_lines_ids = obj_lines.search(self.cr, self.uid, [('statement_id','=',bs_id)])
        
        _logger.debug('Line IDS : %s', bs.line_ids)
            
        balance = bs.starting_balance
        unclear = 0.0
        cleared_receipt = 0.0
        cleared_payments = 0.0
        for line in obj_lines.browse(self.cr, self.uid, obj_lines_ids):
            if line.checked:
                if line.move_line_id.period_id.id == bs.period_id.id:
                    balance = balance + line.amount
                if line.amount > 0.0:
                    cleared_receipt = cleared_receipt + line.amount
                if line.amount < 0.0:
                    cleared_payments = cleared_payments + abs(line.amount)
            else:
                unclear = unclear + abs(line.amount)
        
        #balance = balance - bs.outstandig_cheque_prev_period
        #cleared_receipt = bs.starting_balance - balance
        ob_locale = locale.format("%20.2f",bs.starting_balance, grouping=True)  
        total_locale =   locale.format("%20.2f",bs.starting_balance + cleared_receipt, grouping=True)  
        balance_locale = locale.format("%20.2f",balance, grouping=True)
        unclear_locale = locale.format("%20.2f",unclear, grouping=True)           
        cleared_receipt_locale = locale.format("%20.2f",cleared_receipt, grouping=True)
        cleared_payments_locale = locale.format("%20.2f",cleared_payments, grouping=True)
        
        res = {
            'ob' : ob_locale,
            'balance' : balance_locale,
            'unclear' : unclear_locale,
            'total' : total_locale,
            'cleared_receipt' : cleared_receipt_locale,
            'cleared_payments' : cleared_payments_locale,
        }
            
            
        self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.bs.webkit', 'isf.bs.bank.checking', 'addons/isf_cims_module/report/bs.mako', parser=isf_bs_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
