<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <center>
        <h3>Stock Expired</h3>
    </center>
    %for data in headers():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Location</th>
                <th class="td-data" style="width: 40%;">Date</th>
                <th class="td-data" style="width: 20%;">Currency</th>
            </tr>
            <tr>
                <th class="td-data" style="width: 40%;">${data['location']}</th>
                <td class="td-data" style="width: 40%;">${data['date']}</td>
                <td class="td-data" style="width: 20%;">${data['currency']}</td>
            </tr>
        </table>
    </center>
    %endfor
    <br/>
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 5%;">Lot ID</th>
                <th class="td-data" style="width: 30%;">Product</th>
                <th class="td-data" style="width: 15%;">Cost Price</th>
                <th class="td-data" style="width: 10%;">Qty</th>
                <th class="td-data" style="width: 15%;">Total Cost</th>
                <th class="td-data" style="width: 25%;">Expiry Date</th>
            </tr>
            %for data in lines():
                %if data['is_total'] == '0':
                <tr>
                    <th class="td-data" style="width: 5%;">${data['stock']}</th>
                    <td class="td-data" style="width: 30%;">${data['product']}</td>
                    <td class="td-data" style="width: 15%;text-align:right;">${data['cost_price']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['qty']}</td>
                    <td class="td-data" style="width: 15%;text-align:right;">${data['total_cost']}</td>
                    <td class="td-data" style="width: 25%;text-align:right;">${data['expiry_date']}</td>
                </tr>
                %endif
                %if data['is_total'] == '1':
                    <tr>
                        <th class="td-data-nb" style="width: 50%;" colspan="3"/>
                        <th class="td-data-nb" style="width: 10%;" ><b>Total : </b></td>
                        <th class="td-data-nb" style="width: 15%;text-align:right;" ><b>${data['total_cost']}</b></td>
                        <th class="td-data-nb" style="width: 25%;" />
                    </tr>
                %endif
            %endfor
        </table>
    </center>
</body>
</html>
