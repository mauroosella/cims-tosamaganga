# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_move_donation_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.move.donation.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_stock_move_donation_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.donation')
        
        active_id = ctx.get('active_id')
        moves_ids = obj_report.search(self.cr, self.uid, [('id','=',active_id)])
        moves = obj_report.browse(self.cr, self.uid, moves_ids)[0]
        
        total_total_price = 0.0
        for line in moves.line_ids:
            total_total_price += line.total_price
            total_price_locale = locale.format("%20.2f",line.total_price, grouping=True)
            price_locale = locale.format("%20.2f",line.price, grouping=True)
            res = {
                'is_total' : '0',
                'product' : "["+line.product_id.default_code+"] " + line.product_id.name,
                'qty' : line.quantity,
                'unit_price' : price_locale,
                'total_price' : total_price_locale,
                'expiry_date' : line.expiry_date.name,
            }
            self.result_acc.append(res)
        
        total_total_price_locale = locale.format("%20.2f",total_total_price, grouping=True)
        res = {
            'is_total' : '1',
            'product' : False,
            'qty' : False,
            'unit_price' : False,
            'total_price' : total_total_price_locale,
            'expiry_date' : False,
        }
        self.result_acc.append(res)
        
        return self.result_acc
        
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.donation')
        
        active_id = ctx.get('active_id')
        moves_ids = obj_report.search(self.cr, self.uid, [('id','=',active_id)])
        moves = obj_report.browse(self.cr, self.uid, moves_ids)[0]
        
        
        res = {
            'date' : moves.date,
            'location' : moves.location_id.name,
            'desc' : moves.name,
            'currency' : moves.currency_id.name,
            'sequence_id' : moves.sequence_id,
        }
        self.headers_acc.append(res)
        
        if _debug:
            _logger.debug('Table header : %s',res)
        
        
        return self.headers_acc
            
    

report_sxw.report_sxw('report.isf.stock.move.donation.webkit', 'isf.stock.donation', 'addons/isf_stock/report/pp_donation.mako', parser=isf_stock_move_donation_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
