<html>
<head>
    <style type="text/css">
        ${css}
table {
    border-spacing: 0px;
    page-break-inside: auto
    border-collapse: collapse; 
}

table tr {
        page-break-inside: avoid; 
        page-break-after: auto;
}

td {
    font-size: normal;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size: normal;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family: verdana;
    font-size: normal;
}

.td-data-row {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family: verdana;
    font-size: x-small;
    height: 20px;
}

.td-data-total {
    border: 2px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family: verdana;
    font-size: small;
    height: 20px;
    font-weight: bold;
}
        
@media print {
   thead {display: table-header-group;}
}


    </style>
</head>
<body>
	<%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print lines()
    """
    %>
    
    <h3>
        Stock Inventory Form
    </h3>
    
    %for o in objects:
    <center>
    <table class="table-data">
            <tr>
                <th class="td-data" style="width: 25%;">Inventory</th>
                <th class="td-data" style="width: 25%;">Location</th>
                <th class="td-data" style="width: 25%;">Date</th>
                <th class="td-data" style="width: 25%;">Currency</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 25%;text-align:center;">${o.name}</td>
                <td class="td-data" style="width: 25%;text-align:center;">${o.location_id.name}</td>
                <td class="td-data" style="width: 25%;text-align:center;">${formatLang(o.date,date_time=True)}</td>
                <td class="td-data" style="width: 25%;text-align:center;">${o.currency_id.name}</td>
            </tr>
    </table>
    </center>
    
    <br/>

    <div>
    	<table class="table-data">
	      <tr>
	        <th class="td-data" style="width: 7%;">Lot</th>
	        <th class="td-data" style="width: 10%;">Expiry Date</th>
	        <th class="td-data" style="width: 7%;">Code</th>
	        <th class="td-data" style="width: 50%;">Product</th>
	        <th class="td-data" style="width: 10%;">PUoM</th>
	        <th class="td-data" style="width: 10%;">Quantity</th>
	        <th class="td-data" style="width: 10%;">UoM</th>
	        <th class="td-data" style="width: 10%;">Unit Price</th>
	        <th class="td-data" style="width: 10%;">Total Price</th>
	      </tr>
	    %for i in xrange(20):
	    	<tr>
	          <td class="td-data-row" style="width: 7%;"></td>
	          <td class="td-data-row" style="width: 10%;"></td>
	          <td class="td-data-row" style="width: 7%;"></td>
	          <td class="td-data-row" style="width: 50%;"></td>
	          <td class="td-data-row" style="width: 10%;"></td>
	          <td class="td-data-row" style="width: 10%;"></td>
	          <td class="td-data-row" style="width: 10%;"></td>
	          <td class="td-data-row" style="width: 10%;"></td>
	          <td class="td-data-row" style="width: 10%;"></td>
	        </tr>
	    %endfor
	    </table>
	%endfor
	</div>
	    
	<br/>
</body>
</html>

