import time
from openerp.report import report_sxw

class stock_inventory_move(report_sxw.rml_parse):
    _inherit = 'stock.inventory.move'
    
    def __init__(self, cr, uid, name, context):
        super(stock_inventory_move, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
             'time': time,
             'total_price':self._price_total,
             'qty_total':self._qty_total,
        })

    
    def _price_total(self, objects):
        total = 0.0
        uom = objects[0].product_uom.name
        for obj in objects:
            total += obj.price
        return {'price':total}
        
    def _qty_total(self, objects):
        total = 0.0
        for obj in objects:
            factor = obj.product_uom.factor
            total += obj.product_qty / factor
        return {'quantity':total}


    
report_sxw.report_sxw(
    'report.isf.stock.inventory.move',
    'stock.inventory',
    'addons/isf_cims_module/report/stock_inventory_move.mako',
    parser=stock_inventory_move,
    header='internal'
)
