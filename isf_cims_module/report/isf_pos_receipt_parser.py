# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_pos_receipt_parser(report_sxw.rml_parse):
    _name = 'report.isf.pos.receipt.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_pos_receipt_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headerlines' : self.headerlines,
            'lines' : self.lines,
        })
        self.context = context
        self.result_acc = []
        self.result_header = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def headerlines(self, ids=None, done=None):
        ctx = self.context.copy()
        pos_o = self.pool.get('isf.pos')
        pos = pos_o.browse(self.cr, self.uid, ctx['active_ids'])[0]

        if pos.product_type_sel == 'service':
            op_type = pos.transaction_type_service

        if pos.product_type_sel == 'product':
            op_type = pos.transaction_type_product

        res = {
            'pos' : pos.shop_id.name,
            'type' : op_type,
            'date' : pos.date,
            'time' : datetime.datetime.now().time().strftime("%H:%M:%S"),
            'pos_id' : pos.sequence_id,
            'document_number' : pos.name,
            'description' : pos.desc or '',
        }

        self.result_header.append(res)
        
        return self.result_header

    def _get_payment_description(self, cr, uid, payment_id):
        res = {}
        account_o = self.pool.get('account.account')
        account_ids = account_o.search(cr, uid,[('code','=',payment_id)])
        for account in account_o.browse(cr, uid, account_ids):
            if account.currency_id:
                return account.currency_id.symbol
            else:
                company_o = self.pool.get('res.company')
                company_ids = company_o.search(cr, uid, [])
                for company in company_o.browse(cr, uid, company_ids):
                    return company.currency_id.symbol


    def lines(self):

        ctx = self.context.copy()
        pos_o = self.pool.get('isf.pos')
        pos = pos_o.browse(self.cr, self.uid, ctx['active_ids'])[0]
        currency = self._get_payment_description(self.cr, self.uid, pos.payment_id)

        total_price = 0;
        if pos.product_type_sel == 'service':
           for line in pos.service_ids: 
                price_locale = locale.format("%20.1f",line.unit_price, grouping=True)
                res = {
                    'is_total' : '0',
                    'product' : line.service_id.name_template,
                    'price' : price_locale,
                    'qty' : line.quantity,
                    'currency' : currency,
                }     
                total_price += (line.unit_price * line.quantity)
                self.result_acc.append(res)

        if pos.product_type_sel == 'product':
           for line in pos.service_ids: 
                price_locale = locale.format("%20.0f",line.unit_price, grouping=True)
                res = {
                    'is_total' : '0',
                    'product' : line.product_id.name_template,
                    'price' : price_locale,
                    'qty' : line.quantity,
                    'currency' : currency,
                }     
                total_price += (line.unit_price * line.quantity)
                self.result_acc.append(res)
                
        total_locale = locale.format("%20.0f",total_price, grouping=True)
        res = {
            'is_total' : '1',
            'product' : 'Grand Total',
            'price' : total_locale,
            'qty' : None,
            'currency' : currency,
        
        }  
        self.result_acc.append(res)
        
        if pos.discount_price > 0:
            discount_locale = locale.format("%20.0f",-pos.discount_price, grouping=True)
            total_discount_locale = locale.format("%20.0f",pos.total_discount, grouping=True)
            res = {
                    'is_total' : '2',
                    'product' : 'Discount',
                    'price' : discount_locale,
                    'qty' : None,
                    'currency' : currency,
                }  
            self.result_acc.append(res)
            res = {
                    'is_total' : '1',
                    'product' : 'Total',
                    'price' : total_discount_locale,
                    'qty' : None,
                    'currency' : currency,
                }  
            self.result_acc.append(res)
            
        return self.result_acc


report_sxw.report_sxw('report.isf.pos.receipt.webkit', 'isf.pos', 'addons/isf_cims_module/report/pos_receipt.mako', parser=isf_pos_receipt_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
