<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
td {
    font-size: 14px,
}

table { 
    border-collapse: collapse; 
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1><b><i>Donation Stock Receiving Notes</i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    
    
   
        
        <table style="width: 100%" border="0">
            %for head in headers():
                <tr>
                    <td><b>ID : </b></td><td>${head['sequence_id']}</td>
                </tr>
                <tr>
                    <td><b>Description</b></td><td>${head['desc']}</td>
                </tr>
                <tr>
                    <td><b>Destination</b></td><td>${head['location']}</td>
                </tr>
                <tr>
                    <td><b>Date</b></td><td> ${head['date']}</td>
                </tr>
                <tr>
                    <td><b>Currency</b></td><td>${head['currency']}</td>
                </tr>
            %endfor
        </table>
        <br/>
        <table style="width: 100%" border="0">
            <tr class="table_header" >
                <td>Product</td>
                <td>Qty</td>
                <td>Unit Price</td>
                <td>Total Price</td>
                <td>Expiry Date</td>
            </tr>
            %for line in lines():
                %if line['is_total'] == '0':
                    <tr class="row" style="background-color:#F2F2F2">
                        <td>${line['product']}</td>
                        <td>${line['qty']}</td>
                        <td class="currency">${line['unit_price']}</td>
                        <td class="currency">${line['total_price']}</td>
                        <td class="currency">${line['expiry_date']}</td>
                    </tr>
                %endif
                %if line['is_total'] == '1':
                    <tr class="row" style="background-color:#F2F2F2">
                        <td/>
                        <td/>
                        <td/>
                        <td class="currency"><b>${line['total_price']}</b></td>
                        <td/>
                    </tr>
                %endif
            %endfor
        </table>
        

</body>
</html>
