<html>
<head>
    <style type="text/css">
        ${css}
        
        .posted {
			font-size: xx-small;
			text-align: right;
			color: black;
		}
		
		.unposted {
			font-size: xx-small;
			text-align: right;
			color: blue;
		}
		
        @media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}

    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print headers()
    print lines()
    """
    %>

    <h1><b><center>Income and Expense Divided by Departments</center></b></h1>
    <br/>
    <table class="table-data">
            <tr>
                <th class="td-data" style="width: 20%;">Date Start</th>
                <th class="td-data" style="width: 20%;">Date End</th>
                <th class="td-data" style="width: 20%;">Report Type</th>
                <th class="td-data" style="width: 20%;">Target Moves</th>
                <th class="td-data" style="width: 20%;">Selection</th>
            </tr>
        %for date in datelines():
            <tr>
                <td class="td-data" style="width: 20%;text-align:center">${date['begin_date']}</td>
                <td class="td-data" style="width: 20%;text-align:center">${date['end_date']}</td>
                <td class="td-data" style="width: 20%;text-align:center">${date['report_type']}</td>
                <td class="td-data" style="width: 20%;text-align:center">${date['target']}</td>
                <td class="td-data" style="width: 20%;text-align:center">${date['selection']}</td>
            </tr>
        %endfor
    </table>

    <br/>
    <table  class="table-data">
        <tr>
            %for header in headers():
                <th class="td-data" style="width: 10%;text-align:right"">${header['analytic_account']}</th>
            %endfor
        </tr>
        
            %for account in lines():
                %if account['first_account'] == '1' and account['is_income'] == '1' and account['is_net'] != '1':
                    <tr>
                        <td class="td-data-nb" style="font-weight: bold;">Incomes</td>
                    </tr>
                %endif
                
                %if account['first_account'] == '1' and account['is_income'] == '0' and account['is_net'] != '1':
                    <tr class="row">
                        <td class="td-data-nb" style="font-weight: bold;">Expenses</td>
                    </tr>
                %endif
                
                %if account['first_account'] == '1' and account['is_income'] == '0' and account['is_net'] == '1':
                    <tr class="row">
                        <td class="td-data-nb" style="font-weight: bold;">Gross Margin</td>
                    </tr>
                %endif
                
                %if account['print_account'] == '1':
                    <tr>
                    	%if account['is_total'] == '1':
                        	<td class="td-data" style="width: 15%; font-style: italic; text-align: right;">${account['account']}</td>
                        	%if account['report_type'] == 'detailed':
	                            <td class="td-data" style="font-weight: bold; text-align:right">${account['amount']}</td>
	                        %endif
                        %else:
                        	<td class="td-data" style="width: 15%;">${account['account']}</td>
                        	%if account['report_type'] == 'detailed':
	                            <td class="td-data" style="text-align:right">${account['amount']}</td>
	                        %endif
                        %endif
                        
                %endif
                %if account['print_account'] == '0' and account['is_total'] == '0' and account['is_net'] == '0':
                    <td class="td-data" style="text-align:right">${account['amount']} </td>
                %endif
                %if account['print_account'] == '0' and account['is_total'] == '1':
                    <td class="td-data" style="text-align:right"><b>${account['amount']} </b></td>
                %endif
            %endfor
    
    </body>
</body>
</html>
