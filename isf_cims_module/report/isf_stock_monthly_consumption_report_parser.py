# -*- coding: utf-8 -*-
import time
import calendar
from pprint import pprint as pp

from openerp.osv import fields, orm
from operator import itemgetter
from openerp.report import report_sxw
from openerp import tools
import logging
import locale
from docutils.nodes import line
from reportlab.lib.normalDate import _DateSeqTypes

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class mcr_view(orm.Model):
    _name = "mcr.view"
    _description = "Stock Monthly Consumption View"
    _auto = False
    _columns = {
        'id' : fields.char('id', size=16, required=False, readonly=True),
        'year' : fields.char('id', size=16, required=False, readonly=True),
        'month' : fields.char('type', size=16, required=False, readonly=True),
        'category' : fields.char('type', size=16, required=False, readonly=True),
        'tag' : fields.char('type', size=16, required=False, readonly=True),
        'product' : fields.char('account', size=16, required=False, readonly=True),
        'destination' : fields.char('account', size=16, required=False, readonly=True),
        'qty' : fields.char('amount', size=16, required=False, readonly=True),
        'value' : fields.char('analytic_account', size=16, required=False, readonly=True),
    }

class isf_stock_monthly_consumption_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.monthly.consumption.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_stock_monthly_consumption_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'report_header' : self.report_header,
            'headers' : self.headers,
            'lines' : self.lines,
            'top_lines' : self.top_lines,
            'totals' : self.totals,
        })
        
        self.context = context
        self.result_report_header = []
        self.result_headers = []
        self.result_lines = []
        self.result_top_lines = []
        self.result_totals = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    def _update_view(self, cr, uid, params):
        
        if _debug:
            _logger.debug('==> create or replace view mcr_view')
        
        if _debug:
            _logger.debug('==> date_start : %s', params['date_start'])
            _logger.debug('==> date_end : %s', params['date_end'])
            _logger.debug('==> location_id : %s', params['location_id'])
        
        tools.drop_view_if_exists(cr, 'mcr_view')
        cr.execute("""
            create or replace view mcr_view as (
            
            select row_number() over () as id, *
            from (
            
            SELECT EXTRACT(YEAR FROM a.date_expected) as year, 
                    EXTRACT(MONTH FROM a.date_expected) AS month, 
                    g.name as category,
                    h.name as tag,
                    concat(b.default_code, ' - ', b.name_template) AS product,
                    d.name AS destination,
                    sum(a.product_qty) as qty, 
                    sum(a.product_qty / e.factor * c.cost_price) AS value
                FROM stock_move a
                JOIN product_product b on a.product_id = b.id
                JOIN stock_production_lot c on a.prodlot_id = c.id
                JOIN stock_location d on a.location_dest_id = d.id
                JOIN product_uom e on a.product_uos = e.id
                JOIN product_template f on b.product_tmpl_id = f.id
                JOIN product_category g on f.categ_id = g.id
                LEFT JOIN (
                    SELECT a.product_id, string_agg(b.name, ', ' ORDER BY b.name) as name 
                    FROM product_product_tag_rel a
                    LEFT JOIN product_tag b ON a.tag_id = b.id
                    GROUP BY a.product_id) as h on b.id = h.product_id
                WHERE a.state = 'done'
                and (a.date_expected >= '""" + params['date_start'] + """' and a.date_expected <= '""" + params['date_end'] + """')
                and a.location_id = """ + str(params['location_id']) + """
                and d.usage = 'internal'
                
                GROUP BY EXTRACT(YEAR FROM a.date_expected), EXTRACT(MONTH FROM a.date_expected), g.name, h.name, d.name, b.id
                
                ) as rows
        )""")
    
    def report_header(self, ids=None, done=None):
        ctx = self.context.copy()
        isf_lib = self.pool.get('isf.lib.utils')
        obj_report = self.pool.get('isf.stock.monthly.consumption.report')
        date_start_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_end_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        location_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        group_criteria_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['group_criteria'])
        show_data_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['show_data'])
        date_start = date_start_obj[0]['date_start']
        date_end = date_end_obj[0]['date_end']
        location = location_obj[0]['location_id']
        report_type = type_obj[0]['report_type']
        group_criteria = group_criteria_obj[0]['group_criteria']
        show_data = show_data_obj[0]['show_data']
        currency = isf_lib.get_company_currency(self.cr, self.uid)

        res = { 
            'date_start' : date_start,
            'date_end' : date_end,
            'location' : location[1],
            'report_type' : report_type,
            'group_criteria' : group_criteria,
            'show_data' : show_data,
            'currency' : currency.name,
        }
        self.result_report_header.append(res)
        
        if _debug:
            _logger.debug('==> result_report_header : %s', self.result_report_header)
        
        params = {}
        params['date_start'] = date_start
        params['date_end'] = date_end
        params['location_id'] = location[0]
        
        self._update_view(self.cr, self.uid, params)
            
        return self.result_report_header
    
    def set_and_sort(self, seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.monthly.consumption.report')
        date_start_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_end_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        location_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        date_start = date_start_obj[0]['date_start']
        date_end = date_end_obj[0]['date_end']
        location = location_obj[0]['location_id']
        report_type = type_obj[0]['report_type']
        
        self.result_headers = []
        params = {}
        params['date_start'] = date_start
        params['date_end'] = date_end
        params['location_id'] = location[0]
        
        self._update_view(self.cr, self.uid, params)
        obj_mcr_report = self.pool.get('mcr.view')
        
        if report_type == 'department':
            department_names = obj_mcr_report.search(self.cr, self.uid, [], order='destination')
            department_names = self.set_and_sort([d['destination'] for d in obj_mcr_report.browse(self.cr, self.uid, department_names)])
            for name in department_names:
                res = { 
                    'name' : name,
                }
                self.result_headers.append(res)
                
        elif report_type == 'month':
            years_months = obj_mcr_report.search(self.cr, self.uid, [], order='year, month')
            years_months = self.set_and_sort([str(int(d['year']))+'-'+str(int(d['month'])).zfill(2) for d in obj_mcr_report.browse(self.cr, self.uid, years_months)])

            months = obj_mcr_report.search(self.cr, self.uid, [], order='month')
            months = self.set_and_sort([d['month'] for d in obj_mcr_report.browse(self.cr, self.uid, months)])
            for name in years_months:
                res = { 
                    'name' : name, #calendar.month_name[int(name)],
                }
                self.result_headers.append(res)
                
        if _debug:
            _logger.debug('==> headers : %s', self.result_headers)
        
        return self.result_headers
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.monthly.consumption.report')
        type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        group_criteria_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['group_criteria'])
        report_type = type_obj[0]['report_type']
        group_criteria = group_criteria_obj[0]['group_criteria']
        
        obj_mcr_report = self.pool.get('mcr.view')
        if group_criteria == 'product':
            lines_ids = obj_mcr_report.search(self.cr, self.uid, [], order='product')
        elif group_criteria == 'category':
            lines_ids = obj_mcr_report.search(self.cr, self.uid, [], order='category')
        elif group_criteria == 'tag':
            lines_ids = obj_mcr_report.search(self.cr, self.uid, [], order='tag')
        
        group_by = []
        res_total = self._get_line_object('Total')
        for line in obj_mcr_report.browse(self.cr, self.uid, lines_ids):
            
            if group_criteria == 'product':
                if line.product not in group_by:
                    res = self._get_line_object(line.product)
                else:
                    res = [d for d in self.result_lines if d['criteria'] == line.product][0]
            elif group_criteria == 'category':
                if line.category not in group_by:
                    res = self._get_line_object(line.category)
                else:
                    res = [d for d in self.result_lines if d['criteria'] == line.category][0]
            elif group_criteria == 'tag':
                if not line.tag:
                    line.tag = '<Undefined>'
                if line.tag not in group_by:
                    res = self._get_line_object(line.tag)
                else:
                    res = [d for d in self.result_lines if d['criteria'] == line.tag][0]
            
            if report_type == 'department':
                res[line.destination + '_qty'] += line.qty
                res[line.destination + '_value'] += line.value
                res_total[line.destination + '_value'] += line.value
            elif report_type == 'month':
                index = str(int(line.year))+'-'+str(int(line.month)).zfill(2)
                res[index + '_qty'] += line.qty
                res[index + '_value'] += line.value
                res_total[index + '_value'] += line.value
            res['total_qty'] += line.qty
            res['total_value'] += line.value
            res['average_qty'] = res['total_qty'] / len(self.result_headers)
            res['average_value'] = res['total_value'] / len(self.result_headers)
            res_total['total_value'] += line.value
            
            if group_criteria == 'product':
                if line.product not in group_by:
                    group_by.append(line.product)
                    self.result_lines.append(res)
            elif group_criteria == 'category':
                if line.category not in group_by:
                    group_by.append(line.category)
                    self.result_lines.append(res)
            elif group_criteria == 'tag':
                if line.tag not in group_by:
                    group_by.append(line.tag)
                    self.result_lines.append(res)
                
        if _debug:
            _logger.debug('==> result_lines : %s', self.result_lines)
        
        # format locale
        for result in self.result_lines:
            for group_criteria in self.result_headers:
                qty = result[group_criteria['name'] + '_qty']
                result[group_criteria['name'] + '_qty'] = locale.format("%20.0f",qty, grouping=True)
                value = result[group_criteria['name'] + '_value']
                result[group_criteria['name'] + '_value'] = locale.format("%20.0f",value, grouping=True)
            result['total_qty'] = locale.format("%20.0f",result['total_qty'], grouping=True)
            result['total_value'] = locale.format("%20.0f",result['total_value'], grouping=True)
            result['average_qty'] = locale.format("%20.0f",result['average_qty'], grouping=True)
            result['average_value'] = locale.format("%20.0f",result['average_value'], grouping=True)
            
        # add totals (values only)
        for group_criteria in self.result_headers:
            value = res_total[group_criteria['name'] + '_value']
            res_total[group_criteria['name'] + '_value'] = locale.format("%20.0f",value, grouping=True)
        if len(self.result_lines) > 0:
            res_total.update({
                    'total_value' : locale.format("%20.0f",res_total['total_value'], grouping=True),
                    'average_value' : locale.format("%20.0f",res_total['total_value'] / len(self.result_headers), grouping=True)})
        self.result_totals.append(res_total)
            
        return self.result_lines
    
    def top_lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.monthly.consumption.report')
        show_data_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['show_data'])
        show_data = show_data_obj[0]['show_data']
        
        if show_data == 'qty':
            self.result_top_lines = sorted(self.result_lines, key=itemgetter('total_qty'), reverse=True)[:10]
        elif show_data == 'value':
            self.result_top_lines = sorted(self.result_lines, key=itemgetter('total_value'), reverse=True)[:10]
        
        if _debug:
            _logger.debug('==> result_top_lines : %s', self.result_top_lines)
            
        return self.result_top_lines
    
    def totals(self, ids=None, done=None):
        
        return self.result_totals
    
    def _get_line_object(self, criteria, ids=None, context=None):
        
        res = {'criteria' : criteria,
               'total_qty' : 0.0,
               'total_value' : 0.0,
               'average_qty' : 0.0,
               'average_value' : 0.0}
        for header in self.result_headers:
            res[header['name'] + '_qty'] = 0.0
            res[header['name'] + '_value'] = 0.0
            
        return res
            
        
report_sxw.report_sxw('report.isf.stock.monthly.consumption.webkit', 'isf.stock.monthly.consumption.report', 'addons/isf_cims_module/report/isf_stock_monthly_consumption_report.mako', parser=isf_stock_monthly_consumption_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
