<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}

.header{
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}

.title {
    border :0;
    height: 1px;
    border-bottom: 1px;
}

    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print lines()
    """
    %>
    
        <center>
            <h3 class="th_data">List Report </h3>
        </center>
    
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 10%;">Code</th>
                <th class="td-data" style="width: 60%;">Product</th>
                <th class="td-data" style="width: 20%;">Sell Price</th>
            </tr>
            %for data in lines():
                %if data['print_header'] == '1':
                    <tr><td colspan="3"/></tr>
                    <tr>
                        <th class="td-data" colspan="3" style="text-align: left;">${data['store']} [${data['category']}] - ${data['type']} </th>
                    </tr>
                %endif
                <tr>
                    <td class="td-data" style="width: 10%;text-align: right;">${data['code']}</td>
                    <td class="td-data" style="width: 60%;">${data['name']}</td>
                    <td class="td-data" style="width: 20%;text-align: right;">${data['sell_price']} ${data['currency']}</td>
                </tr>
            %endfor
        </table>
</body>
</html>
