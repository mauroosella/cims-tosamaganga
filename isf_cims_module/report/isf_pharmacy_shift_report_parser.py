# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_pharmacy_shift_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.pharmacy.shift.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_pharmacy_shift_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'header': self.header,
        })
        self.context = context
        self.result_acc = []
        self.header_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_product_report_object(self, ids):
        pr_obj = self.pool.get('isf.pharmacy.shift.report')
        
        pr = bs_obj.browse(self.cr, self.uid, ids)
        
        return pr
    
    def header(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.pharmacy.shift.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_s_obj[0]['date']
        
        res = {
            'date' : date,
        }
        self.header_acc.append(res)
        
        return self.header_acc  
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.pharmacy.shift.report')
        pos_obj = self.pool.get('isf.pharmacy.pos')
        
        date_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        shift_a_obj_s = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shift_a_start'])
        shift_a_obj_e = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shift_a_end'])
        shift_b_obj_s = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shift_b_start'])
        shift_b_obj_e = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shift_b_end'])
        shift_c_obj_s = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shift_c_start'])
        shift_c_obj_e = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shift_c_end'])
        
        
        date = date_obj[0]['date']
        shift_a_start = shift_a_obj_s[0]['shift_a_start']
        shift_b_start = shift_b_obj_s[0]['shift_b_start']
        shift_c_start = shift_c_obj_s[0]['shift_c_start']
        shift_a_end = shift_a_obj_e[0]['shift_a_end']
        shift_b_end = shift_b_obj_e[0]['shift_b_end']
        shift_c_end = shift_c_obj_e[0]['shift_c_end']
        
        # SHIFT A
        total_shift_a = 0.0
        pos_ids = pos_obj.search(self.cr, self.uid, [('name','>=',shift_a_start),('name','<=',shift_a_end)])
        for pos in pos_obj.browse(self.cr, self.uid, pos_ids):
            total_shift_a += pos.total_amount
            
        # SHIFT B
        total_shift_b = 0.0
        pos_ids = pos_obj.search(self.cr, self.uid, [('name','>=',shift_b_start),('name','<=',shift_b_end)])
        for pos in pos_obj.browse(self.cr, self.uid, pos_ids):
            total_shift_b += pos.total_amount
            
        # SHIFT C
        total_shift_c = 0.0
        pos_ids = pos_obj.search(self.cr, self.uid, [('name','>=',shift_c_start),('name','<=',shift_c_end)])
        for pos in pos_obj.browse(self.cr, self.uid, pos_ids):
            total_shift_c += pos.total_amount
        
        res = {
            'total_shift_a' : total_shift_a,
            'total_shift_b' : total_shift_b,
            'total_shift_c' : total_shift_c,
        }
            
        self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.pharmacy.shift.webkit', 'isf.pharmacy.shift.report', 'addons/isf_cims_module/report/shift.mako', parser=isf_pharmacy_shift_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
