<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <center>
        <h3>Stock used for internal purpose</h3>
    </center>
    %for data in headers():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Location</th>
                <th class="td-data" style="width: 30%;">Date Start</th>
                <th class="td-data" style="width: 30%;">Date Stop</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 40%;">${data['location']}</th>
                <td class="td-data" style="width: 30%;">${data['date_start']}</td>
                <td class="td-data" style="width: 30%;">${data['date_stop']}</td>
            </tr>
        </table>
    </center>
    %endfor
    <br/>
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 8%;">Date</th>
                <th class="td-data" style="width: 8%;">Stock</th>
                <th class="td-data" style="width: 8%;">TR#</th>
                <th class="td-data" style="width: 30%;">Product</th>
                <th class="td-data" style="width: 6%;">Qty</th>
                <th class="td-data" style="width: 10%;">Unit Price</th>
                <th class="td-data" style="width: 10%;">Total Price</th>
                <th class="td-data" style="width: 20%;">Expiry Date</th>
            </tr>
            %for data in lines():
                %if data['is_total'] == '0':
                <tr>
                    <td class="td-data" style="width: 8%;">${data['date']}</th>
                    <td class="td-data" style="width: 8%;">${data['stock']}</th>
                    <td class="td-data" style="width: 8%;">${data['tr_id']}</th>
                    <td class="td-data" style="width: 30%;">${data['product']}</td>
                    <td class="td-data" style="width: 6%;text-align:right;">${data['qty']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['unit_price']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['total_price']}</td>
                    <td class="td-data" style="width: 20%;text-align:right;">${data['expiry_date']}</td>
                </tr>
                %endif
                %if data['is_total'] == '1':
                    <td class="td-data-nb" style="width: 62%;" colspan="5"/>
                    <td class="td-data-nb" style="width: 10%;text-align:right;" ><b>Total :</b></td>
                    <td class="td-data-nb" style="width: 10%;text-align:right;"><b>${data['total_amount']}</b></td>
                    <td class="td-data-nb" style="width: 20%;text-align:right;" />
                %endif
            %endfor
        </table>
    </center>
    
    <br/>
</body>
</html>
