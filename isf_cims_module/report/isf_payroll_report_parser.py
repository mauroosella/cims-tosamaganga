# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import platform

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug = False

class isf_payroll_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.payroll.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_payroll_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'header' : self.header,
        })
        self.context = context
        self.result_payroll = []
        self.result_header = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_aai_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    def header(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.payroll.payslip')
        obj_payslip = self.pool.get('hr.payslip.run')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['payslip_batch_id'])
        grade_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['grade_ids'])
        payslip_id = report_data[0]['payslip_batch_id']
        grade = grade_data[0]['grade_ids']
        
        if _debug:
            _logger.debug('Grade : %s', grade)
        
        if grade == 'all':
            grade_str = 'All'
        else:
            grade_id = int(grade)
            grade_obj = self.pool.get('hr.contract.type')
            grade_ids = grade_obj.search(self.cr, self.uid, [('id','=',grade_id)])
            grade_str = grade_obj.browse(self.cr, self.uid, grade_ids)[0].name
        
        payslip_ids = obj_payslip.search(self.cr, self.uid, [('id','=',payslip_id[0])])
        payslip = obj_payslip.browse(self.cr, self.uid, payslip_ids)[0]
        
        
        res = {
            'date_start' : payslip.date_start,
            'date_end' : payslip.date_end,
            'name' : payslip.name,
            'grade' : grade_str,
        }
        
        self.result_header.append(res)
            
        return self.result_header

    def lines(self, ids=None, done=None):
        obj_payslip = self.pool.get('hr.payslip')
        obj_report = self.pool.get('isf.payroll.payslip')
        obj_payslip_line = self.pool.get('hr.payslip.line')
        
        ctx = self.context.copy()
        
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['payslip_batch_id'])
        grade_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['grade_ids'])
        payslip_id = report_data[0]['payslip_batch_id']
        grade = grade_data[0]['grade_ids']
        
        grade_id = -1
        if grade != 'all':
            grade_id = int(grade)
    
        if _debug:
            _logger.debug('Grade : %s', grade)
        
        value = {}
        total = {}
        if grade_id > 0:
            obj_payslip_ids = obj_payslip.search(self.cr, self.uid, [('payslip_run_id','=',payslip_id[0]),('contract_id.type_id.id','=',grade_id)],order="name")
        else:
            obj_payslip_ids = obj_payslip.search(self.cr, self.uid, [('payslip_run_id','=',payslip_id[0])],order="name")
        for payslip in obj_payslip.browse(self.cr, self.uid, obj_payslip_ids):
            obj_payslip_line_ids = obj_payslip_line.search(self.cr, self.uid, [('slip_id','=', payslip.id)])
            for line in obj_payslip_line.browse(self.cr, self.uid, obj_payslip_line_ids):
                value[line.code] = locale.format("%20.2f",line.amount, grouping=True)
                
                if not line.code in total:
                    total[line.code] = 0.0
                total[line.code] += line.amount
                
                
            contract_type = ''
            if payslip.contract_id.type_id is not None:
                contract_type = payslip.contract_id.type_id.name
            res = {
                'is_total' : '0',
                'number' : payslip.number,
                'grade' : contract_type,
                'employee' : payslip.employee_id.name,
                'gross' : value['GROSS'],
                'income_tax' : value['INT'],
                's_duty' : value['SDY'],
                'hg': value['H/G'],
                'kguri' : value['KGR'],
                'net' : value['NET'],
                'adv' : value.get('ADV') or 0,
                }
                
            self.result_payroll.append(res)
            self.result_payroll = sorted(self.result_payroll, key=lambda k: k['grade'])

        res = {
            'is_total' : '1',
            'number' : False,
            'grade' : False,
            'employee' : False,
            'gross' : locale.format("%20d",total['GROSS'], grouping=True),
            'income_tax' : locale.format("%20d",total['INT'], grouping=True),
            's_duty' : locale.format("%20d",total['SDY'], grouping=True),
            'hg': locale.format("%20d",total['H/G'], grouping=True),
            'kguri' : locale.format("%20d",total['KGR'], grouping=True),
            'net' : locale.format("%20d",total['NET'], grouping=True),
            'adv' : locale.format("%20d",total.get('ADV') or 0, grouping=True),
        }

        self.result_payroll.append(res)
            
        return self.result_payroll

report_sxw.report_sxw('report.isf.payroll.webkit', 'isf.payroll.payslip', 'addons/isf_cims_module/report/payroll.mako', parser=isf_payroll_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
