# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_expired_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.expired.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_stock_expired_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        isf_lib = self.pool.get('isf.lib.utils')
        obj_report = self.pool.get('isf.stock.expired.wizard')
        loc_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        date_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_data[0]['date']
        location_id = loc_data[0]['location_id']
        currency = isf_lib.get_company_currency(self.cr, self.uid)
        
        if _debug:
            _logger.debug('Date : %s',date)
            _logger.debug('LOcation_id : %s',location_id)
        
        
        res = {
            'date' : date,
            'location' : location_id[1],
            'currency' : currency.name,
        }
        self.headers_acc.append(res)
        
        
        return self.headers_acc
    
    def _get_cost_price(self, lot, product,cost_percentage):
        if lot['cost_price'] != product.list_price:
            cost_price = lot['cost_price']
        elif product.standard_price != product.list_price and product.standard_price > 0:
            cost_price = product.standard_price
        else:
            cost_price = product.list_price * (cost_percentage/100)
        
        return cost_price
    
    def _get_percentage_cost_price(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','percentage_cost_price')
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.expired.wizard')
        loc_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        date_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_data[0]['date']
        location_id = loc_data[0]['location_id'][0]
        cost_perc = self._get_percentage_cost_price(self.cr, self.uid)
        ctx.update({'location_id' : location_id})
        stock_o = self.pool.get('stock.production.lot')
        stock_ids = stock_o.search(self.cr, self.uid, [('life_date','<=',date)],context=ctx)
        
        total_cost = 0
        for stock in stock_o.browse(self.cr, self.uid, stock_ids, context=ctx):
            if round(stock.stock_available) > 0:
                if _debug:
                    _logger.debug('==> stock_available : %s -> %s',stock.stock_available, round(stock.stock_available))
                cost_price = self._get_cost_price(stock, stock.product_id, cost_perc)
                total_cost += (cost_price * stock.stock_available)
                res = {
                    'is_total' : '0',
                    'stock' : stock.name,
                    'product' : stock.product_id.name_template,
                    'expiry_date' : str(stock.life_date)[:10],
                    'qty' : round(stock.stock_available),
                    'cost_price' : locale.format("%20d",cost_price, grouping=True),
                    'total_cost' : locale.format("%20d",cost_price * stock.stock_available, grouping=True),
                }
                self.result_acc.append(res)
        
        res = {
            'is_total' : '1',
            'total_cost' : locale.format("%20d",total_cost, grouping=True),
        }
        self.result_acc.append(res)
        return self.result_acc
    

report_sxw.report_sxw('report.isf.stock.expired.webkit', 'isf.stock.expired.wizard', 'addons/isf_cims_module/report/stock_expired.mako', parser=isf_stock_expired_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
