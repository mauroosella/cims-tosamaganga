# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.osv import fields, orm

from openerp.report import report_sxw
from openerp import tools
import logging
import locale
import platform

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug = True

class aie_view(orm.Model):
    _name = "aie.view"
    _description = "Analytic Income and Expenses Report View"
    _auto = False
    _columns = {
        'id' : fields.char('id', size=16, required=False, readonly=True),
        'type' : fields.char('type', size=16, required=False, readonly=True),
        'account_id' : fields.char('account', size=16, required=False, readonly=True),
        'account' : fields.char('account', size=16, required=False, readonly=True),
        'amount' : fields.char('amount', size=16, required=False, readonly=True),
        'analytic_account_id' : fields.char('analytic_account', size=16, required=False, readonly=True),
        'analytic_account' : fields.char('analytic_account', size=16, required=False, readonly=True),
    }

class isf_iae_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.iae.webkit'
    
    def _update_view(self, cr, uid, params):
        
        if _debug:
            _logger.debug('==> create or replace view aie_view')
        
        analytic_placeholder = ', '.join(map(str, params['analytic_ids']))
        income_placeholder = ', '.join(map(str, params['income_account_ids']))
        expense_placeholder = ', '.join(map(str, params['expense_account_ids']))
        
        if _debug:
            _logger.debug('==> analytic_placeholder : %s', analytic_placeholder)
            _logger.debug('==> income_placeholder : %s', income_placeholder)
            _logger.debug('==> expense_placeholder : %s', expense_placeholder)
        
        tools.drop_view_if_exists(cr, 'aie_view')
        cr.execute("""
            create or replace view aie_view as (
            
            select row_number() over () as id, *
            from (
            
                select 'income' as type, c.id as account_id, concat_ws(' - ',c.code,c.name) as account, sum(a.debit) - sum (a.credit) as amount, d.id as analytic_account_id, concat_ws(' - ',d.code,d.name) as analytic_account 
                from account_move_line a
                join account_move b on a.move_id = b.id
                join account_account c on a.account_id = c.id
                join account_account_type e on c.user_type = e.id
                join account_analytic_account d on a.analytic_account_id = d.id
                where (a.date >= '""" + params['begin_date'] + """' and a.date <= '""" + params['end_date'] + """')
                and d.id in (""" + analytic_placeholder + """)
                and (c.id in (""" + income_placeholder + """) or c.id in (""" + expense_placeholder + """))
                and e.code = 'income'
                
                group by c.id, d.id
                
                union
                
                select 'expense' as type, c.id as account_id, concat_ws(' - ',c.code,c.name) as account, sum(a.debit) - sum (a.credit) as amount, d.id as analytic_account_id, concat_ws(' - ',d.code,d.name) as analytic_account
                from account_move_line a
                join account_move b on a.move_id = b.id
                join account_account c on a.account_id = c.id
                join account_account_type e on c.user_type = e.id
                join account_analytic_account d on a.analytic_account_id = d.id
                where (a.date >= '""" + params['begin_date'] + """' and a.date <= '""" + params['end_date'] + """')
                and d.id in (""" + analytic_placeholder + """)
                and (c.id in (""" + income_placeholder + """) or c.id in (""" + expense_placeholder + """))
                and e.code = 'expense'
                
                group by c.id, d.id
                
                order by type, account, analytic_account
                ) as rows
        )""")

    def __init__(self, cr, uid, name, context=None):
        super(isf_iae_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'datelines' : self.datelines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.result_date = []
        self.header_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_iae_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    def datelines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.iae.analytic')
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        report_type_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        report_target = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['target'])
        report_selection = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['selection'])
        
        begin_date = report_date_begin[0]['begin_date']
        end_date = report_date_end[0]['end_date']
        report_type = report_type_data[0]['report_type']
        target = report_target[0]['target']
        selection = report_selection[0]['selection']
        
        report_type_title = 'Detailed'
        if report_type == 'summary':
            report_type_title = 'Summary'
        

        res = {
            'begin_date' : begin_date,
            'end_date' : end_date,
            'report_type' : report_type_title,
            'target' : target,
            'selection' : selection,
        }
        
        self.result_date.append(res)
        
        return self.result_date
    
    def headers(self):

        obj_analytic = self.pool.get('account.analytic.account')
        obj_account = self.pool.get('account.account')
        obj_report = self.pool.get('isf.iae.analytic')
        ctx = self.context.copy()
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        report_analytic_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['analytic_account_ids'])
        report_income_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['income_account_ids'])
        report_expense_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['expense_account_ids'])
        report_type_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        report_target = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['target'])
        report_no_filter = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['no_filter'])
        report_selection = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['selection'])
        
        begin_date = report_date_begin[0]['begin_date']
        end_date = report_date_end[0]['end_date']
        report_type = report_type_data[0]['report_type']
        target = report_target[0]['target']
        no_filter = report_no_filter[0]['no_filter']
        selection = report_selection[0]['selection']
        
        if no_filter:
            analytic_ids = obj_analytic.search(self.cr, self.uid, [('type','=','normal')],order='code')
            income_account_ids = obj_account.search(self.cr, self.uid, [('user_type.name','=','Income')],order='code')
            expense_account_ids = obj_account.search(self.cr, self.uid, [('user_type.name','=','Expense')],order='code')
        else:
            analytic_ids = report_analytic_account[0]['analytic_account_ids']
            income_account_ids = report_income_account[0]['income_account_ids']
            expense_account_ids = report_expense_account[0]['expense_account_ids']
        
        params = {}
        params['begin_date'] = begin_date
        params['end_date'] = end_date
        if target == 'all':
            params['target'] = '%'
        else:
            params['target'] = 'posted'
        params['analytic_ids'] = analytic_ids
        params['income_account_ids'] = income_account_ids
        params['expense_account_ids'] = expense_account_ids
        self._update_view(self.cr, self.uid, params)
        obj_aie_report = self.pool.get('aie.view')
        
        if selection == 'with_movements':
            analytic_ids = obj_aie_report.search(self.cr, self.uid, [], order='analytic_account')
            analytic_ids = self.set_and_sort([d['analytic_account_id'] for d in obj_aie_report.browse(self.cr, self.uid, analytic_ids)])
            
        if _debug:
            _logger.debug('==> analytic_ids (headers) : %s', analytic_ids)
        
        res = {
                'analytic_account' : 'Account',
            }
        self.header_acc.append(res)
        
        if report_type == 'detailed':
            for analytic in obj_analytic.browse(self.cr, self.uid, analytic_ids):
                res = {
                    'analytic_account' : analytic.code + ' ' + analytic.name,
                }
                
                if _debug:
                    _logger.debug('==> Header : %s', analytic.name)
            
                self.header_acc.append(res)
            
        res = {
                'analytic_account' : 'Totals',
            }
        self.header_acc.append(res)
            
        return self.header_acc
    
    def set_and_sort(self, seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]

    def lines(self, ids=None, done=None):
        obj_analytic = self.pool.get('account.analytic.account')
        obj_move_line = self.pool.get('account.move.line')
        obj_move = self.pool.get('account.move')
        obj_account = self.pool.get('account.account')
        obj_report = self.pool.get('isf.iae.analytic')
        ctx = self.context.copy()
        report_analytic_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['analytic_account_ids'])
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        report_income_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['income_account_ids'])
        report_expense_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['expense_account_ids'])
        report_cash_bank_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['cash_bank_account_ids'])
        report_type_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        report_target = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['target'])
        report_selection = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['selection'])
        report_no_filter = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['no_filter'])
        
        begin_date = report_date_begin[0]['begin_date']
        end_date = report_date_end[0]['end_date']
        report_type = report_type_data[0]['report_type']
        target = report_target[0]['target']
        selection = report_selection[0]['selection']
        no_filter = report_no_filter[0]['no_filter']
        
        if no_filter:
            analytic_ids = obj_analytic.search(self.cr, self.uid, [('type','=','normal')],order='code')
            income_account_ids = obj_account.search(self.cr, self.uid, [('user_type.name','=','Income')],order='code')
            expense_account_ids = obj_account.search(self.cr, self.uid, [('user_type.name','=','Expense')],order='code')
        else:
            analytic_ids = report_analytic_account[0]['analytic_account_ids']
            income_account_ids = report_income_account[0]['income_account_ids']
            expense_account_ids = report_expense_account[0]['expense_account_ids']
        
        params = {}
        params['begin_date'] = begin_date
        params['end_date'] = end_date
        if target == 'all':
            params['target'] = '%'
        else:
            params['target'] = 'posted'
        params['analytic_ids'] = analytic_ids
        params['income_account_ids'] = income_account_ids
        params['expense_account_ids'] = expense_account_ids
        self._update_view(self.cr, self.uid, params)
        obj_aie_report = self.pool.get('aie.view')
        
        if selection == 'with_movements':
            analytic_ids = obj_aie_report.search(self.cr, self.uid, [], order='analytic_account')
            analytic_ids = self.set_and_sort([d['analytic_account_id'] for d in obj_aie_report.browse(self.cr, self.uid, analytic_ids)])
            report_income_ids = obj_aie_report.search(self.cr, self.uid, [('type','=','income')], order='account')
            income_account_ids = self.set_and_sort([d['account_id'] for d in obj_aie_report.browse(self.cr, self.uid, report_income_ids)])
            report_expense_ids = obj_aie_report.search(self.cr, self.uid, [('type','=','expense')], order='account')
            expense_account_ids = self.set_and_sort([d['account_id'] for d in obj_aie_report.browse(self.cr, self.uid, report_expense_ids)])
        
        if _debug:
            _logger.debug('==> begin_date : %s', begin_date)
            _logger.debug('==> end_date : %s', end_date)
            _logger.debug('==> report_type : %s', report_type)
            _logger.debug('==> target : %s', target)
            _logger.debug('==> selection : %s', selection)
            _logger.debug('==> no_filter : %s', no_filter)
            if not no_filter:
                _logger.debug('==> analytic_ids : %s', analytic_ids)
                _logger.debug('==> income_account_ids : %s', income_account_ids)
                _logger.debug('==> expense_account_ids : %s', expense_account_ids)
        
        #
        # INCOME ACCOUNTS
        #   
        total_per_column_income = {}
        first_account = '1'
        for account in obj_account.browse(self.cr, self.uid, income_account_ids):
            
            print_account = '1'
            total_account_amount = 0.0
            line = []
            addLine = False
            aIndex = 0
            
            for analytic in obj_analytic.browse(self.cr, self.uid, analytic_ids):
            
                report_income_id = obj_aie_report.search(self.cr, self.uid, [('type','=','income'),('account_id','=',account.id),('analytic_account_id','=',analytic.id)])
                report_account = obj_aie_report.browse(self.cr, self.uid, report_income_id)
                
                if report_account:
                    
                    res = {
                        'first_account' : first_account,
                        'is_income' : '1',
                        'is_total' : '0',
                        'is_net' : '0',
                        'print_account' : print_account,
                        'account' : report_account[0].account,
                        'analytic_account' : analytic.code + ' - ' + analytic.name,
                        'report_type' : report_type,
                    }
                    print_account = '0'
                    first_account = '0'
                    account_amount = abs(report_account[0].amount)
                    
                    if account_amount != 0.0 or selection == 'all':
                        addLine = True
                     
                    account_amount_locale = locale.format("%20.0f",account_amount, grouping=True)
                    res.update({
                        'amount' : account_amount_locale,
                    })
                    
                    total_account_amount = total_account_amount + account_amount
                    
                    if aIndex not in total_per_column_income:
                        total_per_column_income[aIndex] = 0.0    
                    total_per_column_income[aIndex] = total_per_column_income[aIndex] + account_amount 
                    aIndex = aIndex + 1
                    
                else:
                    
                    res = {
                        'first_account' : first_account,
                        'is_income' : '1',
                        'is_total' : '0',
                        'is_net' : '0',
                        'print_account' : print_account,
                        'account' : account.code + ' - ' + account.name,
                        'analytic_account' : analytic.code + ' - ' + analytic.name,
                        'report_type' : report_type,
                        'amount' : locale.format("%20.0f",0.0, grouping=True),
                    }
                    
                    if selection == 'all':
                        addLine = True
                    
                    print_account = '0'
                    first_account = '0'
                    
                    if aIndex not in total_per_column_income:
                        total_per_column_income[aIndex] = 0.0    
                    aIndex = aIndex + 1
                    
                line.append(res)
                
            if addLine and line:
                if report_type == 'detailed':
                    for res in line:    
                        self.result_acc.append(res)
                elif report_type == 'summary':
                    self.result_acc.append(line[0])

            
                # Total for account
                total_account_amount_locale = locale.format("%20.0f",total_account_amount, grouping=True)
                res = {
                    'first_account' : '0',
                    'is_income' : '0',
                    'is_total' : '1',
                    'is_net' : '0',
                    'print_account' : '0',
                    'amount' : total_account_amount_locale,
                    'report_type' : report_type,
                }
            
                self.result_acc.append(res)
            elif line:
                if line[0]['first_account'] == '1':
                    first_account = '1'
                    
        #
        # TOTALS PER COLUMN
        #
        print_account = '1'
        total_of_totals_income = 0
        total_line = []
        for k in total_per_column_income:
            total_account_amount_locale = locale.format("%20.0f",total_per_column_income[k], grouping=True)
            total_of_totals_income += total_per_column_income[k]
            #if _debug:
            #    _logger.debug('==> K : %s , V : %f',k, total_per_column[k])
            
            res = {
                'first_account' : '0',
                'is_income' : '0',
                'is_total' : '1',
                'is_net' : '0',
                'print_account' : print_account,
                'account' : 'TOTALS',
                'amount' : total_account_amount_locale,
                'report_type' : report_type,
            }
            print_account = '0'
            total_line.append(res)

        if report_type == 'detailed':
            for res in total_line:
                self.result_acc.append(res)
                
        total_of_totals_locale = locale.format("%20.0f",total_of_totals_income, grouping=True)
        res = {
            'first_account' : '0',
            'is_income' : '0',
            'is_total' : '1',
            'is_net' : '0',
            'print_account' : print_account,
            'account' : 'TOTALS',
            'amount' : total_of_totals_locale,
            'report_type' : report_type,
        }
            
        self.result_acc.append(res)
        
        #
        # EXPENSE ACCOUNT
        #
        total_per_column_expense = {}
        first_account = '1'
        for account in obj_account.browse(self.cr, self.uid, expense_account_ids):
            print_account = '1'
            total_account_amount = 0.0
            line = []
            addLine = False
            aIndex = 0
            for analytic in obj_analytic.browse(self.cr, self.uid, analytic_ids):
                
                report_expense_id = obj_aie_report.search(self.cr, self.uid, [('type','=','expense'),('account_id','=',account.id),('analytic_account_id','=',analytic.id)])
                report_account = obj_aie_report.browse(self.cr, self.uid, report_expense_id)
                
                if report_account:    

                    res = {
                        'first_account' : first_account,
                        'is_income' : '0',
                        'is_total' : '0',
                        'is_net' : '0',
                        'print_account' : print_account,
                        'account' : report_account[0].account,
                        'analytic_account' : analytic.code + ' - ' + analytic.name,
                        'report_type' : report_type,
                    }
                    
                    
                    first_account = '0'
                    print_account = '0'
                    account_amount = report_account[0].amount

                    if account_amount != 0.0 or selection == 'all':
                        addLine = True
                    
                    account_amount_locale = locale.format("%20.0f",account_amount, grouping=True)  
                    res.update({
                        'amount' : account_amount_locale,
                    })
                    
                    total_account_amount = total_account_amount + account_amount
                
                    if aIndex not in total_per_column_expense:
                        total_per_column_expense[aIndex] = 0.0    
                    total_per_column_expense[aIndex] = total_per_column_expense[aIndex] + account_amount 
                    aIndex = aIndex + 1
                    
                else:
                    res = {
                        'first_account' : first_account,
                        'is_income' : '0',
                        'is_total' : '0',
                        'is_net' : '0',
                        'print_account' : print_account,
                        'account' : account.code + ' - ' + account.name,
                        'analytic_account' : analytic.code + ' - ' + analytic.name,
                        'report_type' : report_type,
                        'amount' : locale.format("%20.0f",0.0, grouping=True),
                    }
                    
                    if selection == 'all':
                        addLine = True
                    
                    first_account = '0'
                    print_account = '0'
                    
                    if aIndex not in total_per_column_expense:
                        total_per_column_expense[aIndex] = 0.0
                    aIndex = aIndex + 1
                
                line.append(res)
        
            if addLine and line:
                if report_type == 'detailed':
                    for res in line:
                        self.result_acc.append(res)
                elif report_type == 'summary':
                    self.result_acc.append(line[0])
                
                # Total for account
                total_account_amount_locale = locale.format("%20.0f",total_account_amount, grouping=True)
                res = {
                    'first_account' : '0',
                    'is_income' : '0',
                    'is_total' : '1',
                    'is_net' : '0',
                    'print_account' : '0',
                    'amount' : total_account_amount_locale,
                    'report_type' : report_type,
                }
            
                self.result_acc.append(res)
            elif line:
                if line[0]['first_account'] == '1':
                    first_account = '1'
            
        #
        # TOTALS PER COLUMN
        #
        print_account = '1'
        total_of_totals_expense = 0
        total_line = []
        for k in total_per_column_expense:
            total_account_amount_locale = locale.format("%20.0f",total_per_column_expense[k], grouping=True)
            total_of_totals_expense += total_per_column_expense[k]
            #if _debug:
            #    _logger.debug('==> K : %s , V : %f',k, total_per_column[k])
            
            res = {
                'first_account' : '0',
                'is_income' : '0',
                'is_total' : '1',
                'is_net' : '0',
                'print_account' : print_account,
                'account' : 'TOTALS',
                'amount' : total_account_amount_locale,
                'report_type' : report_type,
            }
            print_account = '0'
            total_line.append(res)

        if report_type == 'detailed':
            for res in total_line:
                self.result_acc.append(res)

        total_of_totals_locale = locale.format("%20.0f",total_of_totals_expense, grouping=True)
        res = {
            'first_account' : '0',
            'is_income' : '0',
            'is_total' : '1',
            'is_net' : '0',
            'print_account' : print_account,
            'account' : 'TOTALS',
            'amount' : total_of_totals_locale,
            'report_type' : report_type,
        }
            
        self.result_acc.append(res)
        
        #
        # TOTALS OF TOTALS PER COLUMN
        #
        first_account = '1'
        print_account = '1'
        total_of_totals = 0
        total_line = []
        for k in total_per_column_income:
            total_account_amount_locale = locale.format("%20.0f",total_per_column_income[k] - total_per_column_expense[k], grouping=True)
            total_of_totals += total_per_column_income[k] - total_per_column_expense[k]
            #if _debug:
            #    _logger.debug('==> K : %s , V : %f',k, total_per_column[k])
            
            res = {
                'first_account' : first_account,
                'is_income' : '0',
                'is_total' : '1',
                'is_net' : '1',
                'print_account' : print_account,
                'account' : 'TOTALS',
                'amount' : total_account_amount_locale,
                'report_type' : report_type,
            }
            first_account = '0'
            print_account = '0'
            total_line.append(res)

        if report_type == 'detailed':
            for res in total_line:
                self.result_acc.append(res)

        total_of_totals_locale = locale.format("%20.0f",total_of_totals, grouping=True)
        res = {
            'first_account' : '0',
            'is_income' : '0',
            'is_total' : '1',
            'is_net' : '1',
            'print_account' : print_account,
            'account' : 'TOTALS',
            'amount' : total_of_totals_locale,
            'report_type' : report_type,
        }
            
        self.result_acc.append(res)
        
        return self.result_acc

report_sxw.report_sxw('report.isf.iae.webkit', 'isf.iae.analytic', 'addons/isf_income_and_expense_report/report/income_and_expense.mako', parser=isf_iae_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
