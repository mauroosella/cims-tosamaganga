<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1><center><b><i>Payroll</i></b></center></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print header()
    print lines()
    """
    %>

    <center>
        <table style="width: 50%" class="basic_table">
            %for head in header():
            <tr>
                <td> <b>Payslip </b></td>
                <td> <b>Period </b></td>
                <td> <b>Grade </b></td>
            </tr>
            <tr>
                <td>
                        ${head['name']} 
                </td>
                <td>
                    ${head['date_start']} - ${head['date_end']}
                </td>
                <td>
                    ${head['grade']}
                </td>
            </tr>
            %endfor
        </table>
    </center>
    
    <br/>

    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td> Payslip </td>
            <td> Employee </td>
            <td> Grade </td>
            <td> Gross </td>
            <td> Income Tax </td>
            <td> S.Duty </td>
            <td> H/G </td>
            <td> K/Guri </td>
            <td> Adv. </td>
            <td> Net </td>
            <td> Signature </td>
        </tr>
        <tr class="row">
            <td colspan="11">
                <table class="tr_bottom_line"></table>
            </td>
        </tr>
        %for data in lines():
            %if data['is_total'] == '0':
                <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td class="row">${data['number']}</td>
                    <td class="row">${data['employee']}</td>
                    <td class="row">${data['grade']}</td>
                    <td class="currency">${data['gross']}</td>
                    <td class="currency">${data['income_tax']}</td>
                    <td class="currency">${data['s_duty']}</td>
                    <td class="currency">${data['hg']}</td>
                    <td class="currency">${data['kguri']}</td>
                    <td class="currency">${data['adv']}</td>
                    <td class="currency">${data['net']}</td>
                    <td/>
                </tr>
            %endif
            %if data['is_total'] == '1':
                <tr class="row">
                    <td colspan="11">
                        <table class="tr_bottom_line"></table>
                    </td>
                </tr>
                <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td class="row"></td>
                    <td class="currency"><b>Total :</b></td>
                    <td class="row"></td>
                    <td class="currency"><b>${data['gross']}</b></td>
                    <td class="currency"><b>${data['income_tax']}</b></td>
                    <td class="currency"><b>${data['s_duty']}</b></td>
                    <td class="currency"><b>${data['hg']}</b></td>
                    <td class="currency"><b>${data['kguri']}</b></td>
                    <td class="currency"><b>${data['adv']}</b></td>
                    <td class="currency"><b>${data['net']}</b></td>
                </tr>
            %endif
        %endfor         
    </table>
</body>
</html>
