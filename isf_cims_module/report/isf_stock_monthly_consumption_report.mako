<html>
<head>
    <style type="text/css">
        ${css}
@media print {
   thead {display: table-header-group;}
}
 
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:xx-small;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:xx-small;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}

.td-total {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:xx-small;
    font-weight:bold;
}

.td-section {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:x-large;
    font-weight:bold;
}

.td-space {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:x-large;
    font-weight:bold;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    print report_header()
    """
    local_report_header = report_header()
    local_headers = headers()
    local_lines = lines()
    local_top_lines = top_lines()
    len_local_headers = len(local_headers) + 3
    for head in local_report_header:
    	if head['group_criteria'] == 'product':
    		group_criteria = 'Products'
    	if head['group_criteria'] == 'category':
    		group_criteria = 'Categories'
    	if head['group_criteria'] == 'tag':
    		group_criteria = 'Active Ingredients'
    	local_show_data = head['show_data']
    	local_currency = head['currency']
    %> 
    
    <h1>Stock Monthly Consumption Report</h1>

    <center>
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 20%;">Date Start</th>
                <th class="td-data" style="width: 20%;">Date End</th>
                <th class="td-data" style="width: 20%;">Location</th>
                <th class="td-data" style="width: 20%;">Report Type</th>
                <th class="td-data" style="width: 20%;">Group By</th>
            </tr>
            %for head in local_report_header:
                <tr >
                	<td class="td-data" style="width: 20%;text-align:center">${head['date_start']}</td>
                    <td class="td-data" style="width: 20%;text-align:center">${head['date_end']}</td>
                    <td class="td-data" style="width: 20%;text-align:center">${head['location']}</td>
                    <td class="td-data" style="width: 20%;text-align:center">${head['show_data']} per ${head['report_type']}</td>
                    <td class="td-data" style="width: 20%;text-align:center">${group_criteria}</td>
                </tr>
            %endfor
        </table>
    </center>
    
    <br />
    <table class="table-data">
    	<tr>
    		%for head in local_report_header:
    			<th class="td-section" style="width: 25%;text-align:left">Top Ten</th>
    		%endfor
        	%for head in local_headers:
            	<th class="td-data" style="text-align:center">${head['name']}</th>
        	%endfor
        	<th class="td-data" style="text-align:right">Total</th>
        	<th class="td-data" style="text-align:right">Average</th>
        </tr>
        %for line in local_top_lines:
	        <tr>
	        	<td class="td-data" style="width: 25%;text-align:left">${line['criteria']}</td>
	        	%for head in local_headers:
	        		%if local_show_data == 'qty':
		        		<td class="td-data" style="text-align:right">${line[head['name']+'_qty']}</th>
		        	%else:
		        		<td class="td-data" style="text-align:right">${line[head['name']+'_value']} ${local_currency}</th>
		        	%endif
	        	%endfor
	        	%if local_show_data == 'qty':
	        		<td class="td-data" style="text-align:right">${line['total_qty']}</td>
	        		<td class="td-data" style="text-align:right">${line['average_qty']}</td>
	        	%else:
	        		<td class="td-data" style="text-align:right">${line['total_value']} ${local_currency}</td>
	        		<td class="td-data" style="text-align:right">${line['average_value']} ${local_currency}</td>
	        	%endif
	        </tr>
    	%endfor
    	<tr>
    		%for head in local_report_header:
    			<th class="td-section" style="width: 25%;text-align:left">All ${group_criteria}</th>
    		%endfor
        	%for head in local_headers:
            	<th class="td-data" style="text-align:center">${head['name']}</th>
        	%endfor
        	<th class="td-data" style="text-align:right">Total</th>
        	<th class="td-data" style="text-align:right">Average</th>
        </tr>
        %for line in local_lines:
	        <tr>
	        	<td class="td-data" style="width: 25%;text-align:left">${line['criteria']}</td>
	        	%for head in local_headers:
	            	%if local_show_data == 'qty':
		        		<td class="td-data" style="text-align:right">${line[head['name']+'_qty']}</th>
		        	%else:
		        		<td class="td-data" style="text-align:right">${line[head['name']+'_value']} ${local_currency}</th>
		        	%endif
	        	%endfor
	        	%if local_show_data == 'qty':
	        		<td class="td-data" style="text-align:right">${line['total_qty']}</td>
	        		<td class="td-data" style="text-align:right">${line['average_qty']}</td>
	        	%else:
	        		<td class="td-data" style="text-align:right">${line['total_value']} ${local_currency}</td>
	        		<td class="td-data" style="text-align:right">${line['average_value']} ${local_currency}</td>
	        	%endif
	        </tr>
    	%endfor
    	%if local_show_data == 'value':
    		%for line in totals():
		        <tr>
		        	<td class="td-total" style="width: 25%;text-align:left">${line['criteria']}</td>
		        	%for head in local_headers:
		        		<td class="td-total" style="text-align:right">${line[head['name']+'_value']} ${local_currency}</th>
		        	%endfor
	        		<td class="td-total" style="text-align:right">${line['total_value']} ${local_currency}</td>
	        		<td class="td-total" style="text-align:right">${line['average_value']} ${local_currency}</td>
		        </tr>
		    %endfor
		%endif
    </table>

</body>
</html>
