<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <center>
        <h3>Sales Summary Report & Type Of Qty</h3>
    </center>
    %for data in headers():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Location</th>
                <th class="td-data" style="width: 30%;">Date Start</th>
                <th class="td-data" style="width: 30%;">Date Stop</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 40%;"><b>${data['store']}</b></th>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date_start']}</td>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date_end']}</td>
            </tr>
        </table>
    </center>
    %endfor
    <br/>
    <table class="table-data" width="100%">
        <tr>
            <th class="td-data" style="width: 10%;">Code</th>
            <th class="td-data" style="width: 30%;">Product</th>
            <th class="td-data" style="width: 20%;">Sold Value</th>
            <th class="td-data" style="width: 10%;">Sold Qty</th>
            <th class="td-data" style="width: 10%;">FoC Qty</th>
            <th class="td-data" style="width: 10%;">Used Qty</th>
            <th class="td-data" style="width: 20%;">Total Qty</th>
        </tr>
        %for data in lines():
        <tr>
            %if data['is_header'] == True:
                <tr>    
                    <td class="td-data" colspan="7"><b>${data['store']} - ${data['type']}</b></td>
                </tr>
                <tr>
                    <td class="td-data" style="width: 10%;text-align:right">${data['code']}</th>
                    <td class="td-data" style="width: 30%;">${data['product']}</th>
                    <td class="td-data" style="width: 20%;text-align:right;">${data['sales']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['sales_qty']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['foc_qty']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['internal_qty']}</td>
                    <td class="td-data" style="width: 20%;text-align:right;">${data['total_qty']}</td>
                </tr>
            %endif
            %if data['is_total'] == False and data['is_header'] == False:
                <td class="td-data" style="width: 10%;text-align:right">${data['code']}</th>
                <td class="td-data" style="width: 30%;">${data['product']}</th>
                <td class="td-data" style="width: 20%;text-align:right;">${data['sales']}</td>
                <td class="td-data" style="width: 10%;text-align:right;">${data['sales_qty']}</td>
                <td class="td-data" style="width: 10%;text-align:right;">${data['foc_qty']}</td>
                <td class="td-data" style="width: 10%;text-align:right;">${data['internal_qty']}</td>
                <td class="td-data" style="width: 20%;text-align:right;">${data['total_qty']}</td>
            %endif
            %if data['is_total'] == True:
                <td class="td-data" style="width: 40%;" colspan="2"><b>Totals :</b></th>
                <td class="td-data" style="width: 20%;text-align:right;"><b>${data['sales']}</b></td>
                <td class="td-data" style="width: 10%;text-align:right;"><b>${data['sales_qty']}</b></td>
                <td class="td-data" style="width: 10%;text-align:right;"><b>${data['foc_qty']}</b></td>
                <td class="td-data" style="width: 10%;text-align:right;"><b>${data['internal_qty']}</b></td>
                <td class="td-data" style="width: 20%;text-align:right;"><b>${data['total_qty']}</b></td>
            %endif
        </tr>
        %endfor
    </table>
    </body>
</html>
