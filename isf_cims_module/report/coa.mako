<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1>Chart of Accounts</h1>
    <%
    level = -1
    %>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print lines()
    """
    %>

    <table style="width: 100%">
        <tr class="table_header">
            <td class="label">Account</td>
            <td class="currency">Currency</td>
            <td class="currency">Type</td>
        </tr>
        <tr class="row">
            <td colspan="3">
                <table class="tr_bottom_line"></table>
            </td>
        </tr>
        %for account in lines():
            <% alevel = account['level'] %>
            <tr class="row" style="border-top: 0.5px solid blue;">
                <td class="label">
                    %for l in range(alevel):
                        &nbsp;&nbsp;
                    %endfor
                    <% level = alevel %>
                    %if account['code'] == '0':
                        ${account['name']}
                    %else:
                        ${account['code']} - ${account['name']}
                    %endif
                </td>
                <td class="currency">
                    ${account['currency_id']['name']}
                </td>
                <td class="currency">
                    ${account['type']}
                </td>
            <tr>
        %endfor
    </table>
</body>
</html>
