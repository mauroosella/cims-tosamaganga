<html>
<head>
    <style type="text/css">
        ${css}
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print header()
    """
    %>
     <center>
        <h3>INCENTIVE REPORT</h3>
    </center>
    %for data in header():
        <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 50%;">Period</th>
                <th class="td-data" style="width: 50%;">Grade</th>
            </tr>
            <tr>
                <th class="td-data" style="width: 50%;">${data['period']}</th>
                <td class="td-data" style="width: 50%;text-align:right">${data['grade']}</td>
            </tr>
        </table>
        </center>
    %endfor
    <br/>
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 10%;">Code</th>
                <th class="td-data" style="width: 35%;">Employee</th>
                <th class="td-data" style="width: 10%;">Grade</th>
                <th class="td-data" style="width: 10%;">Amount</th>
                <th class="td-data" style="width: 5%;">Currency</th>
                <th class="td-data" style="width: 30%;">Signature</th>
            </tr>
        %for data in lines():
            <tr>
                <th class="td-data" style="width: 10%;text-align:left">${data['code']}</th>
                <td class="td-data" style="width: 35%;text-align:left">${data['employee']}</td>
                <th class="td-data" style="width: 10%;text-align:left">${data['grade']}</th>
                <td class="td-data" style="width: 10%;text-align:right">${data['amount']}</td>
                <td class="td-data" style="width: 10%;text-align:right">${data['currency']}</td>
                <td class="td-data" style="width: 35%;text-align:right"/>
            </tr>
        %endfor
    </table>
</body>
</html>
