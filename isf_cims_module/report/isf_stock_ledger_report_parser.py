# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_ledger_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.ledger.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_stock_ledger_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
            'totals' : self.totals,
            'total' : self.total,
        })
        self.context = context
        self.result_type = []
        self.result_acc = []
        self.headers_acc = []
        self.totals_acc = []
        self.total_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.ledger.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_start = date_s_obj[0]['date_start']
        date_end = date_e_obj[0]['date_end']
        product_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['product_id'])
        product = product_o[0]['product_id']
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['store_id'])
        store = store_o[0]['store_id']
        
        res = { 
            'date_start' : date_start,
            'date_end' : date_end,
            'product' : product[1],
            'store' : store[1],
        }
        self.headers_acc.append(res)
        
        return self.headers_acc
        
    def _get_opening_balance(self, cr, uid, store_id, product_id, date_start):
        stock_o = self.pool.get('stock.move')
        date = date_start + " 00:00:01"
        stock_in_ids = stock_o.search(cr, uid, [('location_dest_id','=',store_id),('date_expected','<',date),('product_id','=',product_id),('state','=','done')])
        stock_out_ids = stock_o.search(cr, uid, [('location_id','=',store_id),('date_expected','<',date),('product_id','=',product_id),('state','=','done')])
        
        if _debug:
            _logger.debug('date : %s', date)
            _logger.debug('OB : %s', stock_in_ids)
            _logger.debug('OB : %s', stock_out_ids)
        
        out_move = 0
        in_move = 0
        for move in stock_o.browse(cr, uid, stock_in_ids):
            factor = move.product_uom.factor
            in_move += move.product_qty / factor
            
        for move in stock_o.browse(cr, uid, stock_out_ids):
            factor = move.product_uom.factor
            out_move += move.product_qty / factor
                
        if _debug:
            _logger.debug('IN : %s OUT : %s', in_move, out_move)
        return in_move - out_move
    
    
    def _get_move_type(self, cr, uid, source_location, dest_location,store_id):
        move_type = ''
        if source_location.id == store_id and dest_location.usage in ['supplier','customer']:
            move_type = 'Out'
        elif source_location.id == store_id and dest_location.usage == 'inventory':
            move_type = 'Adjustment'
        elif dest_location.id == store_id and source_location.usage == 'inventory':
            move_type = 'Inventory'
        elif dest_location.id == store_id and source_location.usage == 'supplier':
            move_type = 'In'
        elif (source_location.id == store_id and dest_location.usage == 'internal') or (dest_location.id == store_id and source_location.usage == 'internal'):
            move_type = 'Internal'
        else:
            move_type = 'Unknown'
            _logger.info('STOCK LEDGER LocationId[%d] LocationDestId[%d]',source_location.id,dest_location.id)
        
        if _debug:
            _logger.debug('Move Type: %s', move_type)
            
        return move_type
     
    def _lines_stock_ledger_qty(self, cr, uid, store, product, date_start, date_end):
        res = {}
        total_in_qty = 0.0
        total_out_qty = 0.0
        total_balance = 0.0
        total_price = 0.0
        total = {
            'balance' : total_balance,      
            'price' : total_price,      
        }
        totals = {
            'total_in_qty' : total_in_qty,      
            'total_out_qty' : total_out_qty,      
        }
        product_id = product[0]
        store_id = store[0]
        pharm_pos_obj = self.pool.get('isf.pharmacy.pos')
        pos_obj = self.pool.get('isf.pos')
        stock_o = self.pool.get('stock.move')
        int_o = self.pool.get('isf.stock.moves')
        don_o = self.pool.get('isf.stock.donation')
        stock_disposal_o = self.pool.get('isf.stock.output')
        pharm_disposal_o = self.pool.get('isf.pharmacy.stock.output')
        
        stock_in_ids = stock_o.search(cr, uid, [('location_dest_id','=',store_id),('date_expected','>=',date_start),('date_expected','<=',date_end),('product_id','=',product_id),('state','=','done')],order='date_expected asc')
        stock_out_ids = stock_o.search(cr, uid, [('location_id','=',store_id),('date_expected','>=',date_start),('date_expected','<=',date_end),('product_id','=',product_id),('state','=','done')],order='date_expected asc')
        
        stock_ids = stock_in_ids + stock_out_ids
        
        opening_balance = self._get_opening_balance(cr, uid, store_id, product_id, date_start)
        total_in_qty = opening_balance
        rec = {
            'product' : '',
            'type': '', 
            'tr_num': '', 
            'id_num': '',
            'date' : '', 
            'ref' : "Opening Balance",
            'store' : '',
            'in_qty' : opening_balance ,
            'out_qty' : '' ,
            'batch' :'',
            'expiry_date' : '',
            'price' : '',
            'date' : date_start,
        }
        self.result_acc.append(rec)
       
        if _debug:
            _logger.debug('STOCK IDS : %s', stock_ids)
            
        for move in stock_o.browse(cr, uid, stock_ids):
            rec = False
            
            move_type = self._get_move_type(cr, uid, move.location_id, move.location_dest_id,store_id)
            
            if move_type == 'Internal':
                int_ids = int_o.search(cr, uid, [('sequence_id','=',move.note)])
                rec = self._manage_internal(cr, uid, move.product_id.name_template, move.location_id.id, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name,int_ids)
                
                if not rec:
                    rec = self._manage_generic_internal(cr, uid, move.product_id.name_template, move.location_id.id, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name,int_ids)
                
            if move_type == 'In':
                # Check if the move is by Donation
                don_ids = don_o.search(cr, uid, [('sequence_id','=',move.note)])
                if _debug:
                    _logger.debug('IN IDS : %s', don_ids)
                if don_ids:
                    rec = self._manage_donation(cr, uid, move.product_id.name_template, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name,don_ids)
                else:
                    rec = self._manage_purchase_order(cr, uid, move.product_id.name_template, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name,move.origin)
            
            if move_type == 'Out':
                # Search in pharmacy PoS
                pharm_pos_ids = pharm_pos_obj.search(cr, uid, [('sequence_id','=',move.note)])
                if len(pharm_pos_ids) > 0:
                    rec = self._manage_pharm_pos(cr, uid, product_id, pharm_pos_ids,move.location_id.name,move.product_qty / move.product_uom.factor)
            
                # Search in generic PoS
                if not rec:
                    pos_ids = pos_obj.search(cr, uid, [('sequence_id','=',move.note)])
                    if len(pos_ids) > 0:
                        rec = self._manage_generic_pos(cr, uid, product_id, pos_ids)
                
                # Search in Sales Orders
                if not rec:
                    rec = self._manage_sale_order(cr, uid, move.product_id.name_template, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name,move.origin)
                
                # Search in return products
                if not rec and move.move_returned_from:
                    rec = self._manage_returned(cr, uid, move.product_id.name_template, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name, move.origin)
                    
                if not rec:
                    rec = self._manage_generic_out(cr, uid, move.product_id.name_template, move.location_id.id, move.location_dest_id.name, move.product_qty / move.product_uom.factor, move.date[:10], move.name, move.origin)
            
            if move_type == 'Adjustment':
                # Stock disposal
                disp_ids = stock_disposal_o.search(cr, uid, [('sequence_id','=',move.note)])
                if len(disp_ids) > 0:
                    rec = self._manage_stock_disposal(cr, uid, product_id, disp_ids,move.location_id.name,move.product_qty / move.product_uom.factor)
                
                # Pharmacy disposal
                if not rec:
                    disp_ids = pharm_disposal_o.search(cr, uid, [('id','=',move.note)])
                    if len(disp_ids) > 0:
                        rec = self._manage_stock_disposal(cr, uid, product_id, disp_ids,move.location_id.name,move.product_qty / move.product_uom.factor)
                
                # Stock Inventory by name
                if not rec:
                    inv_ids = stock_o.search(cr, uid, [('name','=',move.name)])
                    if len(inv_ids) > 0:
                        rec = self._manage_inventory_loss(cr, uid, product_id, inv_ids,move.location_dest_id.name,move.product_qty / move.product_uom.factor)
                    
                # Stock Inventory by move
                if not rec:
                    inv_ids = stock_o.search(cr, uid, [('id','=',move.id)])
                    if len(inv_ids) > 0:
                        rec = self._manage_inventory_loss(cr, uid, product_id, inv_ids,move.location_dest_id.name,move.product_qty / move.product_uom.factor)
                        
            if move_type == 'Inventory':
                # Stock inventory
                inv_ids = stock_o.search(cr, uid, [('name','=',move.name)])
                if len(inv_ids) > 0:
                    rec = self._manage_inventory(cr, uid, product_id, inv_ids,move.location_id.name,move.product_qty / move.product_uom.factor)
                
                if not rec:
                    inv_ids = stock_o.search(cr, uid, [('id','=',move.id)])
                    if len(inv_ids) > 0:
                        rec = self._manage_inventory(cr, uid, product_id, inv_ids,move.location_id.name,move.product_qty / move.product_uom.factor)
            
            if _debug:
                _logger.debug('==> rec : %s', rec)
                _logger.debug('    cost_price : %s', move.prodlot_id.cost_price)
                _logger.debug('    product_qty : %s', move.product_qty)
                
            if rec:
                price = 0.0
                if move.prodlot_id:
                    price = move.prodlot_id.cost_price
                
                rec.update({
                    'batch' : move.prodlot_id.name,
                    'expiry_date' : str(move.prodlot_id.life_date)[:10],
                    'price' : price * move.product_qty,
                })
                rec_list = res.get(move.date_expected)
                if rec_list is None:
                    rec_list = []
                rec_list.append(rec)
                res.update({move.date_expected:rec_list})
                
                if rec['in_qty']:
                    total_in_qty += rec['in_qty']
                if rec['out_qty']:
                    total_out_qty += rec['out_qty']
                if rec['price']:
                    total_price += rec['price']
                total_balance = total_in_qty - total_out_qty
                totals.update({
                    'total_in_qty' : total_in_qty,      
                    'total_out_qty' : total_out_qty,      
                })
                total.update({
                    'balance' : total_balance,      
                    'price' : total_price,      
                })
                
        self.total_acc.append(total)
        self.totals_acc.append(totals)
                
        list_keys = res.keys()
        list_keys.sort()
        for k in list_keys:
            move_list = res.get(k)
            for move in move_list:
                self.result_acc.append(move)    
        
        return self.result_acc
    
    def _manage_inventory(self, cr, uid, product_name, inv_ids,store_name, qty):
        move_o = self.pool.get('stock.move')
        for move in move_o.browse(cr, uid, inv_ids):
            id_num = "/"
            if move.name:
                id_num = move.name
            rec = {
                'product' : product_name,
                'type': 'Inventory', 
                'tr_num': '/', 
                'id_num' : id_num,
                'date' : move.date, 
                'ref' : move.name,
                'store' : store_name,
                'in_qty' : qty ,
                'out_qty' : '' ,
            }
        
            return rec
        return False
    
    def _manage_inventory_loss(self, cr, uid, product_name, inv_ids,store_name, qty):
        move_o = self.pool.get('stock.move')
        for move in move_o.browse(cr, uid, inv_ids):
            id_num = "/"
            if move.name:
                id_num = move.name
            rec = {
                'product' : product_name,
                'type': 'Inventory', 
                'tr_num': '/', 
                'id_num' : id_num,
                'date' : move.date, 
                'ref' : move.name,
                'store' : store_name,
                'in_qty' : '' ,
                'out_qty' : qty ,
            }
        
            return rec
        return False
    
    def _manage_stock_disposal(self, cr, uid, product_name, disp_ids,store_name, qty):
        move_o = self.pool.get('stock.move')
        for move in move_o.browse(cr, uid, disp_ids):
            id_num = "/"
            if stock.sequence_id:
                id_num = stock.sequence_id
            rec = {
                'product' : product_name,
                'type': 'Inventory', 
                'tr_num': '/', 
                'id_num' : id_num,
                'date' : stock.date, 
                'ref' : stock.name,
                'store' : store_name,
                'in_qty' : qty ,
                'out_qty' : '' ,
            }
        
            return rec
        return False
    
    def _manage_generic_internal(self, cr, uid, product_name, store_name, qty, date, ref,stock_ids):
        rec = {
            'product' : product_name,
            'type': 'Internal/Unknown', 
            'tr_num': '/', 
            'id_num' : '/',
            'date' : date, 
            'ref' : ref,
            'store' : store_name,
            'in_qty' : '' ,
            'out_qty' : qty ,
        }
    
        return rec
    
    def _manage_generic_out(self, cr, uid, product_name, store_name, qty, date, ref, origin):
        rec = {
            'product' : product_name,
            'type': 'Out/Unknown', 
            'tr_num': '/', 
            'id_num' : '/',
            'date' : date, 
            'ref' : origin,
            'store' : store_name,
            'in_qty' : '' ,
            'out_qty' : qty ,
        }
    
        return rec
    
    def _manage_returned(self, cr, uid, product_name, store_name, qty, date, ref, origin):
        rec = {
            'product' : product_name,
            'type': 'Return', 
            'tr_num': '/', 
            'id_num' : '/',
            'date' : date, 
            'ref' : origin,
            'store' : store_name,
            'in_qty' : '' ,
            'out_qty' : qty ,
        }
    
        return rec
    
    def _manage_internal(self, cr, uid, product_name, source_id, store_name, qty, date, ref,stock_ids):
        stock_o = self.pool.get('isf.stock.moves')
        for stock in stock_o.browse(cr, uid, stock_ids):
            id_num = "/"
            if stock.sequence_id:
                id_num = stock.sequence_id
            
            in_qty = 0
            out_qty = 0
            
            if stock.location_source_id.id == source_id:
                out_qty = qty
            else:
                in_qty = qty
                
            rec = {
                'product' : product_name,
                'type': 'Transfer', 
                'tr_num': '/', 
                'id_num' : id_num,
                'date' : date, 
                'ref' : ref,
                'store' : store_name,
                'in_qty' : in_qty ,
                'out_qty' : out_qty ,
            }
        
            return rec
        return False
        
    def _manage_donation(self, cr, uid, product_name, store_name, qty, date, ref,stock_ids):
        don_o = self.pool.get('isf.stock.donation')
        don_o.browse(cr, uid, stock_ids)
        stock = don_o.browse(cr, uid, stock_ids)[0]
        
        id_num = "/"
        if stock.sequence_id:
            id_num = stock.sequence_id
        rec = {
            'product' : product_name,
            'type': 'In', 
            'tr_num': stock.move_cost_id.name, 
            'id_num' : id_num,
            'date' : date, 
            'ref' : stock.name,
            'store' : store_name,
            'in_qty' : qty ,
            'out_qty' : '' ,
        }
        
        return rec
        
    def _manage_purchase_order(self, cr, uid, product_name, store_name, qty, date, ref,origin):
        order_o = self.pool.get('purchase.order')
        order_ids = order_o.search(cr, uid, [('name','=',origin)])
        if len(order_ids) > 0:
            order = order_o.browse(cr, uid, order_ids)[0]
        
            if _debug:
                _logger.debug('==> Manage Purchase Orders')
                _logger.debug('==> Invoices: %s', order.invoice_ids)
        
            move = False
            for invoice in order.invoice_ids:
                if _debug:
                    _logger.debug('==> Invoice ID: %s', invoice.id)
                    _logger.debug('==> Invoice lines: %s', invoice.invoice_line)
                for line in invoice.invoice_line:
                    if _debug:
                        #_logger.debug('==> product_name: %s', product_name)
                        _logger.debug('==> line.product_id.name: %s', line.product_id.name)
                    if line.product_id.name_template == product_name:
                        move = invoice.move_id
                        break;
        
            if move:
                rec = {
                    'product' : product_name,
                    'type': 'In', 
                    'tr_num': move.name, 
                    'id_num' : "/",
                    'date' : date, 
                    'ref' : order.name,
                    'store' : store_name,
                    'in_qty' : qty ,
                    'out_qty' : '' ,
                }
            else:
                rec = {
                    'product' : product_name,
                    'type': 'In', 
                    'tr_num': "/", 
                    'id_num' : "/",
                    'date' : date, 
                    'ref' : order.name,
                    'store' : store_name,
                    'in_qty' : qty ,
                    'out_qty' : '' ,
                }
                
            if _debug:
                _logger.debug('==> new record: %s', rec)
    
            return rec
        return False
    
    
    def _manage_sale_order(self, cr, uid, product_name, store_name, qty, date, ref,origin):
        order_o = self.pool.get('sale.order')
        order_ids = order_o.search(cr, uid, [('name','=',origin)])
        if len(order_ids) > 0:
            order = order_o.browse(cr, uid, order_ids)[0]
        
            if _debug:
                _logger.debug('==> Manage Sale Orders')
                _logger.debug('==> Invoices: %s', order.invoice_ids)
        
            move = False
            for invoice in order.invoice_ids:
                if _debug:
                    _logger.debug('==> Invoice ID: %s', invoice.id)
                    _logger.debug('==> Invoice lines: %s', invoice.invoice_line)
                for line in invoice.invoice_line:
                    if _debug:
                        _logger.debug('==> product_name: %s', product_name)
                        _logger.debug('==> line.product_id.name: %s', line.product_id.name)
                    if line.product_id.name_template == product_name:
                        move = invoice.move_id
                        break;
        
            if move:
                rec = {
                    'product' : product_name,
                    'type': 'Out', 
                    'tr_num': move.name, 
                    'id_num' : "/",
                    'date' : date, 
                    'ref' : order.name,
                    'store' : store_name,
                    'in_qty' : '' ,
                    'out_qty' : qty ,
                }
            else:
                rec = {
                    'product' : product_name,
                    'type': 'Out', 
                    'tr_num': "/", 
                    'id_num' : "/",
                    'date' : date, 
                    'ref' : order.name,
                    'store' : store_name,
                    'in_qty' : '' ,
                    'out_qty' : qty ,
                }
                
                if _debug:
                    _logger.debug('==> new record: %s', rec)
        
                return rec
        return False
    
    
    
    def _manage_pharm_pos(self, cr, uid, product_id, pharm_pos_ids,store_name, product_qty):
        pharm_pos_obj = self.pool.get('isf.pharmacy.pos')
            
        pos = pharm_pos_obj.browse(cr, uid, pharm_pos_ids)[0]
        for line in pos.product_line_ids:
            if line.product.id == product_id:
                p_name = "["+line.product.default_code+"] " + line.product.name_template
                tr_num = "/"
                if pos.account_move_1:
                    tr_num = pos.account_move_1.name
                    
                trans_type = 'Sales'
                if pos.type == 'foc':
                    trans_type = 'Foc'
                elif pos.type == 'internal':
                    trans_type = 'Internal'
                
                id_num = "/"
                if pos.sequence_id:
                    id_num = pos.sequence_id
                rec = {
                    'product' : p_name,
                    'type': trans_type, 
                    'tr_num': tr_num,
                    'id_num' : id_num, 
                    'date' : pos.date, 
                    'ref' : pos.name,
                    'store' : store_name,
                    'out_qty' : product_qty,
                    'in_qty' : '',
                }
                    
                return rec
     
    def _manage_generic_pos(self, cr, uid, product_id, pos_ids):
        pos_obj = self.pool.get('isf.pos')
        pos = pos_obj.browse(cr, uid, pos_ids)[0]
        
        if _debug:
            _logger.debug('Manage Generic PoS : %s',pos)
        
        for line in pos.product_ids:
            if line.product_id.id == product_id:
                p_name = "["+line.product_id.default_code+"] " + line.product_id.name_template
                tr_num = "/"
                if pos.journal_items_id:
                    tr_num = pos.journal_items_id.name
                id_num = "/"
                if pos.sequence_id:
                    id_num = pos.sequence_id
                
                trans_type = 'Sales'
                if pos.transaction_type_product == 'foc':
                    trans_type = 'Foc'
                elif pos.transaction_type_product == 'internal':
                    trans_type = 'Internal'
                elif pos.transaction_type_product == 'expired':
                    trans_type = 'Expired'
                elif pos.transaction_type_product == 'broken':
                    trans_type = 'Loss'
                
                rec = {
                    'product' : p_name,
                    'type': trans_type, 
                    'tr_num': tr_num, 
                    'id_num' : id_num,
                    'date' : line.pos_id.date, 
                    'ref' : line.pos_id.name,
                    'store' : line.pos_id.location_id.name,
                    'price' : line.unit_price,
                    'out_qty' : line.quantity,
                    'in_qty' : '',
                }
                
                return rec
    
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.ledger.report')
        
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_start = date_s_obj[0]['date_start']
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_end = date_e_obj[0]['date_end']
        product_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['product_id'])
        product = product_o[0]['product_id']
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['store_id'])
        store = store_o[0]['store_id']
        
        return self._lines_stock_ledger_qty(self.cr, self.uid, store,product, date_start, date_end)
    
    def totals(self, ids=None, done=None):
        return self.totals_acc
    
    def total(self, ids=None, done=None):
        return self.total_acc
    

report_sxw.report_sxw('report.isf.stock.ledger.webkit', 'isf.stock.ledger.report', 'addons/isf_cims_module/report/psl.mako', parser=isf_stock_ledger_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
