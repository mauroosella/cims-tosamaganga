<html>
<head>
    <style type="text/css">
        ${css}
        
        .slipt {page-break-after: always}
        
        .posted {
			font-size: xx-small;
			text-align: right;
			color: black;
		}
		
		.unposted {
			font-size: xx-small;
			text-align: right;
			color: blue;
		}
		
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h2><b><i>Detailed Monthly Income from Departments</i></b></h2>
    Unposted in blue
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print headers()
    print lines()
    """
    %>

    <h4>
    %for date in datelines():
        <table style="width:1000px;" border="0">
            <tr><td>Period:</td><td> ${date['begin_date']} - ${date['end_date']}</td>
            <td>Print Date :</td><td> ${date['print_date']}</td></tr>
        </table>
    %endfor
    </h4>
    
    <table  border="0" cellpadding="2px" >
        <tr class="table_header">
            %for header in headers():
            	%if header['analytic_account'] == 'Date':
                    <td style="width:1000px;">${header['analytic_account']}</td>
                %else:
                	<td style="width:1000px; text-align:right;">${header['analytic_account']}</td>
                %endif
            %endfor
        </tr>
        
            %for account in lines():
                %if account['print_account'] == '1':
                    <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td style="width:1000px;">${account['date']}</td>
                    %if account['posted']:
                    	<td style="width:1000px;" class="posted">${account['amount']}</td>
                    %else:
                    	<td style="width:1000px;" class="unposted">${account['amount']}</td>
                    %endif
                %endif
                %if account['print_account'] == '0' and account['is_total'] == '0':
                	%if account['posted']:
                    	<td style="width:1000px;" class="posted">${account['amount']}</td>
                    %else:
                    	<td style="width:1000px;" class="unposted">${account['amount']}</td>
                    %endif
                %endif
                %if account['print_account'] == '0' and account['is_total'] == '1':
                	%if account['posted']:
                    	<td style="width:1000px;" class="posted"><b>${account['amount']}</b></td>
                    %else:
                    	<td style="width:1000px;" class="unposted"><b>${account['amount']}</b></td>
                    %endif
                    
                %endif
            %endfor
        </tr>   
    </table>
    
    <h3><b><i>Accounts</i></b></h3>
    
    <table  border="0" cellpadding="2px" >
    	<tr class="table_header">
            <td style="width:400px;">Account</td>
            <td style="width:200px; text-align:right;">Debit</td>
            <td style="width:200px; text-align:right;">Credit</td>
        </tr>
        
            %for account in sub_lines():
            	%if account['name'] == 'Total':
            		<tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td style="width:400px; text-align:right;"><b>${account['name']}</b></td>
                    %if account['posted']:
	                    <td style="width:200px;" class="posted"><b>${account['debit']}</b></td>
	                    <td style="width:200px;" class="posted"><b>${account['credit']}</b></td>
	                %else:
                		<td style="width:200px;" class="unposted"><b>${account['debit']}</b></td>
	                    <td style="width:200px;" class="unposted"><b>${account['credit']}</b></td>
	                %endif
                    </tr>
            	%else:
                    <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td style="width:400px;">${account['name']}</td>
                    %if account['posted']:
	                    <td style="width:200px;" class="posted"><b>${account['debit']}</b></td>
	                    <td style="width:200px;" class="posted"><b>${account['credit']}</b></td>
	                %else:
                		<td style="width:200px;" class="unposted"><b>${account['debit']}</b></td>
	                    <td style="width:200px;" class="unposted"><b>${account['credit']}</b></td>
	                %endif
                    </tr>
                %endif
            %endfor
    </table>
  </body>
</html>