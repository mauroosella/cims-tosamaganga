# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import platform

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False



class isf_budget_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.budget.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_budget_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'datelines' : self.datelines,
        })
        self.context = context
        self.result_acc = []
        self.result_date = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_budget_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_start_date_from_period(self, period_id):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(self.cr, self.uid, [('id','=',period_id)])
        period = period_obj.browse(self.cr, self.uid, period_ids)[0]
        
        return period.date_start
        
    def _get_end_date_from_period(self, period_id):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(self.cr, self.uid, [('id','=',period_id)])
        period = period_obj.browse(self.cr, self.uid, period_ids)[0]
        
        return period.date_stop
    
    def datelines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.budget.report')
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        
        begin_period_id = report_date_begin[0]['begin_date'][0]
        end_period_id = report_date_end[0]['end_date'][0]
        
        begin_date = self._get_start_date_from_period(begin_period_id)
        end_date = self._get_end_date_from_period(end_period_id)
        
        res = {
            'begin_date' : begin_date,
            'end_date' : end_date,
        }
        
        self.result_date.append(res)
        
        return self.result_date
        
    def _update_with_totals(self, item_list, planned_amount, effective_amount):
        if len(item_list) > 0:
            item = item_list[0]
            
            effective_amount_locale = locale.format("%20.2f",effective_amount, grouping=True)
            
            balance = abs(planned_amount - effective_amount)
            balance_locale = locale.format("%20.2f",balance, grouping=True)
            
            isColored = '0'
            if abs(effective_amount) > abs(planned_amount):
                isColored = '1'
            
            percentage = (balance * 100) / abs(planned_amount)
            percentage_locale = locale.format("%4.2f",percentage, grouping=True)
            
            item.update({
                'effective_amount' : effective_amount_locale,
                'balance' : balance_locale,
                'isColored' : isColored,
                'percentage_on_totals' : percentage_locale,
            })
            
            return True
        return False
        
    def _calculate_percentage(self, item , effective_amount):
        
        percentage = locale.atof(item['amount']) * 100 / effective_amount
        percentage_locale = locale.format("%4.02f",percentage, grouping=True)
        
        if _debug:
            _logger.debug('Percentage : %s', percentage_locale)
        
        item.update({
            'percentage' : percentage_locale,
        })

    def lines(self, ids=None, done=None):
        obj_budget = self.pool.get('crossovered.budget')
        obj_budget_lines = self.pool.get('crossovered.budget.lines')
        obj_budget_post = self.pool.get('account.budget.post')
        
        obj_analytic = self.pool.get('account.analytic.account')
        obj_move_line = self.pool.get('account.move.line')
        obj_account = self.pool.get('account.account')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.budget.report')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['budget_ids'])
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        
        if report_data[0]['budget_ids']:
            budget_ids = report_data[0]['budget_ids']
        begin_period_id = report_date_begin[0]['begin_date'][0]
        end_period_id = report_date_end[0]['end_date'][0]
        
        begin_date = self._get_start_date_from_period(begin_period_id)
        end_date = self._get_end_date_from_period(end_period_id)
        
        budget_id = obj_budget.search(self.cr, self.uid, [('id','=',budget_ids[0])])
        budget = obj_budget.browse(self.cr, self.uid, budget_id)[0]
        
        budget_lines_ids = obj_budget_lines.search(self.cr, self.uid, [('crossovered_budget_id','=',budget.id)])
        
        #result dictionary
        result = {}
        
        for budget_lines in obj_budget_lines.browse(self.cr, self.uid, budget_lines_ids):
            data_dict = {}
            
            budget_post_ids = obj_budget_post.search(self.cr, self.uid, [('id','=', budget_lines.general_budget_id.id)])
            budget_post = obj_budget_post.browse(self.cr, self.uid, budget_post_ids)[0]   
            
            income_list = []
            expense_list = []
            first_for_account_inc = '1'
            first_for_account_exp = '1'
            effective_amount = 0.0
            
            res_ana = {
                'analytic' : '1',
                'first_for_account' : '0',
                'is_total' : '0',
                'is_final_total' : '0',
                'account' : False,
                'analytic_account' : budget_lines.analytic_account_id.name,
                'amount' : False,
                'percentace' : 0.0,
                'effective_amount' : 0.0
            }
            
            
            effective_income = 0.0
            effective_expense = 0.0
            for account in budget_post.account_ids:
                #if _debug:
                #    _logger.debug('Account id : %s', account)
                expense_amount = 0.0
                income_amount = 0.0
                percentage = 0.0
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','=', budget_lines.analytic_account_id.id),('date','>=',begin_date),('date','<=',end_date)]) 
                for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
                    income_amount = income_amount + move.credit
                    expense_amount = expense_amount + move.debit
                    
                
                #if _debug:
                #    _logger.debug('Effective amount : %f', effective_amount)
                    
                if account.user_type.code == 'income' and income_amount > 0.0:
                    income_amount_locale = locale.format("%20.02f",income_amount, grouping=True)
                    planned_amount_locale = locale.format("%20.02f",budget_lines.planned_amount, grouping=True)
                    
                    
                    res = {
                        'analytic' : '0',
                        'first_for_account' : first_for_account_inc,
                        'is_total' : '0',
                        'is_income' : '1',
                        'is_final_total' : '0',
                        'account' : account.code +" - "+account.name,
                        'analytic_account' : budget_lines.analytic_account_id.name,
                        'amount' : income_amount_locale,
                        'percentage' : percentage,
                        'planned_amount' : planned_amount_locale,
                        'effective_amount' : 0.0,
                    }   
        
                    first_for_account_inc = '0'
                    income_list.append(res)
                
                if account.user_type.code == 'expense' and expense_amount > 0.0:
                    expense_amount = -1*expense_amount
                    expense_amount_locale = locale.format("%20.02f",expense_amount, grouping=True)
                    planned_amount_locale = locale.format("%20.02f",budget_lines.planned_amount, grouping=True)
                    
                    res = {
                        'analytic' : '0',
                        'first_for_account' : first_for_account_exp,
                        'is_total' : '0',
                        'is_income' : '0',
                        'is_final_total' : '0',
                        'account' : account.code +" - "+account.name,
                        'analytic_account' : budget_lines.analytic_account_id.name,
                        'amount' : expense_amount_locale,
                        'percentage' : percentage,
                        'planned_amount' : planned_amount_locale,
                        'effective_amount' : 0.0,
                    }   
                
                    first_for_account_exp = '0'
                    expense_list.append(res)
                
           # if _debug:
           #     _logger.debug('Income list : %d', len(income_list))
           #     _logger.debug('Expense list : %d', len(expense_list))
            
                effective_income = income_amount + effective_income
                effective_expense = expense_amount + effective_expense
                
            planned_amount_locale = 0.0
            planned_amount_locale_exp = 0.0
            effective_amount_locale = 0.0
                       
            
            a_list = []
            if budget_lines.analytic_account_id.name in result:
                a_list = result[budget_lines.analytic_account_id.name]
            else:
                a_list.insert(0,res_ana)
            
            if len(income_list) > 0:
                self._update_with_totals(income_list, budget_lines.planned_amount, effective_income)
                for res in income_list:
                    self._calculate_percentage(res, effective_income)
                    a_list.insert(1,res)
                
            if len(expense_list) > 0:
                if _debug:
                    _logger.debug('AA : %s Expense : %f',budget_lines.analytic_account_id.name, effective_expense)
            
                self._update_with_totals(expense_list, budget_lines.planned_amount,effective_expense)
                for res in expense_list:
                    self._calculate_percentage(res, effective_expense)
                    a_list.append(res)
            
            result[budget_lines.analytic_account_id.name] = a_list
            
        
        for k_dept in result:
            for res in result[k_dept]:
                self.result_acc.append(res)
                
        return self.result_acc

report_sxw.report_sxw('report.isf.budget.webkit', 'isf.budget.report', 'addons/isf_budget_report/report/budget.mako', parser=isf_budget_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
