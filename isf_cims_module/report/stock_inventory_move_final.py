import time
from openerp.report import report_sxw

class stock_inventory_move_final(report_sxw.rml_parse):
    _inherit = 'stock.inventory.move.final'
    
    def __init__(self, cr, uid, name, context):
        super(stock_inventory_move_final, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
             'time': time,
             'total_price':self._price_total,
             'qty_total':self._qty_total,
        })

    
    def _price_total(self, objects):
        total = 0.0
        uom = objects[0].product_uom.name
        for obj in objects:
            total += obj.price
        return {'price':total}
        
    def _qty_total(self, objects):
        total = 0.0
        for obj in objects:
            factor = obj.product_uom.factor
            total += obj.product_qty / factor
        return {'quantity':total}


    
report_sxw.report_sxw(
    'report.isf.stock.inventory.move.final',
    'stock.inventory',
    'addons/isf_cims_module/report/stock_inventory_move_final.mako',
    parser=stock_inventory_move_final,
    header='internal'
)
