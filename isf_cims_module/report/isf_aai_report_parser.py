# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import platform

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

    
_logger = logging.getLogger(__name__)
_debug = False

class isf_aai_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.aai.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_aai_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'datelines' : self.datelines,
        })
        self.context = context
        self.result_acc = []
        self.result_date = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_aai_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    def datelines(self, ids=None, done=None):
        ctx = self.context.copy()
        isf_lib = self.pool.get('isf.lib.utils')
        obj_report = self.pool.get('isf.aai.analytic')
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        report_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['print_date'])
        print_date = report_date[0]['print_date']
        begin_date = report_date_begin[0]['begin_date']
        end_date = report_date_end[0]['end_date']
        currency = isf_lib.get_company_currency(self.cr, self.uid)
        
        res = {
            'begin_date' : begin_date,
            'end_date' : end_date,
            'print_date' : print_date,
            'currency' : currency.name,
        }
        
        self.result_date.append(res)
        
        return self.result_date
    
        
    def lines(self, ids=None, done=None):
        obj_analytic = self.pool.get('account.analytic.account')
        obj_move_line = self.pool.get('account.move.line')
        obj_account = self.pool.get('account.account')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.aai.analytic')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['analytic_account_ids'])
        report_date_begin = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['begin_date'])
        report_date_end = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['end_date'])
        report_account = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['account_ids'])
        
        target_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['entry_type'])
        target = target_obj[0]['entry_type']
        
        if report_data[0]['analytic_account_ids']:
            ids = report_data[0]['analytic_account_ids']
        begin_date = report_date_begin[0]['begin_date']
        end_date = report_date_end[0]['end_date']
        account_ids = report_account[0]['account_ids']
        other_ids = obj_analytic.search(self.cr, self.uid, [('id','not in',ids)])
        
        if _debug:
            _logger.debug('==> selected Accounts ids : %s', account_ids)
            _logger.debug('==> selected Analytic Accounts ids : %s', ids)
            _logger.debug('==> other Analytic Accounts ids : %s', other_ids)
        
        if not ids:
            ids = self.ids
        if not ids:
            return []
            
        total_of_total_amount = 0.0
        amount = 0.0
        overall_posted = True
        for account in obj_account.browse(self.cr, self.uid, account_ids):
            total_amount = 0.0
            first_for_account = '1'
            posted = True
            total_posted = True
            # first cycle over the Account to calculate the total amount
            if target == 'posted':
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('date','>=',begin_date),('date','<=',end_date),('move_id.state','=','posted')])
            else:
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('date','>=',begin_date),('date','<=',end_date)])     
            for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
                total_amount = total_amount + move.credit
                posted = posted and move.move_id.state == 'posted'
                total_posted = total_posted and posted
                overall_posted = overall_posted and posted
                
            if _debug:
                _logger.debug('==> Account : %s - %s TOTAL AMOUNT : %f', account.code, account.name, total_amount)    
                    
                    
            # second cycle over selected Analytic Accounts to write the Account's sub_res (with totals and percentage)
            for analytic_id in obj_analytic.browse(self.cr, self.uid, ids):
#                 if _debug:
#                     _logger.debug('==> Analytic ID : %s', analytic_id)
                if target == 'posted':
                    move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','=', analytic_id.id),('date','>=',begin_date),('date','<=',end_date),('move_id.state','=','posted')])
                else:
                    move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','=', analytic_id.id),('date','>=',begin_date),('date','<=',end_date)])
                amount = 0.0
                posted = True
#                 if _debug:
#                     _logger.debug('==> Move line ids : %s', move_line_ids)
                
                for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
                    amount = amount + move.credit
                    posted = posted and move.move_id.state == 'posted'
                    total_posted = total_posted and posted
                    overall_posted = overall_posted and posted
                    
                if _debug:
                    _logger.debug('==> Analytic : %s Amount : %f', analytic_id.name, amount)
                    
                percentage = 0.0
                percentage_str = '0.0'
                if total_amount > 0.0:
                    percentage = float(amount) / float(total_amount) * 100.0
                    percentage_str = "{0:.2f}".format(percentage)
                
                amount_locale = locale.format("%20.2f",amount, grouping=True)
                
                print_record = '0'
                if amount != 0.0:
                    print_record = '1'
                
                res = {
                    'is_total' : '0',
                    'is_final_total' : '0',
                    'first_for_account' : first_for_account,
                    'account' : account.code +" - "+account.name,
                    'analytic_account' : analytic_id.name,
                    'amount' : amount_locale,
                    'percentage' : percentage_str,
                    'print' : print_record,
                    'posted' : posted,
                }
                
                first_for_account = '0'
            
                self.result_acc.append(res)
                
            # third cycle over not selected Analytic Accounts (if any) as 'Others'
            if other_ids:
                if target == 'posted':
                    move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','in', other_ids),('date','>=',begin_date),('date','<=',end_date),('move_id.state','=','posted')])
                else:
                    move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','in', other_ids),('date','>=',begin_date),('date','<=',end_date)])
                amount = 0.0
                posted = True
                for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
                    amount = amount + move.credit
                    posted = posted and move.move_id.state == 'posted'
                    total_posted = total_posted and posted
                    overall_posted = overall_posted and posted
                
                    if _debug:
                        _logger.debug('==> Other Analytic : %s Amount : %f', move.analytic_account_id.name, amount)
                    
                percentage = 0.0
                percentage_str = '0.0'
                if total_amount > 0.0:
                    percentage = float(amount) / float(total_amount) * 100.0
                    percentage_str = "{0:.2f}".format(percentage)
                
                amount_locale = locale.format("%20.2f",amount, grouping=True)
                
                print_record = '0'
                if amount != 0.0:
                    print_record = '1'
                    
                res = {
                    'is_total' : '0',
                    'is_final_total' : '0',
                    'first_for_account' : '0',
                    'account' : False,
                    'analytic_account' : 'Others',
                    'amount' : amount_locale,
                    'percentage' : percentage_str,
                    'print' : print_record,
                    'posted' : posted,
                }
                
                self.result_acc.append(res)
            
            # then cycle over Moves without Analytic Accounts as 'Undefined'
            if target == 'posted':
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','=', False),('date','>=',begin_date),('date','<=',end_date),('move_id.state','=','posted')])
            else:
                move_line_ids = obj_move_line.search(self.cr, self.uid, [('account_id','=',account.id),('analytic_account_id','=', False),('date','>=',begin_date),('date','<=',end_date)])
            amount = 0.0
            posted = True
            for move in obj_move_line.browse(self.cr, self.uid, move_line_ids):
                amount = amount + move.credit
                posted = posted and move.move_id.state == 'posted'
                total_posted = total_posted and posted
                overall_posted = overall_posted and posted
            
                if _debug:
                    _logger.debug('==> Undefined Analytic amount : %f', amount)
                    
            percentage = 0.0
            percentage_str = '0.0'
            if total_amount > 0.0:
                percentage = float(amount) / float(total_amount) * 100.0
                percentage_str = "{0:.2f}".format(percentage)
            
            
            amount_locale = locale.format("%20.2f",amount, grouping=True)
            
            print_record = '0'
            if amount != 0.0:
                print_record = '1'
                
            res = {
                'is_total' : '0',
                'is_final_total' : '0',
                'first_for_account' : '0',
                'account' : False,
                'analytic_account' : 'Undefined',
                'amount' : amount_locale,
                'percentage' : percentage_str,
                'print' : print_record,
                'posted' : posted,
            }
            
            self.result_acc.append(res)
                
            # then show the Account's total
            total_amount_locale = locale.format("%20.2f",total_amount, grouping=True)
            res = {
                'is_total' : '1',
                'is_final_total' : '0',
                'first_for_account' : '0',
                'account' : False,
                'analytic_account' : False,
                'amount' : total_amount_locale,
                'percentage' : False,
                'posted' : total_posted,
            }
            
            self.result_acc.append(res)
            total_of_total_amount = total_of_total_amount + total_amount
            
        # finally, show the total of totals
        total_of_total_amount_locale = locale.format("%20.2f",total_of_total_amount, grouping=True)
        res = {
            'is_total' : '1',
            'is_final_total' : '1',
            'first_for_account' : '0',
            'account' : False,
            'analytic_account' : False,
            'amount' : total_of_total_amount_locale,
            'percentage' : False,
            'posted' : overall_posted,
        }
        self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.aai.webkit', 'isf.aai.analytic', 'addons/isf_aai_report/report/aai.mako', parser=isf_aai_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
