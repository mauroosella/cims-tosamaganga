# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_pharmacy_daily_income_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.pharmacy.credit.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_pharmacy_daily_income_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.pharmacy.daily.income.wizard')
        date_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_date[0]['date']
        
        res = {
            'date' : date,
        }
        self.headers_acc.append(res)
        
        
        return self.headers_acc
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.pharmacy.daily.income.wizard')
        date_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_date[0]['date']

        total_paid = total_foc = total_credit = total_internal = 0.0
        pharm_o = self.pool.get('isf.pharmacy.pos')
        pharm_ids = pharm_o.search(self.cr, self.uid, [('payment_date','=',date),('state','=','close')])
        for data in pharm_o.browse(self.cr, self.uid, pharm_ids):
            if data.type == 'internal':
                total_internal += data.total_amount
            if data.type == 'paid':
                total_paid += data.total_amount
            if data.type == 'credit':
                total_credit += data.total_amount
            if data.type == 'foc':
                total_foc += data.total_free

        res = {
            'is_total' : '0',
            'type' : 'Sales',
            'amount_free' : '0.0',
            'amount' : locale.format("%20d",total_paid, grouping=True),
        }
        self.result_acc.append(res)

        res = {
            'is_total' : '0',
            'type' : 'Internal',
            'amount_free' : '0.0',
            'amount' : locale.format("%20d",total_internal, grouping=True),
        }
        self.result_acc.append(res)

        res = {
            'is_total' : '0',
            'type' : 'Foc',
            'amount' : '0.0',
            'amount_free' : locale.format("%20d",total_foc, grouping=True),
        }
        self.result_acc.append(res)

        res = {
            'is_total' : '0',
            'type' : 'Credit',
            'amount_free' : '0.0',
            'amount' : locale.format("%20d",total_credit, grouping=True),
        }
        self.result_acc.append(res)

        res = {
            'is_total' : '1',
            'type' : 'Credit',
            'amount' : locale.format("%20d",total_credit+total_paid+total_internal, grouping=True),
            'amount_free' : total_foc,
        }
        self.result_acc.append(res)

        return self.result_acc
    

report_sxw.report_sxw('report.isf.pharmacy.daily.income.webkit', 'isf.pharmacy.daily.income.wizard', 'addons/isf_cims_module/report/pharm_income.mako', parser=isf_pharmacy_daily_income_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
