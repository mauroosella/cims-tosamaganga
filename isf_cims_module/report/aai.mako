<html>
<head>
    <style type="text/css">
        ${css}
        
        .posted {
			font-size: xx-small;
			text-align: right;
			color: black;
		}
		
		.unposted {
			font-size: xx-small;
			text-align: right;
			color: blue;
		}
		
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1><b><i>Incomes Divided by Departments</i></b></h1>
    Unposted in blue
    <br />
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print lines()
    """
    %>

    <table style="width: 100%" border="0">
	    %for date in datelines():
	        <tr class="table_header">
	            <td colspan="3">
	                Begin Period : ${date['begin_date']} End Period : ${date['end_date']}
	            </td>
	        </tr>
	        <tr class="table_header">
	            <td colspan="3">
	                Print Date : ${date['print_date']}
	            </td>
	        </tr>
	    %endfor
    </table>
	<br />
    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td> Analytic Account </td>
            <td> Amount </td>
            <td> Percentage </td> 
        </tr>
        <tr class="row">
            <td colspan="3">
                <table class="tr_bottom_line"></table>
            </td>
        </tr>
        %for account in lines():
            %if account['first_for_account'] == '1':
            <tr>
                <td colspan="3"><i>${account['account']}</i></td>
            </tr>
            <tr class="row">
                <td colspan="3">
                    <table class="tr_bottom_line"></table>
                </td>
            </tr>
            %endif
            %if account['is_total'] == '0':
                %if account['print'] == '1':
                    <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td class="label">${account['analytic_account']}</td>
                    %if account['posted']:
                    	<td class="posted">${account['amount']} ${date['currency']}</td>
                    	<td class="posted">${account['percentage']} &#37;</td>
                    %else:
                    	<td class="unposted">${account['amount']} ${date['currency']}</td>
                    	<td class="unposted">${account['percentage']} &#37;</td>
                    %endif
                    </tr>
                %endif
            %endif
            
            %if account['is_total'] == '1' and account['is_final_total'] == '0':
                <tr class="row">
                    <td colspan="3">
                        <table class="tr_bottom_line"></table>
                    </td>
                </tr>
                <tr class="row" style="border-top: 0.5px solid blue;">
                <td><b>Total</b></td>
                %if account['posted']:
                	 <td class="posted"><b>${account['amount']} ${date['currency']}</b></td>
                %else:
                	 <td class="unposted"><b>${account['amount']} ${date['currency']}</b></td>
                %endif
                <td/>
                </tr>
            %endif
            %if account['is_final_total'] == '1':
                <tr />
                <tr class="row">
                    <td colspan="3">
                        <table class="tr_bottom_line"></table>
                    </td>
                </tr>
                <tr class="row" style="border-top: 0.5px solid blue;">
                <td><b>Total of Incomes</b></td>
                %if account['posted']:
                	 <td class="posted"><b>${account['amount']} ${date['currency']}</b></td>
                %else:
                	 <td class="unposted"><b>${account['amount']} ${date['currency']}</b></td>
                %endif
                <td/>
                </tr>
            %endif
        %endfor         
    </table>
</body>
</html>
