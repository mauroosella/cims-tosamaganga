# -*- coding: utf-8 -*-

{
	'name': 'ISF CIMS',
	'version': '0.8.35',
	'category': 'Tools',
	'description': """

ISF CIMS 
========

Home Dashboard

* Simplified Home Dashboard to access easily to all the core functionality of CIMS
	
Accounting Module

* Add a new wizard to perform bank transfer
* Select a journal to perform the operations in Account/Configuration/Bank Transfer 
* Different interface to register Income and Expenses
* Simplified interface to register multiple income and multiple expenses
* Simplified interface to register customer and supplier invoices
* Checking the entries with the bank statement
* Custom Reports

Pharmacy Module

* Add a simplified interface to register item sold by pharmacy
* Manage serial number to trace product
* Manage expiring date on products
* Fully Integrated with warehouse management
* Add Pharmacy kit
* Manage Internal/Sold/Free of Charge/Discounted prescription
* Manage Stock Output for Expired/Loss/Broken Items
* Custom Reports
* Pharmacy Cash Transfer

Human Resource Module

* Manage incentives/advances on Payroll
* Manage class of contracts
* Add payslip massive confirm function
* Modified contracts to upload copy of the important documents
* Payroll Report
* Incentive Report

Stock Module

* Manage Internal move
* Manage Products donation
* Manage Stock Disposal 
* Stock Move Report
* Stock Donation Report
* Stock Disposal Report
* Stock Expired Report
* Stock Internal Use Report
* Stock Sales Summary Report
* Stock Item Wise Report
* Stock Detailed Transaction Report
* Manage batch for products
* Custom Reports

PoS Module

* Generic Point of Sale module, manager products, services or both
* Stock Card Management 
* Stock Disposal 
* PoS Cash Transfer

""",
	'author': 'Matteo Angelino (matteo.angelino@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': [
            'base',
            'account',
            'account_budget',
            'account_voucher',
            'account_cancel',
            'stock',
            'product_expiry',
#             'account_asset',
			'account_asset_management',
            'sale',
            'purchase',
            'hr',
            'hr_contract',
            'hr_payroll',
            'hr_payroll_account',
            'purchase',
            'stock',
            'stock_location',
            'isf_home_dashboard',
            'isf_loginmanager',
            'web_m2o_enhanced',
            'web_pdf_preview',
            'web_printscreen_zb',
            'product_fifo_lifo',
            'one2many_sorted',
            'product_tags',
            'account_unrealized_currency_gain_loss',
            'isf_account_wht',
    ],
	'data': [
        'security/isf_cims_security.xml',
        'utils/isf_lib_view.xml',
        # Views
        'views/isf_menu.xml',
        'views/isf_compound_journal_entry_layout_view.xml',
        'views/isf_bank_account_checking_view.xml',
        'views/res_config_view.xml',
        'views/isf_account_invoice_view.xml',
        'views/isf_account_view.xml',
        'views/isf_voucher_payment_receipt_view.xml',
        'views/isf_journal_entries_multiple_post_view.xml',
        'views/isf_stock_view.xml',
        'views/isf_pharmacy_pos_view.xml',
        'views/product_view.xml',
        'views/isf_pharmacy_kit_view.xml',
        'views/res_config_view.xml',
        'views/isf_stock_card_view.xml',
        'views/isf_stock_output_view.xml',
        'views/isf_pharmacy_pos_money_transfer_view.xml',
        'views/isf_hr_payroll_view.xml',
        'views/isf_hr_payroll_advances_view.xml',
        'views/isf_pos_view.xml',
        'views/isf_pos_stock_card_view.xml',
        'views/isf_pos_money_transfer_view.xml',
        'views/isf_purchase_order_view.xml',
        # Wizard
        'wizard/isf_money_transfer_view.xml',
        'wizard/stock_inventory_line_split_view.xml',
        'wizard/isf_purchase_without_invoice_view.xml',
        'wizard/isf_sale_without_invoice_view.xml',
        'wizard/isf_aai_report_view.xml',
        'wizard/isf_budget_report_view.xml',
        'wizard/isf_bs_report_view.xml',
        'wizard/isf_income_and_expense_report_view.xml',
        'wizard/isf_monthly_income_report_view.xml',
        'wizard/isf_payroll_report_view.xml',
        'wizard/isf_pharmacy_shift_report_view.xml',
        'wizard/isf_product_preparement_report_view.xml',
        'wizard/isf_stock_balance_report_view.xml',
        'wizard/isf_stock_invoice_onshipping_view.xml',
        'wizard/isf_stock_ledger_report_view.xml',
        'wizard/isf_stock_report_view.xml',
        'wizard/isf_stock_sale_summary_report_view.xml',
        'wizard/isf_store_wise_report_view.xml',
        'wizard/isf_stock_monthly_consumption_report.xml',
        'wizard/isf_product_report_view.xml',
        'wizard/isf_account_analytic_cost_ledger_view.xml',
        'wizard/isf_coaa_report_view.xml',
        'wizard/isf_coa_report_view.xml',
        'wizard/isf_incentive_report_view.xml',
        'wizard/isf_stock_expired_report_view.xml',
        'wizard/isf_stock_internal_report_view.xml',
        'wizard/isf_pharmacy_credit_report_view.xml',
        'wizard/isf_pharmacy_daily_income_view.xml',
        'wizard/isf_stock_item_wise_report_view.xml',
        'wizard/isf_stock_transaction_report_view.xml',
        'wizard/isf_pos_credit_report_view.xml',
        'wizard/isf_product_service_report_view.xml',
        'wizard/isf_store_reorder_report_view.xml',
        'wizard/isf_stock_change_product_qty_view.xml',
        #Report
        'report/stock_inventory_move.xml',
        'report/isf_account_report_general_ledger_view.xml',
        # Data
        'data/account_data.xml',
        'data/sequence/sequence.xml',
        'data/csv/isf.home.dashboard.action.group.csv',
        'data/csv/isf.home.dashboard.action.csv',
        #'data/csv/res.users.csv',
        'security/ir.model.access.csv',
    ],
    #'qweb': ['static/src/xml/logo.xml'],
	'css' : [
        'static/src/css/isf.css',
        'static/src/css/isf_base.css',
    ],
	'demo': [],
	'installable' : True,
}

