from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_account_report_iae(osv.osv_memory):
    _name = "isf.iae.analytic"
    _description = "Income and Expense Report Wizard"
    _columns = {
        'begin_date' : fields.date('Start of period',required=True),
        'end_date' : fields.date('End of period',required=True),
        'income_account_ids' : fields.many2many('account.account', 'isf_iae_income_rel', string="Incomes Accounts", select=True),
        'expense_account_ids' : fields.many2many('account.account', 'isf_iae_expense_rel', string="Expenses Accounts", select=True),
        'analytic_account_ids': fields.many2many('account.analytic.account', string="Analytic Accounts", select=True),
        'cash_bank_account_ids' : fields.many2many('account.account', 'isf_iae_cash_bank_account_rel', string="Cash/Bank Account", select=True),
        'report_type' : fields.selection([('summary','Summary'),('detailed','Detailed')],'Type'),
        'target' : fields.selection([('all','All Entries'),('posted','Only Posted')],'Posted / Unposted'),
        'selection' : fields.selection([('all','All Accounts'),('with_movements','With Movements')],'Accounts'),
        'no_filter' : fields.boolean('No Filters'),
    }
    
    def _get_current_date(self, cr, uid, context=None):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d")
    
    _defaults = {
        'begin_date' : _get_current_date,
        'end_date' : _get_current_date,
        'report_type' : 'detailed',
        'target' : 'all',
        'selection' : 'with_movements',
        'no_filter' : False,
    }
    
isf_account_report_iae()
