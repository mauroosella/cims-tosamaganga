from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_account_report_aai(osv.osv_memory):
    _name = "isf.aai.analytic"
    _description = "Account Analytic Income Report Wizard"
    _columns = {
        'begin_date' : fields.date('Start of period',required=True),
        'end_date' : fields.date('End of period',required=True),
        'entry_type' : fields.selection([('all','All Entries'),('posted','Only Posted')],'Target Moves'),
        'account_ids' : fields.many2many('account.account', string="Incomes Accounts", select=True),
        'analytic_account_ids': fields.many2many('account.analytic.account', string="Analytic Accounts", select=True),
        'print_date' : fields.date('Print Date'),
        #'account_expenses_ids' : fields.many2many('account.account', string="Expenses Accounts", select=True),
    }
    
    def _get_current_date(self, cr, uid, context=None):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d")
    
    _defaults = {
        'begin_date' : _get_current_date,
        'end_date' : _get_current_date,
        'entry_type' : 'posted',
        'print_date' : fields.date.context_today,
    }
    
isf_account_report_aai()
