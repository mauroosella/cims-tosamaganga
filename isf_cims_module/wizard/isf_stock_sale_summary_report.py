from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

def _get_stores(self, cr, uid, context=None):
    result = []

    result.append(('A00','ALL'))
    shop_o = self.pool.get('isf.shop')
    
    # Shops
    shop_ids = shop_o.search(cr, uid, [],order='name')
    for shop in shop_o.browse(cr, uid, shop_ids):
        result.append(('S'+str(shop.id),shop.name))        

    # Pharmacy
    pharm_o = self.pool.get('isf.pharmacy.pos')
    if pharm_o is not None:
        result.append(('L00','PHARMACY'))
    result.sort()
    return result


class isf_stock_sale_summary_report(osv.TransientModel):
    _name = "isf.stock.sale.summary.report"
    _description = "Stock Sale Summary Report"
    _columns = {
        #'store_id' : fields.many2one('stock.location','Store'),
        'store_id' : fields.selection(_get_stores,'Store'),
        'date_start' : fields.date('Date Start',required=True),
        'date_end' : fields.date('Date End',required=True),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_end' : fields.date.context_today,
    }
