from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime
import logging
# from serial.rfc2217 import ACTIVE

_logger = logging.getLogger(__name__)
_debug=False

class isf_account_report_bs(osv.osv_memory):
    _name = "isf.bs.bank.checking"
    _description = "Bank Checking Report Wizard"
    _columns = {
        'bank_statement_id' : fields.many2one('isf.bank.account.checking', "Bank Statement", required=True,select=True),
    }
    
    _defaults = {
        'bank_statement_id': lambda self, cr, uid, context: self._get_statement_id(cr, uid, context=context)
    }
    
    def _get_statement_id(self, cr, uid, context=None):
        active_ids = context['active_ids']
        if _debug:
            _logger.debug('==> bank_statement_id from context : %s', active_ids)
        if len(active_ids) == 1:
            return self.pool.get('isf.bank.account.checking').browse(cr, uid, active_ids[0], context).id
        return False
    
isf_account_report_bs()
