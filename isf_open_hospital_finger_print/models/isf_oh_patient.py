from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time
try:
    from py4j.java_gateway import JavaGateway
except:
    pass
import pytz
import re
import sys
import openerp

_debug=False
_logger = logging.getLogger(__name__)


class isf_oh_patient(osv.Model):
    _description = "ISF OH Patient Biometric Data"
    _inherit = "isf.oh.patient"
    
    
    _columns = {
        'finger_print_1' : fields.binary('Finger Print 1'),
        'finger_print_2' : fields.binary('Finger Print 2'),
        'finger_print_3' : fields.binary('Finger Print 3'),
        'finger_print_4' : fields.binary('Finger Print 4'),
        'finger_print_5' : fields.binary('Finger Print 5'),
    }
        
    def open_finger_print(self, cr, uid, ids, context=None):
        patient_id = context.get('patient_id')
        try:
            gateway = JavaGateway()
            gateway.entry_point.createAndShowGUI(patient_id,'capture')
        except:
            _logger.exception('')
            #exc_type, exc_value, exc_traceback = sys.exc_info()
            #raise openerp.exceptions.Warning(exc_value)
            raise openerp.exceptions.Warning(_('Finger Print seems not supported on your system'))
            return True
        return True