from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time
try:
    from py4j.java_gateway import JavaGateway
except:
    pass
import pytz
import re
import subprocess
import sys
import openerp

_debug=False
_logger = logging.getLogger(__name__)

class isf_pos(osv.Model):
    _inherit = 'isf.pos'

    def default_get(self, cr, uid, fields, context):
        if context is None:
            context = {}
        res = super(isf_pos, self).default_get(cr, uid, fields, context=context)
        
        if _debug:
            _logger.debug('Context : %s', context)
        
        partner_id = context.get('partner_id')
        if partner_id:
            res['partner_id'] = partner_id
        
        return res

    def open_finger_print_search(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        result = {'value' : {}}
        this = self.browse(cr, uid, ids)[0]
        pos_o = self.pool.get('isf.pos')

        try:
            gateway = JavaGateway()
            gateway.entry_point.createAndShowGUISearch('dummy','search')
        except:
            _logger.exception('')
            #exc_type, exc_value, exc_traceback = sys.exc_info()
            #raise openerp.exceptions.Warning(exc_value)
            raise openerp.exceptions.Warning(_('Finger Print seems not supported on your system'))
            return result

        
        if gateway.entry_point.patientFound():
            patient_id = gateway.entry_point.getPatientId()
            pat_o = self.pool.get('isf.oh.patient')
            if _debug:
                _logger.debug('Patient ID : %s',patient_id)

            pat_ids = pat_o.search(cr, uid, [('name','=',patient_id)])
            if len(pat_ids) > 0:
                pat = pat_o.browse(cr, uid, pat_ids)[0]
                
                if _debug:
                    _logger.debug('FP Search : %s, %s', patient_id,pat.id)
                mod_obj = self.pool.get('ir.model.data')
                res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital_finger_print', 'view_isf_pos_bio_form')   
                ctx = context.copy() 
                ctx.update({
                    'partner_id' : pat.partner_id.id,
                })
                result = {
                    'name': 'Patient Search',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': [res and res[1] or False],
                    'res_model': 'isf.pos',
                    'context': ctx,
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'current',
                    'res_id' : this.id,
                }   
                pos_o.write(cr,uid, [this.id],{'partner_id':pat.partner_id.id},context=context)
            return result
        return result