from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time
try:
    from py4j.java_gateway import JavaGateway
except:
    pass
import pytz
import re
import openerp.addons.web.http as openerpweb
import sys
import openerp

_debug=False
_logger = logging.getLogger(__name__)


class isf_oh_patient_search(osv.TransientModel):
    _inherit = 'isf.oh.patient.search'

    _columns = {
        'create_patient' : fields.boolean('Create Patient'),
    }

    _defaults = {
        'create_patient' : False,
    }


    def default_get(self, cr, uid, fields, context):
        if context is None:
            context = {}
        res = super(isf_oh_patient_search, self).default_get(cr, uid, fields, context=context)
        
        if _debug:
            _logger.debug('Context : %s', context)
        
        patient_id = context.get('patient_code')
        if patient_id:
            res['filter_patient_code'] = patient_id
        create_patient = context.get('create_patient')
        if create_patient:
            res['create_patient'] = True
        
        if _debug:
            _logger.debug('==> RES : %s',res)
        return res
    
    def open_finger_print_search(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        result = {'value' : {}}
        this = self.browse(cr, uid, ids)[0]
        try:
            gateway = JavaGateway()
            gateway.entry_point.createAndShowGUISearch('dummy','search')
        except:
            _logger.exception('')
            #exc_type, exc_value, exc_traceback = sys.exc_info()
            #raise openerp.exceptions.Warning(exc_value)
            raise openerp.exceptions.Warning(_('Finger Print seems not supported on your system'))
            return result

        if _debug:
            _logger.debug('Found : %s Create : %s',gateway.entry_point.patientFound(),gateway.entry_point.createPatient())
        
        if gateway.entry_point.patientFound():
            if _debug:
                _logger.debug('PATIENT FOUND : RETRIVE DATA')
            patient_id = gateway.entry_point.getPatientId()
            pat_o = self.pool.get('isf.oh.patient')
            if _debug:
                _logger.debug('Patient ID : %s',patient_id)

            pat_ids = pat_o.search(cr, uid, [('name','=',patient_id)])
            if len(pat_ids) > 0:
                pat = pat_o.browse(cr, uid, pat_ids)[0]
                
                if _debug:
                    _logger.debug('FP Search : %s, %s', patient_id,pat.id)
                mod_obj = self.pool.get('ir.model.data')
                res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital_finger_print', 'isf_oh_patient_search_bio_form')   
                ctx = context.copy() 
                ctx.update({
                    'patient_code' : patient_id,
                    'open_patient' : True,
                })
                result = {
                    'name': 'Patient Search',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': [res and res[1] or False],
                    'res_model': 'isf.oh.patient.search',
                    'context': ctx,
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    #'res_id': this.id, 
                }   
            return result
        elif gateway.entry_point.createPatient():
            if _debug:
                _logger.debug('====> 1')
            mod_obj = self.pool.get('ir.model.data')
            res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital_finger_print', 'isf_oh_patient_bio_form')   
            ctx = context.copy() 
            result = {
                'name': 'Patient Search',
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': [res and res[1] or False],
                'res_model': 'isf.oh.patient',
                'context': ctx,
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'current',
                #'res_id': this.id, 
            }   
            return result
        return result