# CIMS - Tosamaganga #

### What is this repository for? ###

* Set of modules for custom OpenERP v7.0 used for ISF/CUAMM in Tosamaganga Hospital (Tanzania)
* Custom OpenERP v7.0 on the related [repository](https://bitbucket.org/mwithi/openerp-7.0)

### How do I get set up? ###

#### Requirements ####

* Same requirements as for custom OpenERP v7.0 on related [repository](https://bitbucket.org/mwithi/openerp-7.0)

#### Istructions ####

* clone branch openeerp7_tosa on [this other](https://bitbucket.org/mwithi/openerp-7.0) repository and make it working
* fork this repo and clone the fork
* add cloned root folder in addons_path in openerp-server.conf
* restart openerp-server.py
* You can restore a Tosamaganga dump (if you have any) or start from an EMPTY DB (see [OpenERP Manual](https://bitbucket.org/mwithi/cims-tosamaganga/downloads/OpenERPManual.pdf)):
    * create new DB
    * go to Settings -> Update Module List
    * go to Settings -> Installed Module 
    * install isf_tosa_coa
    * install isf_tosa_data
    * install isf_tosa_*
    * ...


### Contribution guidelines ###

* Fork, develop & pull-request
* Double analyze the existing code, don't write what is already written, be DRY (Don't Repeat Yourself)
* Dumb code is better than clever one when is time to share
* Write comments when only "YOU" know what you are doing
* Optimize only after achieved
* Less is more!

### Who do I talk to? ###

* Repo owner or admin: [mwithi](https://github.com/mwithi)
* Other community or team contact: [ISF - Informatici Senza Frontiere ONLUS](http://www.informaticisenzafrontiere.org/)