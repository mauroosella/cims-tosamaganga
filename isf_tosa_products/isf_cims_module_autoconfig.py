from openerp.osv import osv
import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_cims_module_autoconfig(osv.osv_memory):
    _name = "isf.cims.module.autoconfig"

    def _set_general_config(self, cr, uid):
        _percentage_cost_price = 0.0
        _consumable_property_account_income_id = 4200001
        _consumable_property_account_expense_id = 5100007
        _product_property_account_income_id = 4200001
        _product_property_account_expense_id = 5100006
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.cims.config', 'percentage_cost_price',_percentage_cost_price)
        ir_values.set_default(cr, uid, 'isf.cims.config', 'consumable_property_account_income_id',self._get_account_id(cr, uid, _consumable_property_account_income_id))
        ir_values.set_default(cr, uid, 'isf.cims.config', 'consumable_property_account_expense_id',self._get_account_id(cr, uid, _consumable_property_account_expense_id))
        ir_values.set_default(cr, uid, 'isf.cims.config', 'product_property_account_income_id',self._get_account_id(cr, uid, _product_property_account_income_id))
        ir_values.set_default(cr, uid, 'isf.cims.config', 'product_property_account_expense_id',self._get_account_id(cr, uid, _product_property_account_expense_id))
    
    def _set_money_transfer(self, cr, uid):
        _money_transfer_journal_code = 'MTJ'
        _money_transfer_help = 'Registrer transfer of money from Cash to Bank, from Bank to Cash or from Bank to Bank, when already occurred with the bank receipt'
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.money.transfer', 'journal_id',self._get_account_journal_id_by_code(cr, uid, _money_transfer_journal_code))
        ir_values.set_default(cr, uid, 'isf.money.transfer', 'help',_money_transfer_help)
        
    def _set_hr_payroll(self, cr, uid):
        _payment_journal_code = 'PRPJ'
        _computing_journal_code = 'PRCJ'
        _advances_journal_code = 'SAPJ'
        _advances_account = 1300004
        _cash_bank_accounts = [1500001,1500002,1500003,1500004,1500005,1500006,1500007,1500008,1500009,1600001,1700001] 
        _account_moh_income = 4100002
        _account_moh = 1500002
        _account_gross = 5400001
        _account_moh_founds = 1500002
        _analytic_account_name = 'MALE WARD'
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'journal_id',self._get_account_journal_id_by_code(cr, uid, _payment_journal_code))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'moh_journal_id',self._get_account_journal_id_by_code(cr, uid, _computing_journal_code))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'advances_journal_id',self._get_account_journal_id_by_code(cr, uid, _advances_journal_code))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'advances_account_id',self._get_account_id(cr, uid, _advances_account))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'cash_bank_account_ids',self._get_account_ids(cr, uid, _cash_bank_accounts))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_moh_income_id',self._get_account_id(cr, uid, _account_moh_income))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_moh_id',self._get_account_id(cr, uid, _account_moh))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_gross_id',self._get_account_id(cr, uid, _account_gross))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'account_moh_founds_id',self._get_account_id(cr, uid, _account_moh_founds))
        ir_values.set_default(cr, uid, 'isf.hr.payroll', 'analytic_account_id',self._get_analytic_account_id_by_name(cr, uid, _analytic_account_name))
    
    def _set_pharmacy_pos(self, cr, uid):
        _cash_journal_code = 'CSH1'
        _credit_journal_code = 'SAJ'
        _stock_journal_code = 'STJ'
        _analytic_account_name = 'MAIN STORE'
        _stock_analytic_account_name = 'MAIN STORE'
        _main_stock_account = 1800001
        _free_of_charge_account = 5100008
        _cost_of_item_sold_account = 5100008
        _cost_of_item_expired_account = 5100008
        _cost_of_item_broken_account = 5100008
        _unexpected_income_account = 4200099
        _unexpected_expense_account = 5100008
        _internal_use_account = 5100008
        _income_account = 4200001
        _discount_account = 5100008
        _discount_product_name = 'Discount'
        _post_on_confirm = True
        _enable_stock = False
        _enable_lot_management = False
        _block_on_not_available = False
        _main_stock_location_name = 'MAIN STORE'
        _stock_location_name = 'SUBSTORE'
        _supplier_location_name = 'Suppliers'
        _output_location_name = 'Output'
        _expired_location_name = 'Scrapped'
        _broken_location_name = 'Scrapped'
        _product_category_id = 34 #By name may give multiple results
        _payment_accounts = [1500006]
        _preferred_method_account = 1500006
        _central_accounts = [1500006]
        _preferred_central_account = 1500006
        _help = 'Registers pharmacy prescription.'
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'discount_account_id',self._get_account_id(cr, uid, _discount_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'central_account_ids',self._get_account_ids(cr, uid, _central_accounts))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'unexpected_income_account_id',self._get_account_id(cr, uid, _unexpected_income_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'unexpected_expense_account_id',self._get_account_id(cr, uid, _unexpected_expense_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'preferred_central_account_id',self._get_account_id(cr, uid, _preferred_central_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'discount_product_id',self._get_product_id_by_name(cr, uid, _discount_product_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'journal_id',self._get_account_journal_id_by_code(cr, uid, _cash_journal_code))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'credit_journal_id',self._get_account_journal_id_by_code(cr, uid, _credit_journal_code))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'internal_account_id',self._get_account_id(cr, uid, _internal_use_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_journal_id',self._get_account_journal_id_by_code(cr, uid, _stock_journal_code))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_location_id',self._get_location_id_by_name(cr, uid, _stock_location_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'main_stock_location_id',self._get_location_id_by_name(cr, uid, _main_stock_location_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'output_location_id',self._get_location_id_by_name(cr, uid, _output_location_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'expired_location_id',self._get_location_id_by_name(cr, uid, _expired_location_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'broken_location_id',self._get_location_id_by_name(cr, uid, _broken_location_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'income_account_id',self._get_account_id(cr, uid, _income_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'analytic_account_id',self._get_analytic_account_id_by_name(cr, uid, _analytic_account_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_analytic_account_id',self._get_analytic_account_id_by_name(cr, uid, _stock_analytic_account_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'free_of_charge_account_id',self._get_account_id(cr, uid, _free_of_charge_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'cost_of_item_sold_account_id',self._get_account_id(cr, uid, _cost_of_item_sold_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'cost_of_item_expired_account_id',self._get_account_id(cr, uid, _cost_of_item_expired_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'cost_of_item_broken_account_id',self._get_account_id(cr, uid, _cost_of_item_broken_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'stock_account_id',self._get_account_id(cr, uid, _main_stock_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'post_on_confirm',_post_on_confirm)
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'enable_stock',_enable_stock)
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'enable_lot_management',_enable_lot_management)
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'block_on_not_available',_block_on_not_available)
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'payment_account_ids',self._get_account_ids(cr, uid, _payment_accounts))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'product_category_id',_product_category_id)
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'preferred_method',self._get_account_id(cr, uid, _preferred_method_account))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'supplier_location_id',self._get_location_id_by_name(cr, uid, _supplier_location_name))
        ir_values.set_default(cr, uid, 'isf.pharmacy.pos', 'help',_help)
            
    
    def _set_compound_journal_entry(self, cr, uid):
        _journal_sale_code = 'SAJ'
        _sale_cash_bank_accounts = [1500001,1500002,1500003,1500004,1500005,1500006,1500007,1500008,1500009,1600001,1700001] 
        _income_accounts = [4100001,4100002,4100003,4100004,4100005,4100006,4100007,4100008,4100009,4100010,4100011,4100012,4100013,4100014,4100015,4100016,4100017,4100018,4200001,4200002,4200003,4200004,4200005,4200006,4200007,4200008,4200009,4200010,4200011,4200012,4200013,4200099,4300001,4300002,4300003,4300004,4400001,4400002,4400003]
        _journal_purchase_code = 'EXJ'
        _purchase_cash_bank_accounts = [1500001,1500002,1500003,1500004,1500005,1500006,1500007,1500008,1500009,1600001,1700001] 
        _help_sale = 'Banks and Cash Collection accounts must be debited, Income and Payable accounts must be credited'
        _help_purchase = 'Expense and Receivable accounts must be debited, Banks and Cash Collection accounts must be credited'
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'journal_sale_id',self._get_account_journal_id_by_code(cr, uid, _journal_sale_code))
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'journal_purchase_id',self._get_account_journal_id_by_code(cr, uid, _journal_purchase_code))
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_cash_bank_account_ids',self._get_account_ids(cr, uid, _sale_cash_bank_accounts))
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'sale_account_ids',self._get_account_ids(cr, uid, _income_accounts))
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', 'purchase_cash_bank_account_ids',self._get_account_ids(cr, uid, _purchase_cash_bank_accounts))
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', '_help_sale',_help_sale)
        ir_values.set_default(cr, uid, 'isf.compound.journal.entry.layout', '_help_purchase',_help_purchase)
        
        
    def _set_pos(self,cr,uid):
        _cash_bank_accounts = [1500001,1500002,1500003,1500004,1500005,1500006,1500007,1500008,1500009,1600001,1700001] 
        _central_accounts = [1500006]
        _preferred_method_account = 1500006
        _dest_location_name = 'Output'
        _expired_location_name = 'Scrapped'
        _internal_location_name = 'Output'
        _unexpected_income_account = 4200099
        _unexpected_expense_account = 5100008
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.pos.config', 'cash_bank_account_ids',self._get_account_ids(cr, uid, _cash_bank_accounts))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'central_account_ids',self._get_account_ids(cr, uid, _central_accounts))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'preferred_method_id',self._get_account_id(cr, uid, _preferred_method_account))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'dest_location_id',self._get_location_id_by_name(cr, uid, _dest_location_name))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'expired_location_id',self._get_location_id_by_name(cr, uid, _expired_location_name))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'internal_location_id',self._get_location_id_by_name(cr, uid, _internal_location_name))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'unexpected_income_account_id',self._get_account_id(cr, uid, _unexpected_income_account))
        ir_values.set_default(cr, uid, 'isf.pos.config', 'unexpected_expense_account_id',self._get_account_id(cr, uid, _unexpected_expense_account))
    
        
    def _set_stock_move(self,cr,uid):
        _stock_journal_code = 'STJ'
        _account = 1800001
        _inventory_account = 1800001
        _donor_account = 4100014
        _expired_account = 5100008
        _loss_account = 5100008
        _supplier_location_name = 'Suppliers'
        _expired_location_name = 'Scrapped'
        _loss_location_name = 'Scrapped'
        _stock_enable = False
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.stock.move', 'journal_id',self._get_account_journal_id_by_code(cr, uid, _stock_journal_code))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'inventory_account_id',self._get_account_id(cr, uid, _account))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'account_id',self._get_account_id(cr, uid, _inventory_account))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'donor_account_id',self._get_account_id(cr, uid, _donor_account))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'expired_account_id',self._get_account_id(cr, uid, _expired_account))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'loss_account_id',self._get_account_id(cr, uid, _loss_account))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'supplier_location_id',self._get_location_id_by_name(cr, uid, _supplier_location_name))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'expired_location_id',self._get_location_id_by_name(cr, uid, _expired_location_name))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'loss_location_id',self._get_location_id_by_name(cr, uid, _loss_location_name))
        ir_values.set_default(cr, uid, 'isf.stock.move', 'enable_stock',_stock_enable)
        

    def _set_purchase_without_invoice(self,cr,uid):
        _journal_code = 'NIPJ'
        _cash_bank_accounts = [1500001,1500002,1500003,1500004,1500005,1500006,1500007,1500008,1500009,1600001,1700001] 
        _expense_accounts = [5400001,5400002,5400003,5400004,5400005,5400006,5400007,5400008,5400009,5400010,5400011,5400012,5400013,5400014,5400015,5400016,5400017,5400018,5400019,5400099,5400100,5400101,5500001,5500002,5500003,5500004,5500005,5600001,5600002,5600003,5600004,5600005,5700001,5700002,5700003,5700004,5700005,5700006,5700099,5800001,5800002,5800003,5800004,5800005,5800006,5800007,5800008,5800009,5800010,5800011,5800012,5800013,5800099,5900001,5900002,5900003,5900004,5900005,5900006,5900007,5900008,5900009,5900010,5900011,5900012,5900013,5900014,5900015,5900016,5900017,5900018,5900019,5900020,5900021,5900022,5900023,5900024,5900025,5900099,5900100,6000001,6000002,6000003,6000004,6000005,6000006,6100001,6100002,6100003,6100004,6100005,6100006,6200001,6200002,6200003,6200004,6200005,6200006,6200007,6200008,6200009,6200010,6200011,6300001,6300099]
        _analytic_accounts_codes = ['AA018','AA019','AA020','AA021','AA022','AA024','AA025','AA026','AA027','AA028','AA029','AA030','AA031','AA032','AA034','AA035','AA036','AA038','AA039','AA040','AA041','AA042','AA043','AA044','AA046','AA047','AA048','AA049','AA050','AA051','AA052','AA054','AA055','AA056','AA057']
        _help = 'Registers receipts and documents of direct expenses, without invoice from supplier. It can be used to record direct expenses like fuel purchases or bank charges. For multiple Analytic Account use the Multiple Expenses feature' 
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'journal_id',self._get_account_journal_id_by_code(cr, uid, _journal_code))
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'cash_bank_account_ids',self._get_account_ids(cr, uid, _cash_bank_accounts))
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'expense_account_ids',self._get_account_ids(cr, uid, _expense_accounts))
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'analytic_account_ids',self._get_analytic_account_ids_by_codes(cr, uid, _analytic_accounts_codes))
        ir_values.set_default(cr, uid, 'isf.purchase.without.invoice', 'help',_help)
        
        
    def _set_sale_without_invoice(self,cr,uid):
        _journal_code = 'NISJ'
        _cash_bank_accounts = [1500001,1500002,1500003,1500004,1500005,1500006,1500007,1500008,1500009,1600001,1700001] 
        _income_accounts = [4100001,4100002,4100003,4100004,4100005,4100006,4100007,4100008,4100009,4100010,4100011,4100012,4100013,4100014,4100015,4100016,4100017,4100018,4200001,4200002,4200003,4200004,4200005,4200006,4200007,4200008,4200009,4200010,4200011,4200012,4200013,4200099,4300001,4300002,4300003,4300004,4400001,4400002,4400003]
        _analytic_accounts_codes = ['AA003','AA004','AA005','AA006','AA007','AA008','AA058','AA010','AA011','AA012','AA013','AA014','AA059','AA015']
        _help = 'Registers receipts and documents of direct incomes, without invoicing by the hospital. It can be used to record receipts like direct sale of drugs, medical services or donations and rentals. For multiple Analytic Account use the Multiple Incomes feature'
        
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'journal_id',self._get_account_journal_id_by_code(cr, uid, _journal_code))
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'cash_bank_account_ids',self._get_account_ids(cr, uid, _cash_bank_accounts))
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'income_account_ids',self._get_account_ids(cr, uid, _income_accounts))
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'analytic_account_ids',self._get_analytic_account_ids_by_codes(cr, uid, _analytic_accounts_codes))
        ir_values.set_default(cr, uid, 'isf.sale.without.invoice', 'help',_help)
        
        
    def _create_shops(self,cr,uid):
        shops_obj = self.pool.get('isf.shop')
        shop_id = shops_obj.search(cr, uid, [('name', '=', 'SUBSTORE')])
        SUBSTORE = {
            'name' : 'SUBSTORE',
            'product_type' : 'product',
            'product_analytic_id' : self._get_analytic_account_id_by_name(cr, uid, 'SUBSTORE'),
            'location_id' : self._get_location_id_by_name(cr, uid, "SUBSTORE"),
            'product_category_id' : 34,
            'income_account_id' : self._get_account_id(cr, uid, 4200001),
            'account_foc_id' : self._get_account_id(cr, uid, 5100008),
            'account_int_id' : self._get_account_id(cr, uid, 5100008),
            'account_sold_id' : self._get_account_id(cr, uid, 5100008),
            'account_expired_id' : self._get_account_id(cr, uid, 5100008),
            'account_broken_id' : self._get_account_id(cr, uid, 5100008),
            'account_asset_id' : self._get_account_id(cr, uid, 1800001),
            'journal_id' : self._get_account_journal_id_by_code(cr, uid, 'SAJ'),
            'stock_journal_id' : self._get_account_journal_id_by_code(cr, uid, 'STJ'),
            'account_discount_id' : self._get_account_id(cr, uid, 5100008),
        }
        if not shop_id:
            shops_obj.create(cr, uid, SUBSTORE)
        else:
            shops_obj.write(cr, uid, shop_id[0], SUBSTORE)
            
        shop_id = shops_obj.search(cr, uid, [('name', '=', 'MALIPO')])
        MALIPO = {
            'name' : 'MALIPO',
            'product_type' : 'both',
            'service_analytic_id' : self._get_analytic_account_id_by_name(cr, uid, 'OUT PATIENT CARE'),
            'product_analytic_id' : self._get_analytic_account_id_by_name(cr, uid, 'DISPENSING'),
            'location_id' : self._get_location_id_by_name(cr, uid, "DISPENSING"),
            'service_category_id' : 78,
            'product_category_id' : 34,
            'income_account_id' : self._get_account_id(cr, uid, 4200001),
            'account_foc_id' : self._get_account_id(cr, uid, 5100008),
            'account_int_id' : self._get_account_id(cr, uid, 5100008),
            'account_sold_id' : self._get_account_id(cr, uid, 5100008),
            'account_expired_id' : self._get_account_id(cr, uid, 5100008),
            'account_broken_id' : self._get_account_id(cr, uid, 5100008),
            'account_asset_id' : self._get_account_id(cr, uid, 1800001),
            'journal_id' : self._get_account_journal_id_by_code(cr, uid, 'SAJ'),
            'stock_journal_id' : self._get_account_journal_id_by_code(cr, uid, 'STJ'),
            'account_discount_id' : self._get_account_id(cr, uid, 5100008),
        }
        if not shop_id:
            shops_obj.create(cr, uid, MALIPO)
        else:
            shops_obj.write(cr, uid, shop_id[0], MALIPO)

#       SHOP_EXAMPLE  = shops_obj.create(cr, uid,
#        {
#             'name' : fields.char('Name',required=True),
#             'product_type' : fields.selection([('product','Product'),('service','Service'),('both','Product/Service')],'Product Type'),
#             'service_analytic_id' : fields.many2one('account.analytic.account','Service Analytic Account'),
#             'product_analytic_id' : fields.many2one('account.analytic.account','Product Analytic Account'),
#             'location_id' : fields.many2one('stock.location','Location'),
#             'service_category_id' : fields.many2one('product.category','Service Category',required=False),
#             'product_category_id' : fields.many2one('product.category','Product Category',required=False),
#             'income_account_id' : fields.many2one('account.account','Income Account Id',required=True),
#             'account_foc_id' : fields.many2one('account.account','FoC Account Id',required=False),
#             'account_int_id' : fields.many2one('account.account','Internal Use Account Id',required=False),
#             'account_sold_id' : fields.many2one('account.account','Cost of Item Sold Account Id',required=False),
#             'account_expired_id' : fields.many2one('account.account','Cost of Item Expired Account Id',required=False),
#             'account_broken_id' : fields.many2one('account.account','Cost of Item Broken\Loss Account Id',required=False),
#             'account_asset_id' : fields.many2one('account.account','Asset Account'),
#             'journal_id' : fields.many2one('account.journal','Journal',required=True),
#             'stock_journal_id' : fields.many2one('account.journal','Stock Journal'),
#             'account_discount_id' : fields.many2one('account.account','Discount Account',required=True),
#         })
        
        
    def _get_account_id(self, cr, uid, account_number):
        account_obj = self.pool.get('account.account')
        account_id = account_obj.search(cr, uid, [('code','=',account_number)])
        if account_id:
            return account_id[0]
    
  
    def _get_account_ids(self, cr, uid, account_numbers):
        account_obj = self.pool.get('account.account')
        account_ids = []
        
        for account_number in account_numbers:
            account_id = account_obj.search(cr, uid, [('code','=',account_number)])
            if account_id:
                account_ids.append(account_id[0])
            
        return account_ids
        
    
    def _get_account_journal_id_by_code(self, cr, uid, journal_code):
        journal_obj = self.pool.get('account.journal')
        journal_id = journal_obj.search(cr, uid, [('code','=',journal_code)])
        if journal_id:
            return journal_id[0]
    
    
    def _get_analytic_account_id_by_name(self, cr, uid, analytic_account_name):
        analytic_account_obj = self.pool.get('account.analytic.account')
        analytic_account_id = analytic_account_obj.search(cr, uid, [('name','=',analytic_account_name)])
        if analytic_account_id:
            return analytic_account_id[0]
    
    
    def _get_analytic_account_ids_by_names(self, cr, uid, analytic_account_names):
        analytic_account_obj = self.pool.get('account.analytic.account')
        analytic_account_ids = []
        
        for analytic_account_name in analytic_account_names:
            analytic_account_id = analytic_account_obj.search(cr, uid, [('name','=',analytic_account_name)])
            if analytic_account_id:
                analytic_account_ids.append(analytic_account_id[0])
            
        return analytic_account_ids
    
    def _get_analytic_account_ids_by_codes(self, cr, uid, analytic_account_codes):
        analytic_account_obj = self.pool.get('account.analytic.account')
        analytic_account_ids = []
        
        for analytic_account_code in analytic_account_codes:
            analytic_account_id = analytic_account_obj.search(cr, uid, [('code','=',analytic_account_code)])
            if analytic_account_id:
                analytic_account_ids.append(analytic_account_id[0])
            
        return analytic_account_ids
    

    def _get_location_id_by_name(self, cr, uid, location_name):
        location_obj = self.pool.get('stock.location')
        location_id = location_obj.search(cr, uid, [('name','=',location_name)])
        if location_id:
            return location_id[0]
    
    
    def _get_product_id_by_name(self, cr, uid, product_name):
        product_obj = self.pool.get('product.product')
        product_id = product_obj.search(cr, uid, [('name_template','=',product_name)])
        if product_id:
            return product_id[0]
    
    
    def _get_product_category_id_by_name(self, cr, uid, product_category_name):
        product_category_obj = self.pool.get('product.category')
        product_category_id = product_category_obj.search(cr, uid, [('name','=',product_category_name)])
        if product_category_id:
            return product_category_id[0]
        
        
    def _auto_config(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        
        if _debug:
            _logger.info("########### CIMS AUTO-CONFIG ###########")
        
        self._set_general_config(cr, uid)
        self._set_money_transfer(cr, uid)
        self._set_hr_payroll(cr, uid)
        self._set_pharmacy_pos(cr, uid)
        self._set_compound_journal_entry(cr, uid)
        self._set_pos(cr, uid)
        self._set_stock_move(cr, uid)
        self._set_purchase_without_invoice(cr, uid)
        self._set_sale_without_invoice(cr, uid)
        self._create_shops(cr, uid)
        
            
isf_cims_module_autoconfig()

    