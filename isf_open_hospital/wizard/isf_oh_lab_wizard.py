from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
import openerp
import time

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_lab_data_result_value_wizard(osv.TransientModel):
    _name = 'isf.oh.lab.data.result.value.wizard'
    
    _columns = {
        'exam_id' : fields.many2one('isf.oh.exam','Exam'),
        'value' : fields.char('Value'),
        'line_id' : fields.many2one('isf.oh.lab.data.wizard',string="Data Id",select=True),
    }

class isf_oh_lab_data_wizard(osv.TransientModel):
    _name = 'isf.oh.lab.data.wizard'
    
    _columns = {
        'patient_id' : fields.many2one('isf.oh.patient','Patient'),
        'note' : fields.text('Note'),
        'exam_result_value_line_ids' : fields.one2many('isf.oh.lab.data.result.value.wizard','line_id',string="Line"),
    }
    
    def default_get(self, cr, uid, fields, context):
        if context is None:
            context = {}
        res = super(isf_oh_lab_data_wizard, self).default_get(cr, uid, fields, context=context)
        
        request_code = context.get('request_code')
        req_o = self.pool.get('isf.oh.exam.request')
        req_ids = req_o.search(cr, uid, [('request_id','=',request_code)])
        
        if _debug:
            _logger.debug('Context : %s', context)
            _logger.debug('REQ IDS : %s', req_ids)
            
        if len(req_ids) > 0:
            req = req_o.browse(cr, uid, req_ids)[0]
            res['patient_id'] = req.patient_id.id
            res['note'] = req.note
            
            for line in req.line_ids:
                if line.exam_id.result_type == 'value':
                    wiz_line_o = self.pool.get('isf.oh.lab.data.result.value.wizard')
                    line_id = wiz_line_o.create(cr, uid, {'exam_id' : line.exam_id.id},context=context)
        
        return res;

    
class isf_oh_lab_wizard(osv.TransientModel):
    _name = 'isf.oh.lab.wizard'
    
    _columns = {
        'state' : fields.selection([('1','Search'),('2','Data')],'Status',readonly=True),
        # Search Fields
        'name' : fields.char('Request Code'),
        
    }
    
    _defaults = {
        'state' : '1',
    }
    
    def next(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        wizard = self.browse(cr, uid, ids)[0]
        
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            #'res_id': record.id,
            'res_model': 'isf.oh.lab.data.wizard',
            'target': 'new',
            # save original model in context, because selecting the list of available
            # templates requires a model in context
            'context': {
                'default_model': 'isf.oh.lab.wizard',
                'ref' : False,
                'request_code' : wizard.name
            },}