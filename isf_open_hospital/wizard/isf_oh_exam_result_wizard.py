from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import datetime
import openerp
import time

_debug=False
_logger = logging.getLogger(__name__)

##############################################################################################################################################################################
##########################################################################  EXAM RESUL WIZARD ################################################################################
##############################################################################################################################################################################

class isf_oh_exam_result_wizard_line_simple(osv.TransientModel):
    _name = 'isf.oh.exam.result.wizard.line.simple'
    
    _columns = {
        'exam_id' : fields.many2one('isf.oh.exam','Exam'),
        'name' : fields.char('Exam Name'),
        'date' : fields.datetime('Date'),
        'value' : fields.char('Value'),
        'normal_range' : fields.char('Normal Range'),
        'statement_req_id' : fields.integer('Wiz Id'),
        'technician_id' : fields.many2one('hr.employee','Technician'),
        'available' : fields.boolean('Available'),
        'tech_ro' : fields.boolean('tech_ro'),
    }
    
    _defaults = {
        'date' : lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'available' : True,
        'tech_ro' : False,
    }



class isf_oh_exam_result_wizard_range_value(osv.TransientModel):
    _name = 'isf.oh.exam.result.wizard.range.value'

    _columns = {
        'name' : fields.char('Name'),
        'exam_id' : fields.many2one('isf.oh.exam','Exam Id'),
        'req_id' : fields.many2one('isf.oh.exam.request','Req Id'),
    }
    
class isf_oh_exam_result_wizard_line_range(osv.TransientModel):
    _name = 'isf.oh.exam.result.wizard.line.range'
    
    _columns = {
        'exam_id' : fields.many2one('isf.oh.exam','Exam'),
        'name' : fields.char('Exam Name'),
        'date' : fields.datetime('Date'),
        'result_id' : fields.many2one('isf.oh.exam.result.wizard.range.value','Result'),
        'value' : fields.char('Range Value'),
        'statement_req_id' : fields.integer('Req Id'),
        'technician_id' : fields.many2one('hr.employee','Technician'),
        'available' : fields.boolean('Available'),
        'tech_ro' : fields.boolean('tech_ro'),
    }
    
    _defaults = {
        'date' : lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'available' : True,
        'tech_ro' : False,
    }
    
    
class isf_oh_exam_result_wizard_line_multi(osv.TransientModel):
    _name = 'isf.oh.exam.result.wizard.line.multi'

    _columns = {
        'exam_id' : fields.many2one('isf.oh.exam','Exam'),
        'name' : fields.char('Test'),
        'date' : fields.datetime('Date'),
        'value' : fields.char('Value'),
        'normal_range' : fields.char('Normal Range'),
        'statement_req_id' : fields.integer('Req Id'),
        'technician_id' : fields.many2one('hr.employee','Technician'),
        'available' : fields.boolean('Available'),
        'tech_ro' : fields.boolean('tech_ro'),
    }
    
    _defaults = {
        'date' : lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'available' : True,
        'tech_ro' : False,
    }

class isf_oh_exam_result_wizard_line_multi_bool(osv.TransientModel):
    _name = 'isf.oh.exam.result.wizard.line.multi.bool'

    _columns = {
        'exam_id' : fields.many2one('isf.oh.exam','Exam'),
        'name' : fields.char('Test'),
        'date' : fields.datetime('Date'),
        'value' : fields.selection([('positive','Positive'),('negative','Negative')]),
        'statement_req_id' : fields.integer('Req Id'),
        'technician_id' : fields.many2one('hr.employee','Technician'),
        'available' : fields.boolean('Available'),
        'tech_ro' : fields.boolean('tech_ro'),
    }
    
    _defaults = {
        'date' : lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'available' : True,
        'tech_ro' : False,
    }
    
        
class isf_oh_exam_result_wizard(osv.TransientModel):
    _name = 'isf.oh.exam.result.wizard'
    
    def _get_lab_dept_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','lab_dept_id')
    
    _columns = {
        'dept_id' : fields.many2one('hr.department','Dept'),
        'request_id' : fields.many2one('isf.oh.exam.request','Request',required=True),
        'line_simple_ids' : fields.one2many('isf.oh.exam.result.wizard.line.simple','statement_req_id','Exam 1'),
        'line_range_ids' : fields.one2many('isf.oh.exam.result.wizard.line.range','statement_req_id','Exam 2'),
        'line_multi_ids' : fields.one2many('isf.oh.exam.result.wizard.line.multi','statement_req_id','Exam 3'),
        'line_multi_bool_ids' : fields.one2many('isf.oh.exam.result.wizard.line.multi.bool','statement_req_id','Exam 4'),
        'statement_id' : fields.integer('Statement'),
        'view_line_simple' : fields.boolean('View S'),
        'view_line_range' : fields.boolean('View R'),
        'view_line_multi' : fields.boolean('View M'),
        'view_line_multi_bool' : fields.boolean('View MB'),
        'job_id' : fields.many2one('hr.job','Job'),
       
    }
    
    _defaults = {
        'view_line_simple' : False,
        'view_line_range' : False,
        'view_line_multi' : False,
        'view_line_multi_bool' : False,
    }
    
    def get_default_job_type_lab(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','job_type_lab')
    
    def default_get(self, cr, uid, fields, context=None):
        request_id = context.get('request_id')
        res = super(isf_oh_exam_result_wizard, self).default_get(cr, uid, fields, context=context)
        res['dept_id'] = self._get_lab_dept_id(cr, uid)
        res['request_id'] = request_id
        res['statement_id'] = context.get('active_id')
        
        job_id = self.get_default_job_type_lab(cr, uid)
        res['job_id'] = job_id
        
        return res
    
    def onchange_request_id(self, cr, uid, ids, request_id, context=None):
        if context is None:
            context = {}
            
        result = {'value':{}}
        req_o = self.pool.get('isf.oh.exam.request')
        
        req_ids = req_o.search(cr, uid, [('id','=',request_id)])
        if not req_ids:
            return result
        req = req_o.browse(cr, uid, req_ids)[0]
        
        # Load exams with simple output
        line_simple_ids = self._load_exam_simple(cr, uid, req,context=context)
        if line_simple_ids:
            result['value'].update({
                'line_simple_ids' : line_simple_ids,
                'view_line_simple' : True,
            })
            
        # Load exams with multiple output (Range)
        line_range_ids = self._load_exam_range(cr, uid, req,context=context)
        if line_range_ids:
            result['value'].update({
                'line_range_ids' : line_range_ids,
                'view_line_range' : True,
            })

            
        # Load exams with multiple output (Multi)
        line_multi_ids = self._load_exam_multi(cr, uid, req,context=context)
        if line_multi_ids:
            result['value'].update({
                'line_multi_ids' : line_multi_ids,
                'view_line_multi' : True,
            })

        # Load exams with multiple output (Multi Bool)
        line_multi_bool_ids = self._load_exam_multi_bool(cr, uid, req,context=context)
        if line_multi_bool_ids:
            result['value'].update({
                'line_multi_bool_ids' : line_multi_bool_ids,
                'view_line_multi_bool' : True,
            })
        
        return result;
        
    def _load_exam_simple(self,cr, uid, request,context=None):
        statement_id = context.get('active_id')
        line_o = self.pool.get('isf.oh.exam.result.wizard.line.simple')
        lab_o = self.pool.get('isf.oh.lab.history')
        
        old_ids = line_o.search(cr, uid, [('statement_req_id','=',statement_id)])
        for old_line in line_o.browse(cr, uid, old_ids):
            line_o.unlink(cr, uid, old_line.id, context=context)
        
        tech_ro = False        
        line_ids = []
        for line in request.exams_id:
            if line.exam_id.result_type == 'value':
                res_ids = lab_o.search(cr, uid, [('request_id','=',request.id),('exam_id','=',line.exam_id.id)])
                result = False
                technician_id = False

                if len(res_ids) > 0:
                   res = lab_o.browse(cr, uid, res_ids)[0]
                   result = res.result
                   technician_id = res.technician_id.id
                
                #if not technician_id and uid != 1:
                #    job_id = self.get_default_job_type_lab(cr, uid)
                #    hr_o = self.pool.get('hr.employee')
                #    hr_ids = hr_o.search(cr, uid, [('user_id','=',uid),('job_id','=',job_id)])
                #    if len(hr_ids) > 0:
                #        emp = hr_o.browse(cr, uid, hr_ids)[0]
                #        technician_id = emp.id
                #        tech_ro = True
                line_id = line_o.create(cr ,uid, {
                    'exam_id' : line.exam_id.id,
                    'name' : line.exam_id.description,
                    'normal_range' : line.exam_id.normal_range,
                    'statement_req_id' : statement_id,
                    'value' : result,
                    'technician_id' : technician_id,
                    'tech_ro' : tech_ro
                })
                line_ids.append(line_id)
                
        return line_ids
        
    def _load_exam_range(self,cr, uid, request,context=None):
        statement_id = context.get('active_id')
        line_o = self.pool.get('isf.oh.exam.result.wizard.line.range')
        result_o = self.pool.get('isf.oh.exam.result.wizard.range.value')
        lab_o = self.pool.get('isf.oh.lab.history')
        range_o = self.pool.get('isf.oh.exam.result.range')
        

        old_ids = line_o.search(cr, uid, [('statement_req_id','=',statement_id)])
        for old_line in line_o.browse(cr, uid, old_ids):
            line_o.unlink(cr, uid, old_line.id, context=context)
        
        line_ids = []
        for line in request.exams_id:
            if line.exam_id.result_type == 'range':
                res_ids = lab_o.search(cr, uid, [('request_id','=',request.id),('exam_id','=',line.exam_id.id)])
                result = False
                technician_id = False
                res_id = False
                if len(res_ids) > 0:
                   res = lab_o.browse(cr, uid, res_ids)[0]
                   for result in line.exam_id.range_value:
                        if _debug:
                            _logger.debug('RESULT : %s | Value : %s',result.value, res.result)
                        if result.value == res.result:
                            res_id = result
                            break
                   technician_id = res.technician_id.id
                
                tech_ro = False
                #if not technician_id and uid != 1:
                #    job_id = self.get_default_job_type_lab(cr, uid)
                #    hr_o = self.pool.get('hr.employee')
                #    hr_ids = hr_o.search(cr, uid, [('user_id','=',uid),('job_id','=',job_id)])
                #    if len(hr_ids) > 0:
                #        emp = hr_o.browse(cr, uid, hr_ids)[0]
                #        technician_id = emp.id
                #        tech_ro = True



                result_id = False
                old_ids = result_o.search(cr, uid, [('exam_id','=',line.exam_id.id),('req_id','=',statement_id)])
                result_o.unlink(cr, uid, old_ids)
                for result in line.exam_id.range_value:
                    result_range_id = result_o.create(cr,uid, {
                        'name' : result.value,
                        'exam_id' : line.exam_id.id,
                        'req_id' : statement_id,
                        })

                    if res_id:
                        if result.value == res_id.value:
                            result_id = result_range_id

                line_id = line_o.create(cr ,uid, {
                    'exam_id' : line.exam_id.id,
                    'name' : line.exam_id.description,
                    'technician_id' : technician_id,
                    'statement_req_id' : statement_id,
                    'result_id' : result_id,
                })

                line_ids.append(line_id)
        
        return line_ids

    def _load_exam_multi_bool(self,cr, uid, request,context=None):
        statement_id = context.get('active_id')
        line_o = self.pool.get('isf.oh.exam.result.wizard.line.multi.bool')
        lab_o = self.pool.get('isf.oh.lab.history')
        
        old_ids = line_o.search(cr, uid, [('statement_req_id','=',statement_id)])
        for old_line in line_o.browse(cr, uid, old_ids):
            line_o.unlink(cr, uid, old_line.id, context=context)
        
        line_ids = []
        # Load Multi Bool
        for line in request.exams_id:
            if line.exam_id.result_type == 'multi2':
                for multi in line.exam_id.multi_bool_value:
                    res_ids = lab_o.search(cr, uid, [('request_id','=',request.id),('exam_id','=',line.exam_id.id),('test','=',multi.name)])
                    result = False
                    technician_id = False
                    if len(res_ids) > 0:
                        res = lab_o.browse(cr, uid, res_ids)[0]
                        result = res.result
                        technician_id = res.technician_id.id
                    
                    tech_ro = False
                    #if not technician_id and uid != 1:
                    #    job_id = self.get_default_job_type_lab(cr, uid)
                    #    hr_o = self.pool.get('hr.employee')
                    #    hr_ids = hr_o.search(cr, uid, [('user_id','=',uid),('job_id','=',job_id)])
                    #    if len(hr_ids) > 0:
                    #        emp = hr_o.browse(cr, uid, hr_ids)[0]
                    #        technician_id = emp.id
                    #        tech_ro = True
                   
                    line_id = line_o.create(cr ,uid, {
                        'exam_id' : line.exam_id.id,
                        'name' : multi.name,
                        'statement_req_id' : statement_id,
                        'value' : result,
                        'technician_id' : technician_id,
                    })
                    line_ids.append(line_id)
        return line_ids
        
    def _load_exam_multi(self,cr, uid, request,context=None):
        statement_id = context.get('active_id')
        line_o = self.pool.get('isf.oh.exam.result.wizard.line.multi')
        lab_o = self.pool.get('isf.oh.lab.history')
        
        old_ids = line_o.search(cr, uid, [('statement_req_id','=',statement_id)])
        for old_line in line_o.browse(cr, uid, old_ids):
            line_o.unlink(cr, uid, old_line.id, context=context)
        
        line_ids = []
        # Load Multi Value
        for line in request.exams_id:
            if line.exam_id.result_type == 'multi':
                for multi in line.exam_id.multi_value:
                    res_ids = lab_o.search(cr, uid, [('request_id','=',request.id),('exam_id','=',line.exam_id.id),('test','=',multi.name)])
                    result = False
                    technician_id = False
                    if len(res_ids) > 0:
                       res = lab_o.browse(cr, uid, res_ids)[0]
                       result = res.result
                       technician_id = res.technician_id.id
                    
                    tech_ro = False
                    #if not technician_id and uid != 1:
                    #    job_id = self.get_default_job_type_lab(cr, uid)
                    #    hr_o = self.pool.get('hr.employee')
                    #    hr_ids = hr_o.search(cr, uid, [('user_id','=',uid),('job_id','=',job_id)])
                    #    if len(hr_ids) > 0:
                    #        emp = hr_o.browse(cr, uid, hr_ids)[0]
                    #        technician_id = emp.id
                    #        tech_ro = True
                   
                    line_id = line_o.create(cr ,uid, {
                        'exam_id' : line.exam_id.id,
                        'name' : multi.name,
                        'normal_range' : multi.normal_value,
                        'statement_req_id' : statement_id,
                        'value' : result,
                        'technician_id' : technician_id,
                    })
                    line_ids.append(line_id)

        return line_ids
        
        
    def save(self, cr, uid, ids, context=None):
        wizard = self.browse(cr, uid, ids)[0]
        lab_h_o = self.pool.get('isf.oh.lab.history')
        exam_req_o = self.pool.get('isf.oh.exam.request')
        exam_req_line_o = self.pool.get('isf.oh.exam.request.line')
        line_simple_o = self.pool.get('isf.oh.exam.result.wizard.line.simple')
        line_range_o = self.pool.get('isf.oh.exam.result.wizard.line.range')
        line_multi_o = self.pool.get('isf.oh.exam.result.wizard.line.multi')
        line_multi_bool_o = self.pool.get('isf.oh.exam.result.wizard.line.multi.bool')
        req_id = wizard.request_id.id
        exam_req_line_ids = exam_req_line_o.search(cr, uid, [('statement_id','=',req_id)])
       
       
        # Save/Update result with simple value
        line_simple_ids = line_simple_o.search(cr, uid, [('statement_req_id','=',req_id)]) 
        for line in line_simple_o.browse(cr,uid,line_simple_ids):
            for exam_req in exam_req_line_o.browse(cr, uid, exam_req_line_ids):
                if line.available == False:
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'cancel'}, context=context)
                if line.exam_id.id == exam_req.exam_id.id and line.available == True:
                    lab_ids = lab_h_o.search(cr, uid, [('request_id','=',req_id),('exam_id','=',line.exam_id.id)])
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'done','technician_id':line.technician_id.id}, context=context)
                    
                    # Update request with result
                    exam_req_line_o.write(cr, uid, exam_req.id,{'result' : line.value}, context=context)

                    if len(lab_ids) > 0:
                        lab_h_o.write(cr, uid, lab_ids, {
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'result' : line.value,
                            'technician_id' : line.technician_id.id,
                        }, context=None)
                    else:
                        lab_h_o.create(cr, uid, {
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'result' : line.value,
                            'technician_id' : line.technician_id.id,
                        }, context=None)
                    
        # Save/Update range with simple value
        line_range_ids = line_range_o.search(cr, uid, [('statement_req_id','=',req_id)]) 
        for line in line_range_o.browse(cr,uid,line_range_ids):
            for exam_req in exam_req_line_o.browse(cr, uid, exam_req_line_ids):
                if line.available == False:
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'cancel'}, context=context)
                if line.exam_id.id == exam_req.exam_id.id and line.available == True:
                    lab_ids = lab_h_o.search(cr, uid, [('request_id','=',req_id),('exam_id','=',line.exam_id.id)])
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'done','technician_id':line.technician_id.id}, context=context)

                    # Update request with result
                    exam_req_line_o.write(cr, uid, exam_req.id,{'result' : line.result_id.name}, context=context)

                    if len(lab_ids) > 0:
                        lab_h_o.write(cr, uid, lab_ids, {
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'result' : line.result_id.name,
                            'technician_id' : line.technician_id.id,
                        }, context=None)
                    else:
                        lab_h_o.create(cr, uid, {
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'result' : line.result_id.name,
                            'technician_id' : line.technician_id.id,
                        }, context=None)
                    
        # Save/Update multi with simple value
        line_multi_ids = line_multi_o.search(cr, uid, [('statement_req_id','=',req_id)])
        for line in line_multi_o.browse(cr,uid,line_multi_ids):
            for exam_req in exam_req_line_o.browse(cr, uid, exam_req_line_ids):
                if line.available == False:
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'cancel'}, context=context)
                if line.exam_id.id == exam_req.exam_id.id and line.available == True:
                    lab_ids = lab_h_o.search(cr, uid, [('request_id','=',req_id),('exam_id','=',line.exam_id.id),('test','=',line.name)])
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'done','technician_id':line.technician_id.id}, context=context)

                    # Update request with result
                    exam_req_line_o.write(cr, uid, exam_req.id,{'result' : line.value}, context=context)

                    if len(lab_ids) > 0:
                         lab_h_o.write(cr, uid, lab_ids,{
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'test' : line.name,
                            'result' : line.value,
                            'technician_id' : line.technician_id.id,
                        }, context=None)
                    else:
                        lab_h_o.create(cr, uid, {
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'test' : line.name,
                            'result' : line.value,
                            'technician_id' : line.technician_id.id,
                        }, context=None)

        # Save/Update multi with bool value
        line_multi_bool_ids = line_multi_bool_o.search(cr, uid, [('statement_req_id','=',req_id)])
        for line in line_multi_bool_o.browse(cr,uid,line_multi_bool_ids):
            for exam_req in exam_req_line_o.browse(cr, uid, exam_req_line_ids):
                if line.available == False:
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'cancel'}, context=context)
                if line.exam_id.id == exam_req.exam_id.id and line.available == True:
                    lab_ids = lab_h_o.search(cr, uid, [('request_id','=',req_id),('exam_id','=',line.exam_id.id),('test','=',line.name)])
                    exam_req_line_o.write(cr, uid, exam_req.id, {'state' : 'done','technician_id':line.technician_id.id}, context=context)

                    # Update request with result
                    exam_req_line_o.write(cr, uid, exam_req.id,{'result' : line.value}, context=context)

                    if len(lab_ids) > 0:
                         lab_h_o.write(cr, uid, lab_ids,{
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'test' : line.name,
                            'result' : line.value,
                            'technician_id' : line.technician_id.id,
                        }, context=None)
                    else:
                        lab_h_o.create(cr, uid, {
                            'date' : line.date,
                            'patient_id' : wizard.request_id.patient_id.id,
                            'request_id' : req_id,
                            'exam_id' : line.exam_id.id,
                            'test' : line.name,
                            'result' : line.value,
                            'technician_id' : line.technician_id.id,
                        }, context=None)

            
        return True
            
                
        