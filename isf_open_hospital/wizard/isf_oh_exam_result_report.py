from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_oh_exam_result_wizard(osv.osv_memory):
    _name = "isf.oh.exam.result.wizard"
    _description = "Exam Result Report"
    
    _columns = {
        'exam_id' : fields.many2one('isf.oh.exam','Lab ID',required=True),
    }
    
    def default_get(self, cr, uid, fields, context):
        if context is None:
            context = {}
        res = super(isf_oh_exam_result_wizard, self).default_get(cr, uid, fields, context=context)
        
        exam_id = context.get('request_id')
        if exam_id:
            res['exam_id'] = exam_id
        
        return res
    
    
isf_oh_exam_result_wizard()
