from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time
import pytz
import re

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_patient_add_wizard(osv.TransientModel):
    _name = 'isf.oh.patient.add.wizard'
    
    _columns = {
        'name' : fields.char('Patient Name'),
    }