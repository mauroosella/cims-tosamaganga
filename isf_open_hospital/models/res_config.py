import time
import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from os.path import join as opj

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_config_settings(osv.TransientModel):
    _name = 'isf.oh.config.settings'
    _inherit = 'res.config.settings'
    
    _columns = {
        # General Tab
        'patient_start_number' : fields.integer('Start Number'),
        # Human Resources Tab
        'job_type_doctor' : fields.many2one('hr.job','Job Type (Doctor)'),
        'job_type_nurse' : fields.many2one('hr.job','Job Type (Nurse)'),
        'job_type_lab' : fields.many2one('hr.job','Job Type (Lab)'),
        #Accounting Tab
        'account_enabled' : fields.boolean('Enable Account'),
        'journal_id' : fields.many2one('account.journal','Journal'),
        'income_account_id' : fields.many2one('account.account','Income Account'),
        'cost_sold_acc_id' : fields.many2one('account.account','Cost of Item Sold'),
        'cost_foc_acc_id' : fields.many2one('account.account','Cost of Item FoC'),
        'cost_int_acc_id' : fields.many2one('account.account','Cost of Item Internal'),
        'cash_account_id' : fields.many2one('account.account','Cash Account'),
        # Laboratory Tab
        'lab_dept_id' : fields.many2one('hr.department','Lab Department'),
        'lab_product_category_id' : fields.many2one('product.category','Product Category'),
        'lab_aa_id' : fields.many2one('account.analytic.account','Analytic Account'),
        #OPD Tab
        'opd_start_number' : fields.integer('Start Number'),
        'opd_product_id' : fields.many2one('product.product','OPD Product'),
        'opd_aa_id' : fields.many2one('account.analytic.account','Analytic Account'),
        #Admission/Discharge Tab
        'open_bill_on_admission' : fields.boolean('Open Bill On Admission'),
        'admission_start_number' : fields.integer('Start Number'),
        'adm_product_id' : fields.many2one('product.product','Admission Product'),
    }
    
    _defaults = {
        'patient_start_number' : 0,
        'account_enabled' : False,
        'opd_start_number' : 0,
        'admission_start_number' : 0,
        'open_bill_on_admission' : False,
    }
    
    # Set method
    
    def set_patient_start_number(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'patient_start_number',config.patient_start_number)
        
        ir_sequence = self.pool.get('ir.sequence')
        ir_sequence_id = ir_sequence.search(cr, uid, [('name','=','Patient')])[0]
        ir_sequence.write(cr, uid, ir_sequence_id, {'number_next': config.patient_start_number,})

    
    def set_job_type_doctor(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'job_type_doctor',config.job_type_doctor.id)
    
    def set_job_type_nurse(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'job_type_nurse',config.job_type_nurse.id)
    
    def set_job_type_lab(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'job_type_lab',config.job_type_lab.id)
    
    def set_account_enabled(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'account_enabled',config.account_enabled)
        
    def set_lab_dept_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'lab_dept_id',config.lab_dept_id.id)
        
    def set_lab_product_category_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'lab_product_category_id',config.lab_product_category_id.id)

    def set_lab_aa_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'lab_aa_id',config.lab_aa_id.id)
        
    def set_income_account_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'income_account_id',config.income_account_id.id)

        
    def set_cash_account_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'cash_account_id',config.cash_account_id.id)
        
    def set_journal_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'journal_id',config.journal_id.id)
        
    def set_cost_sold_acc_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'cost_sold_acc_id',config.cost_sold_acc_id.id)
        
    def set_cost_foc_acc_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'cost_foc_acc_id',config.cost_foc_acc_id.id)
    
    def set_cost_int_acc_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'cost_int_acc_id',config.cost_int_acc_id.id)

    def set_opd_aa_id(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        if account_enabled:
            config = self.browse(cr, uid, ids[0], context)
            ir_values = self.pool.get('ir.values')
            ir_values.set_default(cr, uid, 'isf.oh.settings', 'opd_aa_id',config.opd_aa_id.id)
            
    def set_opd_start_number(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'opd_start_number',config.opd_start_number)
        
        ir_sequence = self.pool.get('ir.sequence')
        ir_sequence_id = ir_sequence.search(cr, uid, [('name','=','OPD')])[0]
        ir_sequence.write(cr, uid, ir_sequence_id, {'number_next': config.opd_start_number,})

    def set_opd_product_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'opd_product_id',config.opd_product_id.id)

    def set_open_bill_on_admission(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'open_bill_on_admission',config.open_bill_on_admission)
        
    def set_admission_start_number(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'admission_start_number',config.admission_start_number)
        
        ir_sequence = self.pool.get('ir.sequence')
        ir_sequence_id = ir_sequence.search(cr, uid, [('name','=','Admission')])[0]
        ir_sequence.write(cr, uid, ir_sequence_id, {'number_next': config.opd_start_number,})    

    def set_adm_product_id(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.oh.settings', 'adm_product_id',config.adm_product_id.id)


        
            
    # Get Method
    
    def get_default_patient_start_number(self, cr, uid, ids, context=None):
        ir_sequence = self.pool.get('ir.sequence')
        ir_sequence_id = ir_sequence.search(cr, uid, [('name','=','Patient')])[0]
        patient_start_number = ir_sequence.browse(cr, uid, ir_sequence_id).number_next_actual
        
        #patient_start_number = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','patient_start_number')
        return {'patient_start_number' : patient_start_number }
    
    def get_default_job_type_nurse(self, cr, uid, ids, context=None):
        job_type_nurse = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','job_type_nurse')
        return {'job_type_nurse' : job_type_nurse }
    
    def get_default_job_type_lab(self, cr, uid, ids, context=None):
        job_type_lab = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','job_type_lab')
        return {'job_type_lab' : job_type_lab }
    
    def get_default_job_type_doctor(self, cr, uid, ids, context=None):
        job_type_doctor = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','job_type_doctor')
        return {'job_type_doctor' : job_type_doctor }
    
    def get_default_account_enabled(self, cr, uid, ids, context=None):
        account_enabled = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
        return {'account_enabled' : account_enabled }
        
    def get_default_journal_id_id(self, cr, uid, ids, context=None):
        journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','journal_id')
        return {'journal_id' : journal_id }
    
    def get_default_lab_dept_id(self, cr, uid, ids, context=None):
        lab_dept_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','lab_dept_id')
        return {'lab_dept_id' : lab_dept_id }
        
    def get_default_lab_product_category_id(self, cr, uid, ids, context=None):
        lab_product_category_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','lab_product_category_id')
        return {'lab_product_category_id' : lab_product_category_id }

    def get_default_lab_aa_id(self, cr, uid, ids, context=None):
        lab_aa_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','lab_aa_id')
        return {'lab_aa_id' : lab_aa_id }
        
    def get_default_income_account_id(self, cr, uid, ids, context=None):
        income_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','income_account_id')
        return {'income_account_id' : income_account_id }
    
    def get_default_cash_account_id(self, cr, uid, ids, context=None):
        cash_account_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','cash_account_id')
        return {'cash_account_id' : cash_account_id }
        
    def get_default_cost_sold_acc_id(self, cr, uid, ids, context=None):
        cost_sold_acc_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','cost_sold_acc_id')
        return {'cost_sold_acc_id' : cost_sold_acc_id }
        
    def get_default_cost_foc_acc_id(self, cr, uid, ids, context=None):
        cost_foc_acc_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','cost_foc_acc_id')
        return {'cost_foc_acc_id' : cost_foc_acc_id }
        
    def get_default_cost_int_acc_id(self, cr, uid, ids, context=None):
        cost_int_acc_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','cost_int_acc_id')
        return {'cost_int_acc_id' : cost_int_acc_id }
        
    def get_default_opd_start_number(self, cr, uid, ids, context=None):
        ir_sequence = self.pool.get('ir.sequence')
        ir_sequence_id = ir_sequence.search(cr, uid, [('name','=','OPD')])[0]
        opd_start_number = ir_sequence.browse(cr, uid, ir_sequence_id).number_next_actual
        
        #opd_start_number = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_start_number')
        return {'opd_start_number' : opd_start_number }

    def get_default_opd_product_id(self, cr, uid, ids, context=None):
        opd_product_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_product_id')
        return {'opd_product_id' : opd_product_id }

    def get_default_opd_aa_id(self, cr, uid, ids, context=None):
        opd_aa_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_aa_id')
        return {'opd_aa_id' : opd_aa_id }

    def get_default_open_bill_on_admission(self, cr, uid, ids, context=None):
        open_bill_on_admission = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','open_bill_on_admission')
        return {'open_bill_on_admission' : open_bill_on_admission }
    
    def get_default_admission_start_number(self, cr, uid, ids, context=None):
        ir_sequence = self.pool.get('ir.sequence')
        ir_sequence_id = ir_sequence.search(cr, uid, [('name','=','Admission')])[0]
        admission_start_number = ir_sequence.browse(cr, uid, ir_sequence_id).number_next_actual
        
        #admission_start_number = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','admission_start_number')
        return {'admission_start_number' : admission_start_number }
    
    def get_default_adm_product_id(self, cr, uid, ids, context=None):
        adm_product_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','adm_product_id')
        return {'adm_product_id' : adm_product_id }


        