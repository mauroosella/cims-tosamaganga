from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_vaccine_category(osv.Model):
    _name = 'isf.oh.vaccine.category'
    
    _columns = {
        'name' : fields.char('Category',size=64, required=True),
    }
    

class isf_oh_vaccine(osv.Model):
    _name = 'isf.oh.vaccine'
    
    _columns = {
        'name' : fields.char('Code',size=16,required=True),
        'description' : fields.char('Description',size=128, required=False),
        'category' : fields.many2one('isf.oh.vaccine.category','Category',required=True),
    }

class isf_oh_patient_vaccine(osv.Model):
    _name = 'isf.oh.patient.vaccine'
    
    _columns = {
        'date' : fields.date('Date',required=True),
        'patient_id' : fields.many2one('isf.oh.patient','Patient',required=True),
        'patient_name' : fields.related('patient_id','partner_id','name',type='char',relation='isf.oh.patient',readonly=True,store=True,string='Name'),
        'patient_age' : fields.related('patient_id','age',type='char',relation='isf.oh.patient',readonly=True,store=True,string='Age'),
        'patient_sex' : fields.related('patient_id','sex',type='char',relation='isf.oh.patient',readonly=True,store=True,string='Sex'),
        'vaccine_type_id' : fields.many2one('isf.oh.vaccine.category','Vaccine Type',required=True),
        'vaccine_id' : fields.many2one('isf.oh.vaccine','Vaccine',required=True),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
    }
    
    def onchange_patient(self, cr, uid, ids, patient_id, context=None):
        result = {'value' : {}}
        
        pat_o = self.pool.get('isf.oh.patient')
        pat_ids = pat_o.search(cr, uid, [('id','=',patient_id)])
        patient = pat_o.browse(cr, uid, pat_ids)[0]
        
        result['value'].update({
            'patient_name' : patient.partner_id.name,
            'patient_age' : patient.age,
            'patient_sex' : patient.sex,
        })
        
        return result