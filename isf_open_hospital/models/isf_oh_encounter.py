from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import time
from datetime import datetime
import openerp

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_encounter_item(osv.Model):
	_name = 'isf.oh.encounter.item'

	_columns = {
		'name' : fields.char('Name',size=64),
		'encounter_id' : fields.many2one('isf.oh.encounter','Encounter',select=True),
	}

class isf_oh_encounter(osv.Model):
	_name = 'isf.oh.encounter'

	_rec_name = 'source'

	_columns = {
		'source' : fields.char('Encounter',size=64),
		'invoice_id' : fields.many2one('account.invoice','Invoice'),
		'state' : fields.selection([('open','Open'),('close','Close')]),
		'item_list' : fields.one2many('isf.oh.encounter.item','encounter_id','Items'),
	}

	_defaults = {
		'state' : 'open',
	}

	def find(self, cr, uid,source):
		enc_ids = self.search(cr, uid, [('state','=','open'),('source','=',source)],limit=1)
		for enc in self.browse(cr, uid, enc_ids):
			return enc.id
		return None

	def add(self, cr, uid, encounter_id, item):
		item_o = self.pool.get('isf.oh.encounter.item')
		item_o.create(cr, uid, {
			'name' : item,
			'encounter_id' : encounter_id,
			})

	def close(self, cr, uid):
		self.write(cr, uid, {'state' : 'close'})
