from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv

_debug=False
_logger = logging.getLogger(__name__)


class isf_oh_bed(osv.Model):
    _name = 'isf.oh.bed'
    
    _order = 'ward,number asc'
    
    #_rec_name = 'number'
    
    _columns = {
        'number' : fields.char('Bed No.'),
        'have_tv' : fields.boolean('TV'),
        'phone_number' : fields.char('Phone'),
        'ward' : fields.many2one('isf.oh.ward','Ward'),
        'state' : fields.selection([('free','Free'),('occupied','Occupied')],'State'),
    }
    
    _defaults = {
        'state' : 'free',
        'have_tv' : False,
    }

    def name_get(self, cr, uid, ids, context=None):
        
        res = []
        
        for i in ids:
            d = self.browse(cr, uid, i)
            if d.ward:
                name = '['+d.ward.prefix+'] '+str(d.number)
                res.append((d.id,name))
        
        return res
    

class isf_oh_ward(osv.Model):
    _name = 'isf.oh.ward'
    _description = 'Hospital Ward'
    
    _columns = {
        'name' : fields.char('Ward',size=64,required=True),
        'prefix' : fields.char('Prefix',size=64,required=True),
        'hr_department_id' : fields.many2one('hr.department','Department'),
        'phone' : fields.char('Phone Number'),
        'fax' : fields.char('Fax Number'),
        'email' : fields.char('E-Mail'),
        'beds' : fields.integer('Beds'),
        'gender' : fields.selection([('m','Male Ward'),('f','Female Ward'),('unisex','Unisex')],'Gender'),
        'doctors' : fields.integer('Doctors'),
        'nurses' : fields.integer('Nurses'),
        'min_age' : fields.integer('Minimum Age (months)'),
        'max_age' : fields.integer('Maximum Age (months)'),
        'bed_ids' : fields.one2many('isf.oh.bed','ward','Beds'),
        'aa_id' : fields.many2one('account.analytic.account','Analytic Account'),
    }
    
    _defaults = {
        'gender' : 'unisex',
        'min_age' : 0,
        'max_age' : 0,
    }

    def create_beds(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        bed_o = self.pool.get('isf.oh.bed')
        data = self.browse(cr, uid, ids)[0]
        bed_ids = []
        for num in range(1,data.beds):
            bed_ids = bed_o.search(cr, uid, [('number','=',num),('ward','=',data.id)])
            if len(bed_ids) == 0:
                bed_id = bed_o.create(cr, uid, {
                        'number' : num,
                        'ward' : data.id,
                        'state' : 'free',
                    },context=context)
                bed_ids.append(bed_id)

        return True

class isf_oh_clinic(osv.Model):
    _name = 'isf.oh.clinic'
    
    _columns = {
        'name' : fields.char('Name'),
        'prefix' : fields.char('Prefix'),
        'ward_id' : fields.many2one('isf.oh.ward','Ward'),
        'min_age' : fields.integer('Minimum Age (month)'),
        'max_age' : fields.integer('Maximum Age (month)'),
        'gender' : fields.selection([('m','Male Clinic'),('f','Female Clinic'),('unisex','Unisex')],'Gender'),
    }

    _defaults = {
        'gender' : 'unisex',
        'min_age' : 0,
        'max_age' : 0,
    }