from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
import openerp
import time

_debug=False
_logger = logging.getLogger(__name__)

##############################################################################################################################################################################
###########################################################################  EXAM UOM #######################################################################################
##############################################################################################################################################################################

class isf_oh_uom(osv.Model):
    _name = 'isf.oh.uom'
    
    _columns = {
        'name' : fields.char('Uom',required=True),
        'full_name' : fields.char('Name',required=True),
        'description' : fields.text('Description',required=False),
    }

##############################################################################################################################################################################
###########################################################################  EXAM TYPE #######################################################################################
##############################################################################################################################################################################


class isf_oh_exam_type(osv.Model):
    _name = 'isf.oh.exam.type'
    
    _columns = {
        'name' : fields.char('Type',required=True),
    }

##############################################################################################################################################################################
######################################################################  EXAM RESULT RANGE LINE ################################################################################
##############################################################################################################################################################################
   

class isf_oh_exam_result_range(osv.Model):
    _name = 'isf.oh.exam.result.range'
    _rec_name = 'value'
    
    _columns = {
        'value' : fields.char('Value'),
        'default' : fields.boolean('Default'),
    }
        
    
##############################################################################################################################################################################
######################################################################  EXAM RESULT MULTI ####################################################################################
##############################################################################################################################################################################
class isf_oh_exam_result_multi(osv.Model):
    _name = 'isf.oh.exam.result.multi'
    
    _columns = {
        'name' : fields.char('Name'),
        'normal_value' : fields.char('Normal Value'),
        'uom' : fields.many2one('isf.oh.uom','UoM'),
    }

##############################################################################################################################################################################
######################################################################  EXAM RESULT MULTI ####################################################################################
##############################################################################################################################################################################
class isf_oh_exam_result_multi_bool(osv.Model):
    _name = 'isf.oh.exam.result.multi.bool'
    
    _columns = {
        'name' : fields.char('Name'),
        'result' : fields.selection([('positive','POSITIVE'),('negative','NEGATIVE')],'Result')
    }

    
##############################################################################################################################################################################
##########################################################################  EXAM #############################################################################################
##############################################################################################################################################################################


class isf_oh_exam(osv.Model):
    _name = 'isf.oh.exam'
    _inherits = {
        'product.product' : 'product_id'
    }
    _rec_name = 'description'
    
    def _calculate_normal_range(self, cr, uid, ids, fields_name, arg, context=None):
        res = {}
        for i in ids:
            data = self.browse(cr, uid, i)
            res[i] = False
            if data.result_type == 'value':
                if data.min_value >= 0.0 and data.max_value >= 0.0:
                    res[i] =  str(data.min_value) + '-' + str(data.max_value)
            
                if data.min_value < 0.0 and data.max_value > 0.0:
                     res[i] = 'Up to ' + str(data.max_value)
            
                if data.min_value > 0.0 and data.max_value < 0.0:
                    res[i] =  '> ' + str(data.max_value)
            elif data.result_type == 'range':
                for v in data.range_value:
                    if v.default:
                        res[i] = v.value
                        
            if not res[i]:
                res[i] = '-'
       
        return res
    
    _columns = {
        'product_id': fields.many2one('product.product', required=True,string='Related Product', ondelete='cascade',help='Product-related data of the exam'),
        'description' : fields.char('Description',required=True),
        'code' : fields.char('Code',required=False),
        'exam_type' : fields.many2one('isf.oh.exam.type','Type', required=True),
        'result_type' : fields.selection([('value','Free Value'),('range','Range Value'),('multi','Multi Value'),('multi2','Multi Bool Value')],required=True),
        'value' : fields.char('Value'),
        'range_value' : fields.many2many('isf.oh.exam.result.range','isf_oh_exam_range_rel','exam_id','res_id','Range Value'),
        'multi_value' : fields.many2many('isf.oh.exam.result.multi','isf_oh_exam_multi_rel','exam_id','res_id','Multi Value'),
        'multi_bool_value' : fields.many2many('isf.oh.exam.result.multi.bool','isf_oh_exam_multi_bool_rel','exam_id','res_id','Multi Value'),
        'exams' : fields.boolean('Exams',required=False),
        'uom_id' : fields.many2one('isf.oh.uom','Unit Of Measure',required=False),
        'normal_range' : fields.function(_calculate_normal_range,type="string",string='Normal Range', required=False),
        'min_value' : fields.float('Min Value'),
        'max_value' : fields.float('Max Value'),
        'ref' : fields.char('Reference'),
    }
    
    _defaults = {
        'exams' : True,
        'result_type' : 'value',
        'min_value' : -1.0,
        'max_value' : -1.0,
    }
    
    def create(self, cr, uid, vals, context=None):
        vals['name'] = vals['description']
        vals['type'] = 'service'
        return super(isf_oh_exam, self).create(cr, uid, vals, context=context)