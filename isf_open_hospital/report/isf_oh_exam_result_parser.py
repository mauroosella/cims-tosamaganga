# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_exam_result_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.oh.exam.result.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_oh_exam_result_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'types' : self.types,
            'lines': self.lines,
            'headerlines' : self.headerlines,
            'microscope' : self.microscope,
            'cultures' : self.cultures,
        })
        self.context = context
        self.result_acc = []
        self.result_header = []
        self.result_types = []
        self.result_micro = []
        self.result_cultures = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_bs_object(self, ids):
        bs_obj = self.pool.get('isf.bank.account.checking')
        
        _logger.debug('_get_bs_object : %s',ids)
        
        bs = bs_obj.browse(self.cr, self.uid, ids)
        
        return bs
    
    def types(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.exam.result.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['exam_id'])
        request_id = report_data[0]['exam_id'][0]
        lab_obj = self.pool.get('isf.oh.lab')
        lab_ids = lab_obj.search(self.cr, self.uid, [('request_id','=',request_id)])
        lab = lab_obj.browse(self.cr, self.uid, lab_ids)[0]
        
        cultures = '0',
        if lab.have_cultures:
            cultures = '1'
    
        microscope = '0'
        if lab.need_micro:
            microscope = '1'
        
        res = {
            'result' : '1',
            'cultures' : cultures,
            'microscope' : microscope,
        }
        
        if  len(self.result_types) == 0:
            self.result_types.append(res)
        
        return self.result_types
    
    def headerlines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.exam.result.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['exam_id'])
        request_id = report_data[0]['exam_id'][0]
        lab_obj = self.pool.get('isf.oh.lab')
        lab_ids = lab_obj.search(self.cr, self.uid, [('request_id','=',request_id)])
        lab = lab_obj.browse(self.cr, self.uid, lab_ids)[0]
        
        obj_report = self.pool.get('isf.oh.patient')
        hr_dep_obj = self.pool.get('hr.department')
        
        res = {
            'patient_name' : lab.patient_id.name,
            'patient_number' : lab.patient_id.id_number,
            'sex' : lab.patient_id.sex,
            'age' : lab.patient_id.age,
            'date' : lab.date,
            'request_id' : lab.request_id.request_id,
        }
        
        if len(self.result_header) == 0:
            self.result_header.append(res)
        
        return self.result_header

    def cultures(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.exam.result.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['exam_id'])
        request_id = report_data[0]['exam_id'][0]
        lab_obj = self.pool.get('isf.oh.lab')
        lab_ids = lab_obj.search(self.cr, self.uid, [('request_id','=',request_id)])
        lab = lab_obj.browse(self.cr, self.uid, lab_ids)[0]
        
        
        sensitive_drugs = ''
        intermediate_drugs = ''
        resistant_drugs = ''
        specimen = '-'
        collection_date = '-'
        appaerance = '-'
        turbidity = '-'
        direct = '-'
        gram_stain = '-'
        other_stain = '-'
        
        if lab.specimen_id:
            specimen = lab.specimen_id.name
        
        if lab.collection_date:
            collection_date = lab.collection_date
        
        if lab.appaerance_id.name:
            appaerance = lab.appaerance_id.name
            
        if lab.turbidity_id.name:
            turbidity = lab.turbidity_id.name
        
        sens_drugs = []
        for prod in lab.sensitive_drugs:
            sens_drugs.append(prod.name_template)
            
        int_drugs = []
        for prod in lab.intermediate_drugs:
            int_drugs.append(prod.name_template)
        
        res_drugs = []
        for prod in lab.resistant_drugs:
            res_drugs.append(prod.name_template)
            
        if lab.direct:
            direct = lab.direct
            
        if lab.gram_stain:
            gram_stain = lab.gram_stain
            
        if lab.other_stain:
            other_stain = lab.other_stain
        
        if lab.have_cultures:
            res = {
                'specimen' : specimen,
                'collection_date' : collection_date,
                'appaerance' : appaerance,
                'turbidity' : turbidity,
                'glucose' : lab.glucose,
                'protein' : lab.protein,
                'reaction' : lab.reaction,
                'direct' : direct,
                'gram_stain' : gram_stain,
                'other_stain' : other_stain,
                'sensitive_drugs' : sens_drugs,
                'intermediate_drugs' : int_drugs,
                'resistant_drugs' : res_drugs,
            }
            
            self.result_cultures.append(res)
        return self.result_cultures

    
    def microscope(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.exam.result.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['exam_id'])
        request_id = report_data[0]['exam_id'][0]
        lab_obj = self.pool.get('isf.oh.lab')
        lab_ids = lab_obj.search(self.cr, self.uid, [('request_id','=',request_id)])
        lab = lab_obj.browse(self.cr, self.uid, lab_ids)[0]
        
        appaerance = '-'
        gram_stain = '-'
        other_stain = '-'
        zn_stain = '-'
        direct = '-'
        
        if lab.direct_observation:
            direct = lab.direct_observation
        
        if lab.appaerance_id.name:
            appaerance = lab.appaerance_id.name
            
        if lab.gram_stain:
            gram_stain = lab.gram_stain
            
        if lab.other_stain_micro:
            other_stain = lab.other_stain_micro
            
        if lab.zn_stain:
            zn_stain = lab.zn_stain
        
        if lab.need_micro:
            res = {
                'appaerance' : appaerance,
                'volume' :  lab.volume,
                'direct_observation' : direct,
                'protein' : lab.protein,
                'glucose' : lab.glucose,
                'ph' : lab.reaction,
                'gram_stain' : gram_stain,
                'zn_stain' : zn_stain,
                'other_stain' : other_stain,
                'wbc_count' : lab.wbc_count,
                'neutrophils' : lab.neutrophils,
                'lympocytes' : lab.lympocytes,
                'monocytes' : lab.monocytes,
                'eosinophils' : lab.eosinophils,
                'basophil' : lab.basophil,
            }
            
            self.result_micro.append(res)
        return self.result_micro
            
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.exam.result.wizard')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['exam_id'])
        request_id = report_data[0]['exam_id'][0]
        lab_obj = self.pool.get('isf.oh.lab')
        lab_ids = lab_obj.search(self.cr, self.uid, [('request_id','=',request_id)])
        lab = lab_obj.browse(self.cr, self.uid, lab_ids)[0]
            
        
        for exam in lab.exams_ids:
            normal_range = exam.normal_range
            if not normal_range:
                normal_range = ' '
            
            note = exam.note
            if not note:
                note = ' '
                
            result = exam.result
            if not result:
                result = ' '
        
            res = {
                'exam_name' : exam.exam_id.description,
                'uom' : exam.exam_id.uom_id.name,
                'normal_range' : normal_range,
                'result' : result,
                'note' : note,
            }
        
            self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.oh.exam.result.webkit', 'isf.oh.exam.result', 'addons/isf_open_hospital/report/isf_oh_exam_result_report.mako', parser=isf_oh_exam_result_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
