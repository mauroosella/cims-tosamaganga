<html>
<head>
    <style type="text/css">
        ${css}

    @media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 2px;
    font-family:verdana;
    font-size:9px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h3><b><i>101 - Patient Statistics</i></b></h3>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print header()
    print lines()
    """
    %>

     %for data in header():
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 33%;">Date Start</th>
                <th class="td-data" style="width: 33%;">Date Stop</th>
                <th class="td-data" style="width: 34%;">Group By</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 33%;">${data['date_start']}</th>
                <td class="td-data" style="width: 33%;text-align:right;">${data['date_stop']}</td>
                <td class="td-data" style="width: 34%;text-align:right;">${data['group_type']}</td>
            </tr>
        </table>
        <br/>
        
    </center>
    %endfor
    <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Date</th>
                <th class="td-data" style="width: 20%;">Male</th>
                <th class="td-data" style="width: 20%;">Female</th>
                <th class="td-data" style="width: 20%;">Total</th>
            </tr>
            %for data in lines():
                %if data['is_total'] == '0':
                    <tr>
                        <td class="td-data" style="width: 45%;border-style:dashed">${data['date']}</th>
                        <td class="td-data" style="width: 20%;border-style:dashed;text-align:right;">${data['male']}</th>
                        <td class="td-data" style="width: 20%;border-style:dashed;text-align:right;">${data['female']}</th>
                        <td class="td-data" style="width: 20%;border-style:dashed;text-align:right;">${data['total']}</th>
                    </tr>
                %endif
                %if data['is_total'] == '1':
                <tr>
                    <td colspan="3" class="td-data" style="text-align:right;"><b>Total : </b></td>
                    <td class="td-data" style="width: 20%;text-align:right;"><b>${data['total']}</b</th>
                </tr>
                %endif
            %endfor
        </table>
        <br/>
    
</body>
</html>
