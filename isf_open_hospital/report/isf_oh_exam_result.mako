<html>
<head>
    <style type="text/css">
        ${css}
        
table { 
    border-collapse: collapse; 
}

.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.not_break{
    display: block;
    clear: both;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print types()
    print headerlines()
    print lines()
    print microscope()
    print cultures()
    
    """
    %>

    %for report in types():
        %if report['result'] == '1':
            %if report['result'] == '1' and report['cultures'] == '1' or report['microscope'] == '1':
                <div class="break">
            %endif
            %if report['result'] == '1' and report['cultures'] == '0' and report['microscope'] == '0':
                <div class="not_break">
            %endif
                    <center>
                        Result Lab Report
                    </center>
                    <br/>
                    <table style="border: 1px solid black; border-top: 1px solid black; width: 100%" >
                        %for head in headerlines():
                            <tr>
                                <td><b>Request Id</b></td>
                                <td><b>Patient Id</b></td> 
                                <td><b>Date</b></td> 
                            </tr>
                            <tr>
                                <td>${head['request_id']} </td>
                                <td>${head['patient_number']} </td>
                                <td>${head['date']} </td>
                            </tr>
                            <tr>
                                <td><b>Name</b></td>
                                <td><b>Sex</b></td>
                                <td><b>Age</b></td>
                            </tr>
                            <tr>
                                <td>${head['patient_name']}</td>
                                <td>${head['sex']}</td>
                                <td>${head['age']}</td>
                            </tr>
                        %endfor
                    </table>
    
                    <br/>
    
                    <table style="border: 1px solid black;  width: 100%">
                            <tr class="table_header" >
                                <td style="border: 1px solid black;">Exam</td>
                                <td style="border: 1px solid black;">Value</td>
                                <td/>
                                <td style="border: 1px solid black;">Normal Range</td>
                                <td style="border: 1px solid black;">Note</td>
                            </tr>
    
                            %for data in lines():
                                <tr >
                                    <td style="border: 1px solid black; width:30%">${data['exam_name']}</td>
                                    <td style="border: 1px solid black;width:10%">${data['result']}</td>
                                    <td style="border: 1px solid black;width:10%">${data['uom']}</td>
                                    <td style="border: 1px solid black;width:20%">${data['normal_range']}</td>
                                    <td style="border: 1px solid black;width:30%">${data['note']}</td>
                                </tr>
                            %endfor
                    </table>
                </div>

        %endif
        
        %if report['cultures'] == '1':
            %if report['cultures'] == '1' and report['microscope'] == '1':
                <div class="break">
            %endif
            %if report['cultures'] == '1' and report['microscope'] == '0':
                <div class="not_break">
            %endif
                    <center>
                        Bacteriology Section/Culture and sesitivity Report
                    </center>
                    <table style="border: 1px solid black; border-top: 1px solid black; width: 100%" >
                        %for head in headerlines():
                            <tr>
                                <td><b>Request Id</b></td>
                                <td><b>Patient Id</b></td> 
                                <td><b>Date</b></td> 
                            </tr>
                            <tr>
                                <td>${head['request_id']} </td>
                                <td>${head['patient_number']} </td>
                                <td>${head['date']} </td>
                            </tr>
                            <tr>
                                <td><b>Name</b></td>
                                <td><b>Sex</b></td>
                                <td><b>Age</b></td>
                            </tr>
                            <tr>
                                <td>${head['patient_name']}</td>
                                <td>${head['sex']}</td>
                                <td>${head['age']}</td>
                            </tr>
                        %endfor
                    </table>
                     %for data in cultures():
                        <br/>
                        <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                            <tr  class="table_header" >
                                <td colspan="2">Physical Examination</td>
                            </tr>
                            <tr class="row" style="border-top: 0.1px solid black;">
                                <td><b>Specimen</b></td>
                                <td><b>Collection Date</b></td>
                            </tr>
                            <tr>
                                <td style="width:40%;text-align:left;">${data['specimen']}</td>
                                <td style="width:40%;text-align:left;">${data['collection_date']}</td>
                            </tr>
                            <tr class="row" style="border-top: 0.1px solid black;">
                                <td><b>Appearance</b></td>
                                <td><b>Turbidity</b></td>
                            </tr>
                            <tr>
                                <td style="width:40%;text-align:left;">${data['appaerance']}</td>
                                <td style="width:40%;text-align:left;">${data['turbidity']}</td>
                            </tr>
                        </table>
                         <br/>
                        <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                            <tr  class="table_header" >
                                <td colspan="3">Chemical Examination</td>
                            </tr>
                            <tr class="row" style="border-top: 0.1px solid black;">
                                <td><b>Protein</b></td>
                                <td><b>Glucose</b></td>
                                <td><b>Reaction (PH)</b></td>
                            </tr>
                            <tr>
                                <td style="width:40%;text-align:left;">${data['protein']}</td>
                                <td style="width:40%;text-align:left;">${data['glucose']}</td>
                                <td style="width:40%;text-align:left;">${data['reaction']}</td>
                            </tr>
                        </table>
                         <br/>
                        <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                            <tr  class="table_header" >
                                <td colspan="2">Microscopic Examination</td>
                            </tr>
                            <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Direct Wet Mount Result</td>
                                <td style="width:40%;text-align:left;">${data['direct']}</td>
                            <tr>
                            <tr>
                                <td>Gram Stain Result</td>
                                <td style="width:40%;text-align:left;">${data['gram_stain']}</td>
                            </tr>
                            <tr>
                                <td>Other Stain Result</td>
                                <td style="width:40%;text-align:left;">${data['other_stain']}</td>
                            </tr>
                        </table>
                        <br/>
                        <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                            <tr  class="table_header" >
                                <td colspan="3">Culture Result</td>
                            </tr>
                            
                            <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                                <tr class="table_header">
                                    <td><i>Sensitive Drugs</i></td>
                                %for d in data['sensitive_drugs']:
                                    <tr>
                                        <td> ${d} </td>
                                    </tr>
                                %endfor
                                <tr/>
                            </table>
                            <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                                <tr class="table_header">
                                <td><i>Intermediate Drugs</i></td>
                                 %for d in data['intermediate_drugs']:
                                    <tr>
                                        <td> ${d} </td>
                                    </tr>
                                %endfor
                            </table>
                            <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                            <tr class="table_header">  
                                <td><i>Resistant Drugs</i></td>
                                 %for d in data['resistant_drugs']:
                                    <tr>
                                        <td> ${d} </td>
                                    </tr>
                                %endfor
                            </table>
                        </table>
                </div>
            %endfor
        %endif
         %if report['microscope'] == '1':
            <div>
                <center>
                    Microscope Observation
                </center>
                <br/>
                <table style="border: 1px solid black; border-top: 1px solid black; width: 100%" >
                    %for head in headerlines():
                        <tr>
                            <td><b>Request Id</b></td>
                            <td><b>Patient Id</b></td> 
                            <td><b>Date</b></td> 
                        </tr>
                        <tr>
                            <td>${head['request_id']} </td>
                            <td>${head['patient_number']} </td>
                            <td>${head['date']} </td>
                        </tr>
                        <tr>
                            <td><b>Name</b></td>
                            <td><b>Sex</b></td>
                            <td><b>Age</b></td>
                        </tr>
                        <tr>
                            <td>${head['patient_name']}</td>
                            <td>${head['sex']}</td>
                            <td>${head['age']}</td>
                        </tr>
                    %endfor
                </table>
                %for data in microscope():
                    <br/>
                    <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                        <tr  class="table_header" >
                            <td colspan="2"> Macroscopic Observation </td>
                        </tr>

                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Appearance</td>
                                <td style="width:40%;text-align:right;">${data['appaerance']}</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Volume</td>
                                <td style="width:40%;text-align:right;">${data['volume']}</td>
                        </tr>                            
                    </table>
                    <br/>
                    <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                        <tr  class="table_header" >
                            <td colspan="2"> Chemical Test </td>
                        </tr>

                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Protein</td>
                                <td style="width:40%;text-align:right;">${data['protein']}</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Glucose</td>
                                <td style="width:40%;text-align:right;">${data['glucose']}</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>ph</td>
                                <td style="width:40%;text-align:right;">${data['ph']}</td>
                        </tr>                            
                    </table>
                    <br/>
                    <table style="border-bottom: 0.1px solid black; border-top: 0.1px solid black; width: 100%">
                        <tr  class="table_header" >
                            <td colspan="2"> Microscopic Observation </td>
                        </tr>

                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Direct microscope</td>
                                <td style="width:40%;text-align:right;">${data['direct_observation']}</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Gram Stain</td>
                                <td style="width:40%;text-align:right;">${data['gram_stain']}</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Zeihl Nelsen Stain</td>
                                <td style="width:40%;text-align:right;">${data['zn_stain']}</td>
                        </tr>  
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Other Stain</td>
                                <td style="width:40%;text-align:right;">${data['other_stain']}</td>
                        </tr>   
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Total WBC Count (5WB/MM)</td>
                                <td style="width:40%;text-align:right;">${data['wbc_count']}</td>
                        </tr>                            
                    </table>
                    <br/>
                    <table style="border-bottom: 0.1px solid black; border-top: 1px solid black; width: 100%">
                        <tr  class="table_header" >
                            <td colspan="3"> Differential Count </td>
                        </tr>

                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Neutrophils</td>
                                <td style="width:40%;text-align:right;">${data['neutrophils']}</td>
                                <td style="text-align:right;">(40-72)</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Lympocytes</td>
                                <td style="width:40%;text-align:right;">${data['lympocytes']}</td>
                                <td style="text-align:right;">(21-40)</td>
                        </tr>
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Monocytes</td>
                                <td style="width:40%;text-align:right;">${data['monocytes']}</td>
                                <td style="text-align:right;">(2-10)</td>
                        </tr>     
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Eosinophils</td>
                                <td style="width:40%;text-align:right;">${data['eosinophils']}</td>
                                <td style="text-align:right;">(1-6)</td>
                        </tr>   
                        <tr class="row" style="border-top: 0.1px solid black;">
                                <td>Basophil</td>
                                <td style="width:40%;text-align:right;">${data['basophil']}</td>
                                <td style="text-align:right;">(0-1)</td>
                        </tr>                            
                    </table>
                %endfor
            </div>
        %endif
    %endfor
</body>
</html>
