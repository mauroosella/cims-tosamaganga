# -*- coding: utf-8 -*-

{
	'name': 'ISF HIMS Core',
	'version': '0.6',
	'category': 'Tools',
	'description': """

ISF HIMS Core
=============
	
* HIMS Module
* Patients Registration
* OPD Management
* Admission/Discharge Management
* Exams Management
* Disease Management
* Ward Management 
* ICD-10 Semplified Disease Database

""",
	'author': 'Matteo Angelino (matteo.angelino@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': [
        'base',
        'report_webkit',
        'hr',
        'isf_home_dashboard',
        'web_m2o_enhanced',
        'tg_partner_firstname',
        'account',
        'isf_cims_module',
    ],
	'data': [
        # security
        'security/oh_security.xml',
        'security/ir.model.access.csv',
        # view
        'views/isf_oh_menu.xml',
        'views/res_config_view.xml',
        'views/isf_oh_hospital_view.xml',
        'views/isf_oh_exam_view.xml',
        'views/isf_oh_lab_view.xml',
        'views/isf_oh_operation_view.xml',
        'views/isf_oh_place_view.xml',
        'views/isf_oh_patient_view.xml',
        'views/isf_oh_disease_view.xml',
        'views/isf_oh_vaccine_view.xml',
        'views/isf_oh_admission_view.xml',
        'views/isf_oh_opd_view.xml',
        'views/isf_oh_sequence.xml',
        # Wizard
        'wizard/isf_oh_patient_search_view.xml',
        'wizard/isf_oh_exam_result_wizard_view.xml',
        'wizard/isf_oh_patient_number_wizard_view.xml',
        # Data
        'data/sequence/sequence.xml',
        'data/csv/isf.oh.ward.csv',
        'data/csv/isf.oh.place.csv',
        'data/csv/isf.oh.admission.type.csv',
        'data/csv/isf.oh.disease.category.csv',
        'data/csv/isf.oh.disease.csv',
        'data/csv/isf.oh.discharge.type.csv',
        'data/csv/isf.oh.operation.type.csv',
        'data/csv/isf.oh.operation.csv',
        'data/csv/isf.oh.operation.patient.condition.csv',
        'data/csv/isf.oh.anesthetic.method.csv',
        'data/csv/isf.oh.vaccine.category.csv',
        'data/csv/isf.oh.vaccine.csv',
        'data/csv/isf.oh.exam.type.csv',
        'data/csv/isf.oh.uom.csv',
        'data/csv/isf.oh.exam.csv',
        'data/csv/isf.home.dashboard.action.group.csv',
        'data/csv/isf.home.dashboard.action.csv',
        
    ],
	'css' : ['static/src/css/isf.css'],
	'demo': [],
	'installable' : True,
}

