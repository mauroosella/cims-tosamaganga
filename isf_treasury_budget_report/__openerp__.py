# -*- coding: utf-8 -*-
{
	'name': 'ISF Treasury Budget Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF Treasury Budget Report 
==========================

The report allows to define:

- Cash-In flows section
- Cash-Out flows section

both with accouts and sub-section as well. It then calculates for each period of the choosen Fiscal Year: 

- Balance at the beginning of period
- Differences between Cash-In and Cash-Out Flows
- Balance at the end of period
- Cash and Bank Reconciliation
- Difference Books & Cash Balances (must be zero)

The report also warns the user if some accounts are used in more than one section and if there are accounts that are not found in any sections (but found in the journal entries)
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['base_headers_webkit','account_accountant'],
	'data': [
		'isf_treasury_report.xml',
        'wizard/isf_treasury_report_view.xml',
    ],
	'demo': [],
	'installable' : True,
}

