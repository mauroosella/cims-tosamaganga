# -*- coding: utf-8 -*-

{
	'name': 'ISF Payroll',
	'version': '0.1',
	'category': 'Tools',
	'description': """

ISF Payroll 
==================================

Add the following functions to bank and cash menu. All functionality can be configured in Settings/Configuration

* Salary Advances On Payroll
* Advance Payment Refund
* Payroll Computing
* Payroll Payment
	
""",
	'author': 'Matteo Angelino (matteo.angelino@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['account','isf_home_dashboard','isf_cims_module'],
	'data': [
        	'data/account_data.xml',
            'security/isf_payroll_security.xml',
        	'isf_salary_advances_on_payroll_view.xml',
            #'isf_advance_payment_refund_view.xml',
            'isf_payroll_computing_view.xml',
            'isf_payroll_payment_view.xml',
            'res_config_view.xml',
            'data/isf.home.dashboard.action.group.csv',
            'data/isf.home.dashboard.action.csv',
            'security/ir.model.access.csv',
    	],
    'css' : ['static/src/css/isf.css'],
	'demo': [],
	'installable' : True,
}

