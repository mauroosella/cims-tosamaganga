from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=True
    

class isf_payroll_computing(osv.osv_memory):
    _name = 'isf.payroll.computing'
    
    
    def _get_account_liability_payroll(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'account_liability_payroll')
                
        
    def _get_account_liability_payroll_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_payroll(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
    
    def _get_account_expense_gross(self, cr, uid, context=None):
        if context is None:
            context = {}
        result = []
        ir_values = self.pool.get('ir.values')
        expense_ids = ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'account_expense_gross')
        if expense_ids:
            for expense_id in expense_ids:
                account_obj = self.pool.get('account.account')
                account_ids = account_obj.search(cr, uid, [('id','=',expense_id)])
                if account_ids:
                    account = account_obj.browse(cr, uid, account_ids)[0]
                    result.append((account.id, account.name))        
                
        return result

        
    def _get_account_liability_deduction(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'account_liability_deduction')
        
    def _get_account_liability_deduction_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_liability_deduction(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
        
    def _get_account_asset_advances(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'account_asset_advances')
        
    def _get_account_asset_advances_text(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        account_id = self._get_account_asset_advances(cr, uid, context=context)
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]

            return account.code+" - "+account.name
                
        return None
        
    def _get_default_journal_id(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'pay_comp_journal_id')
            
    def _get_analytic_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        
        ir_values = self.pool.get('ir.values')
        aa_ids = ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'pay_comp_analytic_account_ids')

        if aa_ids:
            for aa_id in aa_ids:
                aa_obj = self.pool.get('account.analytic.account')
                aa_ids = aa_obj.search(cr, uid, [('id','=',aa_id)])
                if aa_ids:
                    aa = aa_obj.browse(cr, uid, aa_ids)[0]
                    result.append((aa.id, aa.name))        
                
        return result
    
    def _get_help(self, cr, uid, context=None):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.payroll.config.settings', 'pay_comp_help')
    
    
            
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal', required=True),
        'ref' : fields.char('Reference', size=64,required=True),
        'name' : fields.char('Description', size=64, required=True),
        'date' : fields.date('Date', required=True),
        'account_expense_gross' : fields.selection(_get_account_expense_gross,'Gross Amount', required=True),
        'account_liability_payroll' : fields.many2one('account.account','Net Amount Payroll', required=True),
        'account_liability_deduction' : fields.many2one('account.account','Deductions', required=True),
        'account_asset_advances' : fields.many2one('account.account','Advances', required=True),
        'analytic_account' : fields.selection(_get_analytic_accounts,'Analytic account', required=True),
        'amount_expense_gross' : fields.float('Gross Amount', required=True),
        'amount_liability_payroll' : fields.float('Net Amount', required=True),
        'amount_liability_deduction' : fields.float('Deductions (If Any)', required=False),
        'amount_asset_advances' : fields.float('Advances (If Any)', required=False),
        'currency' : fields.many2one('res.currency','Currency', required=True,help='Currency of the operation'),
        'balance' : fields.float('Balance', size=32,help='Balance must be zero if data are correct'),
        'account_liability_payroll_text' : fields.char('Net Amount',size=64,readonly=True),
        'account_liability_deduction_text' : fields.char('Deductions (If Any)',size=64,readonly=True),
        'account_asset_advances_text' : fields.char('Advances (If Any)',size=64,readonly=True),
        'help' : fields.text('Help', size=512),
    }
    
    _defaults = {
        'journal_id' : _get_default_journal_id,
        'date' : fields.date.context_today,
        'account_liability_payroll' : _get_account_liability_payroll,
        'account_liability_deduction' : _get_account_liability_deduction,
        'account_asset_advances' : _get_account_asset_advances,
        'amount_asset_advances' : 0.0,
        'amount_liability_deduction' : 0.0,
        'balance' : 0.0,
        'account_liability_payroll_text' : _get_account_liability_payroll_text,
        'account_liability_deduction_text' : _get_account_liability_deduction_text,
        'account_asset_advances_text' : _get_account_asset_advances_text,
        'help' : _get_help,
        
    }
    
    def _check_amount_expense_gross(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
          
        if obj.amount_expense_gross <= 0.0 :
            return False
        return True
        
    def _check_amount_liability_payroll(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
            
        if obj.amount_liability_payroll <= 0.0 :
            return False
        return True
        
    def _check_amount_liability_deduction(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
         
        if obj.amount_liability_deduction >= 0.0 :
            return True
        return False
        
    def _check_amount_asset_advances(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
         
        if obj.amount_asset_advances >= 0.0 :
            return True
        return False
        
    def _check_balanced(self, cr, uid, ids ,context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        
        
        balance = obj.amount_expense_gross - (obj.amount_liability_payroll + obj.amount_asset_advances + obj.amount_liability_deduction )
        if _debug:
            _logger.debug('###### Balance : %f', balance)
        if balance == 0.0:
            return True
            
        return False

    _constraints = [
         (_check_amount_expense_gross, 'Gross mount must be positive ( > 0.0)',['']),
         (_check_amount_liability_payroll, 'Net mount must be positive ( > 0.0)',['']),
         (_check_amount_liability_deduction, 'Deduction amount must be positive ( > 0.0)',['']),
         (_check_amount_asset_advances, 'Advances amount must be positive ( > 0.0)',['']),
         (_check_balanced, 'Entry must be balanced, check your data',['']),
    ]

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
	
	
    
    def compute_payroll(self, cr, uid, ids, context=None):
        if context is None:
            context = {}    
        
        data = self.browse(cr, uid, ids, context=context)[0]

        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
		
        move = move_pool.account_move_prepare(cr, uid, data.journal_id.id, data.date, ref=data.ref, context=context)	
        move_id = move_pool.create(cr, uid, move, context=context)
		
		
        if _debug:
            _logger.debug('move_id : %s', move_id)

        company_currency_id = self._get_company_currency_id(cr, uid, context=context)
        
        currency = False
        amount_expense_gross_currency = False
        amount_liability_payroll_currency = False
        amount_liability_deduction_currency = False
        amount_asset_advances_currency = False
        
        amount_expense_gross = data.amount_expense_gross
        amount_liability_payroll = data.amount_liability_payroll
        amount_liability_deduction = data.amount_liability_deduction
        amount_asset_advances = data.amount_asset_advances
        
        if data.currency.id != company_currency_id:
            c_pool = self.pool.get('res.currency')
            currency = data.currency.id
            amount_expense_gross = c_pool.compute(cr, uid,data.currency.id, company_currency_id,  data.amount_expense_gross, context=context)
            amount_liability_payroll = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_payroll, context=context)
            amount_liability_deduction = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_liability_deduction, context=context)
            amount_asset_advances = c_pool.compute(cr, uid, data.currency.id,company_currency_id,  data.amount_asset_advances, context=context)
            
            amount_expense_gross_currency = data.amount_expense_gross
            amount_liability_payroll_currency = -1 * data.amount_liability_payroll
            amount_liability_deduction_currency = -1 * data.amount_liability_deduction
            amount_asset_advances_currency = -1 * data.amount_asset_advances
            
        move_line = {
            'analytic_account_id': data.analytic_account or False,
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency,
            'credit': 0.0,
            'debit': amount_expense_gross,
            'date_maturity' : False,
            'amount_currency': amount_expense_gross_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.account_expense_gross),
            'state' : 'valid'
        }
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
			
        move_line = {
            'analytic_account_id': False,
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency,
            'credit': amount_liability_payroll,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': amount_liability_payroll_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.account_liability_payroll),
            'state' : 'valid'
        }
	
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        if data.amount_liability_deduction > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_liability_deduction,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_liability_deduction_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_liability_deduction),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        if data.amount_asset_advances > 0.0:
            move_line = {
                'analytic_account_id': False,
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': currency,
                'credit': amount_asset_advances,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': amount_asset_advances_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': int(data.account_asset_advances),
                'state' : 'valid'
            }
        
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        return {'type': 'ir.actions.act_window_close'}
    
    def _get_analytic_journal_by_name(self, cr, uid, name):
        analytic_journal_pool = self.pool.get('account.analytic.journal')
        analytic_journal_ids = analytic_journal_pool.search(cr, uid, [('name','=',name)])
        return analytic_journal_ids[0] if len(analytic_journal_ids) else None
            
    def _create_default_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
            
        journal_pool = self.pool.get('account.journal')
        journal_ids = journal_pool.search(cr, uid, [('code','=','PRCJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Payroll Computing Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Payroll Computing Sequence',
                    'prefix' : 'PRC/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            journal = {
                'name' : 'Payroll Computing Journal',
                'code' : 'PRCJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General'),
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        return True
        
    def onchange_amount(self,cr,uid,ids,amount_expense_gross,amount_liability_payroll,amount_liability_deduction,amount_asset_advances,context=None):
        balance = amount_expense_gross  - (amount_liability_payroll + amount_liability_deduction + amount_asset_advances)
        
        result = {'value':{} }
        result['value'].update({
            'balance' : balance,
        })
        
        return result
        
    def _redirect(self, cr, uid,  context=None):
        return {
            'name':_('Test'),
            'view_mode': 'list',
            'view_id': False,
            #'views': [(resource_id,'form')],
            'view_type': 'list, tree, form',
            'view_id' : 328, # id of the object to which to redirected
            'res_model': 'account.move', # object name
            'type': 'ir.actions.act_window',
            #'target': 'inline', # if you want to open the form in new tab
        }
        
isf_payroll_computing()
