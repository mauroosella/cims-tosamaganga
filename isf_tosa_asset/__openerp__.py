# -*- encoding: utf-8 -*-

{
    'name': 'Tosamaganga - Asset Extension',
    'version': '0.9',
    'depends': ['isf_cims_module',
                'account_asset_management',
                'account_asset_register'],
    'author': 'Alessandro Domanico <alessandro.domanico@informaticisenzafrontiere.org>',
    'description': """
Tosamaganga Hospital Asset Extension
====================================

The module adds these fields:

* Location (new model vith views)
* State of Use
* Old Reference
* Serial Number
* Set 'code' unique constraint and readonly <- automatically set by a sequence defined on the Asset Category

and these procedures:

* multiple assets computation
* multiple assets validation
* multiple assets revaluation

and these reports:

* Fixed Assets Revision Book

and these menus:

* CIMS \ Operations \ Assets
* CIMS \ Reports \ 904 - Fixed Assets Revision Book
    """,
    'license': 'AGPL-3',
    'category': 'Localization',
    'data': ['view/account_asset_view.xml',
             'view/asset_register_view.xml',
             'view/isf_asset_validate_selected_view.xml',
             'view/isf_asset_compute_selected_view.xml',
             'view/isf_asset_revaluate_selected_view.xml',
             'view/isf_tosa_asset_location_view.xml',
             'wizard/isf_tosa_asset_revision_book_view.xml',
             'security/ir.model.access.csv',
             'data.xml',
            ],
    'demo': [],
    'installable': True,
    'auto_install': False
}
